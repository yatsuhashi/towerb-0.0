﻿/***********************************************************************//**
	@file
***************************************************************************/
#pragma once

namespace twb {
namespace render {
/***********************************************************************//**
	@brief 
***************************************************************************/
class Env {
 public:
  enum AlphaMode {
    ALPHA_NULL, 
    ALPHA_BLEND, 
    ALPHA_ADD, 
    ALPHA_MUL, 
    ALPHA_REVERSE, 
    ALPHA_SCREEN, 
    ALPHA_NONE, 
  };

  enum CullFace {
    CULL_NULL, 
    CULL_NONE, 
    CULL_BACK, 
    CULL_FRONT
  };

 private:
  enum Flag {
    FLAG_DEPTH_TEST, 
    FLAG_DEPTH_WRITE, 
    FLAG_POINT_SMOOTH, 
    FLAG_LINE_SMOOTH, 
    FLAG_POLYGON_SMOOTH, 
    FLAG_MAX
  };

 private:
  AlphaMode alphaMode_;
  CullFace cullFace_;
  std::bitset<FLAG_MAX> flag_;
  std::bitset<FLAG_MAX> change_;

 public:
  Env();
  Env(const Env& src, const Env& des);
  ~Env() {}

  Env& setAlphaMode(AlphaMode mode) {
    alphaMode_ = mode;
    return *this;
  }

  AlphaMode getAlphaMode() const {
    return alphaMode_;
  }

  Env& setCullFace(CullFace face) {
    cullFace_ = face;
    return *this;
  }

  CullFace getCullFace() const {
    return cullFace_;
  }

  Env& setDepthTest(bool isTest) {
    return setFlag(FLAG_DEPTH_TEST, isTest);
  }

  bool isDepthTest() const {
    return flag_.test(FLAG_DEPTH_TEST);
  }

  Env& setDepthWrite(bool isWrite) {
    return setFlag(FLAG_DEPTH_WRITE, isWrite);
  }

  bool isDepthWrite() const {
    return flag_.test(FLAG_DEPTH_WRITE);
  }

  Env& setPointSmooth(bool isSmooth) {
    return setFlag(FLAG_POINT_SMOOTH, isSmooth);
  }

  bool isPointSmooth() const {
    return flag_.test(FLAG_POINT_SMOOTH);
  }

  Env& setLineSmooth(bool isSmooth) {
    return setFlag(FLAG_LINE_SMOOTH, isSmooth);
  }

  bool isLineSmooth() const {
    return flag_.test(FLAG_LINE_SMOOTH);
  }

  Env& setPolygonSmooth(bool isSmooth) {
    return setFlag(FLAG_POINT_SMOOTH, isSmooth);
  }

  bool isPolygonSmooth() const {
    return flag_.test(FLAG_POLYGON_SMOOTH);
  }

  void apply();

 private:
  void applyAlphaMode();
  void applyCullFace();

  Env& setFlag(Flag flag, bool value);
  void applyFlag(Flag flag, GLenum cap);
};
/***********************************************************************//**
	$Id$
***************************************************************************/
}
}
