﻿/***********************************************************************//**
	@file
***************************************************************************/
#pragma once

namespace giraffe {
/***********************************************************************//**
	@brief エクスポーター基底クラス
***************************************************************************/
class Exporter : public twb::SharedObject {
 private:
  twb::Shared<Node> node_;
  twb::Shared<model::Model> model_;
  wxFileName path_;
  std::auto_ptr<wxOutputStream> out_;

 public:
  virtual ~Exporter();

  wxFileName getRelativePath(const wxFileName& path) const;

  static bool ExportNode(Node* node);
  static bool ExportNode(Node* node, const wxFileName& path);

  static bool ExportModel(model::Model* model);
  static bool ExportModel(model::Model* model, const wxFileName& path);

 protected:
  Exporter();

  Node* getNode() const {
    return node_;
  }

  model::Model* getModel() const {
    return model_;
  }

  const wxFileName& getPath() const {
    return path_;
  }

  bool open();
  wxOutputStream* getOut() {
    return out_.get();
  }

  virtual bool exportNode() {
    return false;
  }

  virtual bool exportModel() {
    return false;
  }

  static wxString Join(const wxArrayString& items, 
                       const wxString& indent = "");

 private:
  void setNode(Node* node);
  void setModel(model::Model* model);
  void setPath(const wxFileName& path) {
    path_ = path;
  }

  static Exporter* Create(const wxFileName& path);
};
/***********************************************************************//**
	$Id$
***************************************************************************/
}
