﻿/***********************************************************************//**
	@file
***************************************************************************/
#pragma once
/***********************************************************************//**
	@brief プラットフォーム判別
***************************************************************************/
#if defined(__ENVIRONMENT_MAC_OS_X_VERSION_MIN_REQUIRED__)
#define TWB_PLATFORM "osx"
#define TWB_PLATFORM_OSX
#define TWB_PLATFORM_APPLE
#define TWB_PLATFORM_POSIX
#define TWB_WITH_GL
#define TWB_WITH_WX

#elif defined(__ENVIRONMENT_IPHONE_OS_VERSION_MIN_REQUIRED__)
#define TWB_PLATFORM "ios"
#define TWB_PLATFORM_IOS
#define TWB_PLATFORM_APPLE
#define TWB_PLATFORM_POSIX
#define TWB_WITH_GLES
#define TWB_WITH_GLES_OES

#elif defined(__ANDROID__)
#define TWB_PLATFORM "android"
#define TWB_PLATFORM_ANDROID
#define TWB_PLATFORM_POSIX
#define TWB_WITH_GLES

#elif defined(__linux)
#define TWB_PLATFORM "linux"
#define TWB_PLATFORM_LINUX
#define TWB_PLATFORM_POSIX
#define TWB_WITH_GL

#elif defined(WIN32)
#define TWB_PLATFORM "win32"
#define TWB_PLATFORM_WINDOWS
#define TWB_PLATFORM_WIN32
#define TWB_WITH_GL
#define TWB_WITH_WX
#define STRICT
#include <windows.h>

#else
#error "not support platform"

#endif
/***********************************************************************//**
	@brief 
***************************************************************************/
#include <errno.h>
#include <stdlib.h>

#include <bitset>
#include <climits>
#include <functional>
#include <map>
#include <memory>
#include <stack>
#include <string>
#include <vector>

#if defined(TWB_PLATFORM_POSIX)
#include <pthread.h>
#include <sched.h>
#endif

#if defined(TWB_PLATFORM_APPLE)
#include <mach/mach_time.h>
#endif
/***********************************************************************//**
	@brief wxWidgets
***************************************************************************/
#if defined(TWB_WITH_WX)
#if defined(TWB_PLATFORM_OSX)
#define _FILE_OFFSET_BITS 64
#define __WXMAC__
#define __WXOSX__
#define __WXOSX_COCOA__
#endif
#include <wx/wx.h>
#include <wx/aui/auibar.h>
#include <wx/aui/auibook.h>
#include <wx/aui/framemanager.h>
#include <wx/cmdproc.h>
#include <wx/config.h>
#include <wx/glcanvas.h>
#include <wx/treectrl.h>
#include <wx/propgrid/propgrid.h>
#include <wx/propgrid/advprops.h>
#include <wx/splitter.h>
#include <wx/sstream.h>
#include <wx/stdpaths.h>
#include <wx/wfstream.h>
#include <wx/xml/xml.h>
#endif
/***********************************************************************//**
	@brief GLM
***************************************************************************/
#define GLM_FORCE_RADIANS
#define GLM_FORCE_CXX03
/*
#include <glm/detail/setup.hpp>
#undef GLM_HAS_EXTENDED_INTEGER_TYPE
#define GLM_HAS_EXTENDED_INTEGER_TYPE 0
*/
#include <glm/glm.hpp>
#include <glm/ext.hpp>
/***********************************************************************//**
	@brief 
***************************************************************************/
#if defined(TWB_PLATFORM_WINDOWS) && !defined(NDEBUG)
#include <crtdbg.h>
#define _CRTDBG_MAP_ALLOC
#define NEW new(_NORMAL_BLOCK, __FILE__, __LINE__)

#else
#define NEW new

#endif
/***********************************************************************//**
	@brief 
***************************************************************************/
namespace twb {
template <class T, size_t SIZE>
constexpr size_t GetArraySize(const T(&array)[SIZE]) {
  return SIZE;
}
}
/***********************************************************************//**
	@brief 
***************************************************************************/
namespace twb {
class Buffer;
class SharedObject;
class String;
class TreeNode;

namespace canvas {
class Context;
class Layout;
class Widget;
}

namespace render {
class Camera;
class Geometry;
class Image;
class Material;
class Mesh;
class Model;
class OrthoCamera;
class PerspectiveCamera;
class Renderable;
class Renderer;
class Scene;
class Shader;
class Texture;

namespace geometry {
class Axis;
class GeometryBase;
class Grid;
}
}

namespace thread {
class Thread;
}
};
/***********************************************************************//**
	$Id$
***************************************************************************/
