﻿/***********************************************************************//**
	@file
***************************************************************************/
#pragma once

#include "twb/Stream.hpp"

namespace twb {
/***********************************************************************//**
	@brief ファイル
***************************************************************************/
class File : public Stream {
 private:
  FILE* fd_;
  int error_;

 public:
  File();
  File(const char* path, const char* mode);
  virtual ~File();

  bool open(const char* path, const char* mode);
  bool close();

  bool isError() const {
    return error_ != 0;
  }
  const char* getError() const;

  virtual size_t read(void* buff, size_t size) override;
  virtual size_t write(const void* buff, size_t size) override;

  static size_t GetSize(const char* path);
  static bool Read(const char* path, Buffer& buffer);
  static bool IsExist(const char* path);
};
/***********************************************************************//**
	$Id$
***************************************************************************/
}
