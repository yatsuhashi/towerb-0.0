﻿/***********************************************************************//**
	@file
***************************************************************************/
#pragma once

#include "twb/Shared.hpp"
#include "twb/SharedArray.hpp"

namespace twb {
namespace render {
/***********************************************************************//**
	@brief レンダラー
***************************************************************************/
class Renderer {
 public:
  enum CullFace {
    CULL_NULL, 
    CULL_BACK, 
    CULL_FRONT, 
    CULL_NONE
  };

 private:
  enum {
    FLAG_RENDERING, 
    FLAG_MAX
  };

 private:
  std::bitset<FLAG_MAX> flag_;
  SharedArray<Camera> cameraStack_;
  Shared<Shader> shader_;
  Shared<Material> material_;
  GLint defaultFrameBuffer_;
  GLint textureUnitMax_;
  Shared<Texture>* textures_;

 public:
  Renderer();
  ~Renderer();

  void beginRender();
  void endRender();
  bool isRendering() const {
    return flag_.test(FLAG_RENDERING);
  }

  void pushCamera(Camera* camera);
  void popCamera();
  Camera* getCamera() const;

  void beginShader(Shader* shader);
  void endShader();
  Shader* getShader() const {
    return shader_;
  }

  void setModelMatrix(const glm::mat4& matrix);

  void beginMaterial(Material* material);
  void endMaterial();
  Material* getMaterial() const {
    return material_;
  }

  void beginTexture(Texture* texture, int unit = 0);
  void endTexture(int unit = 0);

  void clear(const glm::vec4& color);
};
/***********************************************************************//**
	$Id$
***************************************************************************/
}
}
