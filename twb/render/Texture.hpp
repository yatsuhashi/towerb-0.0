﻿/***********************************************************************//**
	@file
***************************************************************************/
#pragma once

#include "twb/Shared.hpp"
#include "twb/SharedObject.hpp"

namespace twb {
namespace render {
/***********************************************************************//**
	@brief テクスチャ
***************************************************************************/
class Texture : public SharedObject {
 public:
  enum Filter {
    FILTER_NEAREST, 
    FILTER_LINEAR
  };

  enum Wrap {
    WRAP_REPEAT, 
    WRAP_CLAMP
  };

 private:
  Shared<Image> image_;
  GLenum magFilter_;
  GLenum minFilter_;
  GLenum wrapU_;
  GLenum wrapV_;

 public:
  Texture(Image* image);
  virtual ~Texture();

  void bind(int unit = 0) const;
  void unbind(int unit = 0) const;

  Texture& setMagFilter(Filter filter) {
    magFilter_ = GetFilter(filter);
    return *this;
  }

  Filter getMagFilter() const {
    return GetFilter(magFilter_);
  }

  Texture& setMinFilter(Filter filter) {
    minFilter_ = GetFilter(filter);
    return *this;
  }

  Filter getMinFilter() const {
    return GetFilter(minFilter_);
  }

  Texture& setWrapU(Wrap wrap) {
    wrapU_ = GetWrap(wrap);
    return *this;
  }

  Wrap getWrapU() const {
    return GetWrap(wrapU_);
  }

  Texture& setWrapV(Wrap wrap) {
    wrapV_ = GetWrap(wrap);
    return *this;
  }

  Wrap getWrapV() const {
    return GetWrap(wrapV_);
  }

 private:
  static GLenum GetFilter(Filter filter);
  static Filter GetFilter(GLenum filter);
  static GLenum GetWrap(Wrap wrap);
  static Wrap GetWrap(GLenum);
};
/***********************************************************************//**
	$Id$
***************************************************************************/
}
}
