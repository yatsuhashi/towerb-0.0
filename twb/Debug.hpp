﻿/***********************************************************************//**
	@file
***************************************************************************/
#pragma once
/***********************************************************************//**
	@brief デバッグ用マクロ
***************************************************************************/
#if !defined(TWB_MASTER)
#define TWB_PRINTF(...)                         \
  twb::Debug::Printf(__VA_ARGS__)

#define TWB_DUMP(value)                         \
  TWB_PRINTF("%s = ", #value);                  \
  twb::Debug::Dump(value);                      \
  TWB_AT

#define TWB_SUSPEND                             \
  twb::Debug::Suspend()

#define TWB_AT                                  \
  TWB_PRINTF(" at %s:%d\n", __FILE__, __LINE__)

#define TWB_ASSERT(e)                           \
  if(!(e)) {                                    \
    TWB_PRINTF("[ASSERT]%s", #e);               \
    TWB_AT;                                     \
    TWB_SUSPEND;                                \
  }

#define TWB_WARNING(...)                        \
  {                                             \
    TWB_PRINTF("[WARNING]");                    \
    TWB_PRINTF(__VA_ARGS__);                    \
    TWB_AT;                                     \
  }

#define TWB_ERROR(...)                          \
  {                                             \
    TWB_PRINTF("[ERROR]");                      \
    TWB_PRINTF(__VA_ARGS__);                    \
    TWB_AT;                                     \
    TWB_SUSPEND;                                \
  }

#else
#define TWB_PRINTF(...)
#define TWB_DUMP(value)
#define TWB_SUSPEND
#define TWB_AT
#define TWB_ASSERT(e)
#define TWB_WARNING(...)
#define TWB_ERROR(...)

#endif

namespace twb {
/***********************************************************************//**
	@brief デバッグ補助
***************************************************************************/
class Debug {
 private:
  static std::auto_ptr<Debug> Instance_;
  static std::string LineBuff_;

 public:
  virtual ~Debug();

  static void Printf(const char* format, ...);

  /**
   * intの値を出力する
   * @param[in] value 出力する値
   */
  static void Dump(int value) {
    Printf("%d", value);
  }

  /**
   * floatの値を出力する
   * @param[in] value 出力する値
   */
  static void Dump(float value) {
    Printf("%f", value);
  }

  static void Suspend();

 protected:
  Debug();

  virtual void print(const char* text);
  virtual void suspend();

 private:
  static Debug* GetInstance();
};
/***********************************************************************//**
	$Id$
***************************************************************************/
}
