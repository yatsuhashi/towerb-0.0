﻿/***********************************************************************//**
	@file
***************************************************************************/
#include "stdafx.hpp"
#include "giraffe/gui/Application.hpp"
#include "giraffe/gui/Canvas.hpp"
#include "giraffe/gui/MainFrame.hpp"
#include "giraffe/gui/Menu.hpp"
#include "giraffe/gui/Outliner.hpp"
#include "giraffe/gui/PropertyGrid.hpp"
#include "giraffe/gui/TimeLine.hpp"

namespace giraffe {
namespace gui {
static const int CANVAS_WIDTH = 640;
static const int CANVAS_HEIGHT = 480;
static const int OUTLINER_WIDTH = 240;
static const int PROPERTY_WIDTH = 240;
static const int TIMELINE_HEIGHT = 200;
static const int WINDOW_WIDTH = 
  OUTLINER_WIDTH + CANVAS_WIDTH + PROPERTY_WIDTH;
static const int WINDOW_HEIGHT = 
  CANVAS_HEIGHT + TIMELINE_HEIGHT;
/***********************************************************************//**
	@brief コンストラクタ
***************************************************************************/
MainFrame::MainFrame()
  : super(0, wxID_ANY, "Giraffe", wxDefaultPosition, 
          wxSize(WINDOW_WIDTH, WINDOW_HEIGHT)), 
    auiManager_(this), 
    canvas_(NEW Canvas(this)), 
    outliner_(NEW Outliner(this)), 
    propertyGrid_(NEW PropertyGrid(this)), 
    timeLine_(NEW TimeLine(this)), 
    log_(NEW wxTextCtrl(this, wxID_ANY, wxEmptyString, 
                        wxDefaultPosition, wxDefaultSize, 
                        wxTE_MULTILINE |
                        wxTE_READONLY))
{
  Application::Get()->pushLogTarget(NEW wxLogTextCtrl(log_));
  setupMenuBar();
  auiManager_.AddPane(canvas_, 
                      wxAuiPaneInfo().
                      Name("Canvas").
                      Caption("キャンバス").
                      CenterPane());
  auiManager_.AddPane(outliner_, 
                      wxAuiPaneInfo().
                      Name("Outliner").
                      Caption("アウトライナ").
                      Left().
                      BestSize(OUTLINER_WIDTH, CANVAS_HEIGHT));
  auiManager_.AddPane(propertyGrid_, 
                      wxAuiPaneInfo().
                      Name("PropertyGrid").
                      Caption("プロパティ").
                      Right().
                      BestSize(PROPERTY_WIDTH, CANVAS_HEIGHT));
  auiManager_.AddPane(log_, 
                      wxAuiPaneInfo().
                      Name("Log").
                      Caption("ログ").
                      Bottom().
                      BestSize(CANVAS_WIDTH, TIMELINE_HEIGHT));
  auiManager_.AddPane(timeLine_, 
                      wxAuiPaneInfo().
                      Name("TimeLine").
                      Caption("タイムライン").
                      Bottom().
                      BestSize(WINDOW_WIDTH, TIMELINE_HEIGHT));
  auiManager_.Update();
  Bind(wxEVT_CLOSE_WINDOW, &MainFrame::onClose, this);
  update();
}
/***********************************************************************//**
	@brief デストラクタ
***************************************************************************/
MainFrame::~MainFrame() {
  auiManager_.UnInit();
}
/***********************************************************************//**
	@brief 
***************************************************************************/
void MainFrame::onSelectNode(Node* node) {
  outliner_->onSelectNode(node);
  propertyGrid_->onSelectNode(node);
  timeLine_->onSelectNode(node);
}
/***********************************************************************//**
	@brief 
***************************************************************************/
void MainFrame::update() {
  outliner_->update();
  propertyGrid_->update();
  canvas_->Refresh();
}
/***********************************************************************//**
	@brief 
***************************************************************************/
void MainFrame::setupMenuBar() {
  wxMenuBar* menuBar = NEW wxMenuBar();
  {
    Menu* menu = NEW Menu();
    menu->append(MENU_FILE_OPEN_PROJECT);
    menu->append(MENU_FILE_SAVE_PROJECT);
    menu->AppendSeparator();
    menu->append(MENU_FILE_EXIT);
    menuBar->Append(menu, "ファイル");
  }
  {
    Menu* menu = NEW Menu();
    menu->append(MENU_EDIT_DELETE);
    menu->AppendSeparator();
    menu->append(MENU_EDIT_UNDO);
    menu->append(MENU_EDIT_REDO);
    menuBar->Append(menu, "編集");
  }
  {
    Menu* menu = NEW Menu();
    menu->append(MENU_MODEL_IMPORT_MODEL);
    menu->append(MENU_MODEL_IMPORT_ANIMATION);
    menu->AppendSeparator();
    menu->append(MENU_MODEL_EXPORT_MODEL);
    menu->append(MENU_MODEL_EXPORT_ANIMATION);
    menuBar->Append(menu, "モデル");
  }
  {
    Menu* menu = NEW Menu();
    menu->append(MENU_VIEW_SKELETON);
    menu->append(MENU_VIEW_NODE);
    menuBar->Append(menu, "表示");
  }
  SetMenuBar(menuBar);
  Bind(wxEVT_MENU_OPEN, &MainFrame::onOpenMenu, this);
  Bind(wxEVT_COMMAND_MENU_SELECTED, &MainFrame::onSelectMenu, this);
}
/***********************************************************************//**
	@brief 
***************************************************************************/
void MainFrame::onOpenMenu(wxMenuEvent& event) {
  Menu::OnOpen(event);
}
/***********************************************************************//**
	@brief 
***************************************************************************/
void MainFrame::onSelectMenu(wxCommandEvent& event) {
  Menu::OnSelect(event);
}
/***********************************************************************//**
	@brief 
***************************************************************************/
void MainFrame::onClose(wxCloseEvent& event) {
  Application::Get()->popLogTarget();
  Destroy();
}
/***********************************************************************//**
	$Id$
***************************************************************************/
}
}
