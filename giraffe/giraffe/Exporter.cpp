﻿/***********************************************************************//**
	@file
***************************************************************************/
#include "stdafx.hpp"
#include "giraffe/Exporter.hpp"
#include "giraffe/gui/Application.hpp"
#include "giraffe/gui/MainFrame.hpp"
#include "giraffe/model/Model.hpp"
#include "giraffe/threejs/Exporter.hpp"
#include "giraffe/xml/Exporter.hpp"

namespace giraffe {
/***********************************************************************//**
	@brief デストラクタ
***************************************************************************/
Exporter::~Exporter() {
}
/***********************************************************************//**
	@brief 相対パスを取得する
	@param[in] _path パス
	@return 出力先ファイルからの相対パス
***************************************************************************/
wxFileName Exporter::getRelativePath(const wxFileName& _path) const {
  wxFileName path(_path.GetFullPath());
  path.MakeRelativeTo(getPath().GetPath());
  return path;
}
/***********************************************************************//**
	@brief 
***************************************************************************/
bool Exporter::ExportNode(Node* node) {
  const wxFileName& path = node->getPath();
  wxFileDialog dialog(gui::Application::Get()->getMainFrame(), 
                      "出力先を指定して下さい", 
                      path.IsOk() ? path.GetPath() : "", 
                      path.IsOk() ? path.GetFullName() : "", 
                      "Giraffeファイル(*.grf)|*.grf", 
                      wxFD_SAVE | wxFD_OVERWRITE_PROMPT);
  if(dialog.ShowModal() == wxID_OK) {
    return ExportNode(node, wxFileName(dialog.GetPath()));
  }
  return false;
}
/***********************************************************************//**
	@brief 
***************************************************************************/
bool Exporter::ExportNode(Node* node, const wxFileName& path) {
  twb::Shared<Exporter> exporter(Create(path));
  if(exporter) {
    exporter->setNode(node);
    exporter->setPath(path);
    return exporter->exportNode();
  }
  return false;
}
/***********************************************************************//**
	@brief 
***************************************************************************/
bool Exporter::ExportModel(model::Model* model) {
  wxFileDialog dialog(gui::Application::Get()->getMainFrame(), 
                      "モデルをエクスポート先を指定して下さい", 
                      "", "", 
                      "THREE.jsファイル(*.threejs)|*.threejs", 
                      wxFD_SAVE | wxFD_OVERWRITE_PROMPT);
  if(dialog.ShowModal() == wxID_OK) {
    wxLogMessage("path = %s", dialog.GetPath());
    return ExportModel(model, wxFileName(dialog.GetPath()));
  }
  return false;
}
/***********************************************************************//**
	@brief 
***************************************************************************/
bool Exporter::ExportModel(model::Model* model, const wxFileName& path) {
  twb::Shared<Exporter> exporter(Create(path));
  if(exporter) {
    exporter->setModel(model);
    exporter->setPath(path);
    return exporter->exportModel();
  }
  return false;
}
/***********************************************************************//**
	@brief コンストラクタ
***************************************************************************/
Exporter::Exporter() {
}
/***********************************************************************//**
	@brief 
***************************************************************************/
bool Exporter::open() {
  wxString path = getPath().GetFullPath();
  wxLogMessage("open '%s'", path);
  out_.reset(NEW wxFileOutputStream(path));
  if(!out_->IsOk()) {
    wxLogError("write error, '%s'", path);
    return false;
  }
  return true;
}
/***********************************************************************//**
	@brief 
***************************************************************************/
wxString Exporter::Join(const wxArrayString& lines, 
                        const wxString& indent) {
  wxString result;
  for(size_t i = 0, n = lines.GetCount(); i < n; i++) {
    result << indent << lines[i];
    if(i + 1 < n) {
      result << ",";
    }
    result << "\n";
  }
  return result;
}
/***********************************************************************//**
	@brief 
***************************************************************************/
void Exporter::setNode(Node* node) {
  node_.reset(node);
}
/***********************************************************************//**
	@brief 
***************************************************************************/
void Exporter::setModel(model::Model* model) {
  model_.reset(model);
}
/***********************************************************************//**
	@brief 
***************************************************************************/
Exporter* Exporter::Create(const wxFileName& path) {
  wxString ext = path.GetExt();
  if(ext == "grf") {
    return NEW xml::Exporter();
  }
  else if(ext == "threejs") {
    return NEW threejs::Exporter();
  }
  return 0;
}
/***********************************************************************//**
	$Id$
***************************************************************************/
}
