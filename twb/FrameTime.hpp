﻿/***********************************************************************//**
	@file
***************************************************************************/
#pragma once

#include "twb/Comparable.hpp"

namespace twb {
typedef double FrameTime_t;
/***********************************************************************//**
	@brief フレーム時間
***************************************************************************/
class FrameTime : public Comparable<FrameTime, FrameTime_t> {
 private:
  FrameTime_t time_;

 public:
  FrameTime()
    : time_(0)
  {}

  FrameTime(FrameTime_t time)
    : time_(time)
  {}

  FrameTime(FrameTime_t frame, FrameTime_t fps)
    : time_(frame / fps)
  {}
  
  FrameTime_t getTime() const {
    return time_;
  }

  FrameTime_t getSecond() const {
    return getTime();
  }

  FrameTime_t getFrame(FrameTime_t fps) const {
    return getTime() * fps;
  }

  FrameTime operator+(const FrameTime& rhs) const {
    return FrameTime(getTime() + rhs.getTime());
  }

  FrameTime operator-(const FrameTime& rhs) const {
    return FrameTime(getTime() - rhs.getTime());
  }

  FrameTime operator*(FrameTime_t rhs) const {
    return FrameTime(getTime() * rhs);
  }

  FrameTime operator/(FrameTime_t rhs) const {
    return FrameTime(getTime() / rhs);
  }

  virtual FrameTime_t compare(const FrameTime& rhs) const {
    return getTime() - rhs.getTime();
  }

  bool isZero() const {
    return getTime() == 0;
  }

  FrameTime update(const FrameTime& now);
  FrameTime update() {
    return update(GetNow());
  }

  static FrameTime GetNow();
  static const FrameTime& GetZero();
};
/***********************************************************************//**
	$Id$
***************************************************************************/
}
