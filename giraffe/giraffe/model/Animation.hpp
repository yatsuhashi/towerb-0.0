﻿/***********************************************************************//**
	@file
***************************************************************************/
#pragma once

#include "giraffe/Node.hpp"

namespace giraffe {
namespace model {
/***********************************************************************//**
	@brief アニメーションノード
***************************************************************************/
class Animation : public Node {
  typedef Node super;

 private:
  int fps_;
  twb::Shared<Skeleton> skeleton_;
  twb::SharedArray<Pose> poses_;

 public:
  Animation();
  virtual ~Animation();

  void setFps(int fps) {
    fps_ = fps;
  }

  int getFps() const {
    return fps_;
  }

  void setSkeleton(Skeleton* skeleton);
  const Skeleton* getSkeleton() const {
    return skeleton_;
  }

  void appendPose(Pose* pose);
  size_t countPose() const {
    return poses_.size();
  }
  const Pose* getPose(size_t index) const {
    return poses_.at(index);
  }
};
/***********************************************************************//**
	$Id$
***************************************************************************/
}
}
