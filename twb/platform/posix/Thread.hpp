﻿/***********************************************************************//**
	@file
***************************************************************************/
#pragma once

#include "twb/thread/ThreadBase.hpp"

namespace twb {
namespace thread {
/***********************************************************************//**
	@brief スレッド
***************************************************************************/
class Thread : public ThreadBase {
 private:
  pthread_t threadId_;

 public:
  virtual ~Thread();

  static void Sleep(double second);
  static void Yield();

 protected:
  Thread();

  virtual void create() override;
  virtual void join() override;

 private:
  static void* Execute(void* arg);
};
/***********************************************************************//**
	$Id$
***************************************************************************/
}
}
