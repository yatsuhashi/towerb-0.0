﻿/***********************************************************************//**
	@file
***************************************************************************/
#include "stdafx.hpp"
#include "giraffe/fbx/Importer.hpp"
#include "giraffe/model/Animation.hpp"
#include "giraffe/model/Bone.hpp"
#include "giraffe/model/Material.hpp"
#include "giraffe/model/Mesh.hpp"
#include "giraffe/model/Model.hpp"
#include "giraffe/model/Pose.hpp"
#include "giraffe/model/Skeleton.hpp"

namespace giraffe {
namespace fbx {
/***********************************************************************//**
	@brief コンストラクタ
***************************************************************************/
Importer::Importer(const wxFileName& path)
  : super(path), 
    manager_(FbxManager::Create()), 
    importer_(FbxImporter::Create(manager_, "Importer")), 
    scene_(FbxScene::Create(manager_, "Scene"))
{
  importer_->Initialize(path.GetFullPath());
  importer_->Import(scene_);
  FbxGlobalSettings& settings = scene_->GetGlobalSettings();
  FbxAxisSystem(FbxAxisSystem::eOpenGL).ConvertScene(scene_);
  timeMode_ = settings.GetTimeMode();
  period_.SetTime(0, 0, 0, 1, 0, timeMode_);
  fps_ = int(1 / period_.GetSecondDouble() + 0.5);
}
/***********************************************************************//**
	@brief デストラクタ
***************************************************************************/
Importer::~Importer() {
  manager_->Destroy();
}
/***********************************************************************//**
	@brief 
***************************************************************************/
model::Model* Importer::importModel() {
  FbxGeometryConverter converter(manager_);
  converter.Triangulate(scene_, true);
  model::Model* model = NEW model::Model();
  model->setName(getPath().GetName());
  FbxNode* root = scene_->GetRootNode();
  parseMaterial(model);
  model::Skeleton* skeleton = parseSkeleton(root);
  model->setSkeleton(skeleton);
  model->setBasePose(parsePose(skeleton, root));
  parseMesh(model, root);
  return model;
}
/***********************************************************************//**
	@brief 
***************************************************************************/
model::Animation* Importer::importAnimation() {
  if(importer_->GetAnimStackCount() > 0) {
    model::Animation* animation = 
      parseAnimation(importer_->GetTakeInfo(0), scene_->GetRootNode());
    animation->setName(getPath().GetName());
    return animation;
  }
  return 0;
}
/***********************************************************************//**
	@brief 
***************************************************************************/
void Importer::parseMaterial(model::Model* model) {
  for(int i = 0, n = scene_->GetMaterialCount(); i < n; i++) {
    FbxSurfaceMaterial* materialData = scene_->GetMaterial(i);
    model::Material* material = NEW model::Material();
    material->setName(materialData->GetName());
    FbxProperty property = 
      materialData->FindProperty(FbxSurfaceMaterial::sDiffuse);
    for(int j = 0, n = property.GetSrcObjectCount<FbxFileTexture>();
        j < n; j++) {
      FbxFileTexture* textureData = 
        property.GetSrcObject<FbxFileTexture>(j);
      wxFileName path = getImagePath(textureData->GetRelativeFileName());
      material->setTexture(model::Material::TYPE_DIFFUSE, path);
    }
    model->appendMaterial(material);
  }
}
/***********************************************************************//**
	@brief 
***************************************************************************/
void Importer::parseMesh(model::Model* model, FbxNode* node) {
  if(FbxMesh* meshData = node->GetMesh()) {
    model::Mesh* mesh = NEW model::Mesh();
    mesh->setName(node->GetName());

    twb::SharedArray<model::Geometry> geometries;
    for(int i = 0, n = node->GetMaterialCount(); i < n; i++) {
      FbxSurfaceMaterial* materialData = node->GetMaterial(i);
      if(model::Material* material = 
         model->findMaterial(materialData->GetName())) {
        model::Geometry* geometry = NEW model::Geometry(material);
        geometries.push_back(geometry);
        mesh->appendGeometry(geometry);
      }
      else {
        wxLogWarning("no such material, '%s'", materialData->GetName());
      }
    }
    parseSkin(model, meshData);

    meshData->GetUVSetNames(uvSetNames_);

    FbxLayerElementArrayTemplate<int>* materials = 0;
    meshData->GetMaterialIndices(&materials);

    for(int i = 0, n = meshData->GetPolygonCount(); i < n; i++) {
      model::Geometry::Vertex vertexes[3];
      for(int j = 0; j < 3; j++) {
        parseVertex(vertexes[j], meshData, i, j);
      }
      int material = materials ? materials->GetAt(i) : 0;
      geometries[material]->
        appendTriangle(vertexes[0], vertexes[1], vertexes[2]);
    }
    model->appendMesh(mesh);
  }
  for(int i = 0, n = node->GetChildCount(); i < n; i++) {
    parseMesh(model, node->GetChild(i));
  }
}
/***********************************************************************//**
	@brief 
***************************************************************************/
void Importer::parseSkin(model::Model* model, FbxMesh* mesh) {
  int num = mesh->GetControlPointsCount();
  vertexes_.clear();
  vertexes_.resize(num, model::Geometry::Vertex());
  for(int i = 0; i < num; i++) {
    model::Geometry::Vertex& vertex = vertexes_[i];
    FbxVector4 pos = mesh->GetControlPointAt(i);
    vertex.pos = glm::vec3(pos[0], pos[1], pos[2]) * getScale();
  }
  FbxSkin* skin = 
    static_cast<FbxSkin*>(mesh->GetDeformer(0, FbxDeformer::eSkin));
  const model::Skeleton* skeleton = model->getSkeleton();
  for(int i = 0, n = skin->GetClusterCount(); i < n; i++) {
    FbxCluster* cluster = skin->GetCluster(i);
    const char* name = cluster->GetLink()->GetName();
    const model::Bone* bone = skeleton->findBone(name);
    if(bone) {
      int boneIndex = bone->getIndex();
      int* indexes = cluster->GetControlPointIndices();
      double* weights = cluster->GetControlPointWeights();
      for(int j = 0, n = cluster->GetControlPointIndicesCount();
          j < n; j++) {
        vertexes_[indexes[j]].weights.push_back
          (model::Geometry::Weight(boneIndex, weights[j]));
      }
    }
  }
}
/***********************************************************************//**
	@brief 
***************************************************************************/
void Importer::parseVertex(model::Geometry::Vertex& vertex, 
                           FbxMesh* mesh, 
                           int polygonIndex, 
                           int vertexIndex) {
  vertex = vertexes_[mesh->GetPolygonVertex(polygonIndex, vertexIndex)];
  {
    FbxVector2 uv;
    bool isUnmap;
    if(mesh->GetPolygonVertexUV(polygonIndex, vertexIndex, 
                                uvSetNames_[0], uv, isUnmap)) {
      if(!isUnmap) {
        vertex.uv = glm::vec2(uv[0], 1.0f - uv[1]);
      }
    }
  }
  {
    FbxVector4 normal;
    if(mesh->GetPolygonVertexNormal(polygonIndex, vertexIndex, normal)) {
      vertex.normal = 
        glm::normalize(glm::vec3(normal[0], normal[1], normal[2]) * 
                       getScale());
    }
  }
}
/***********************************************************************//**
	@brief 
***************************************************************************/
model::Skeleton* Importer::parseSkeleton(FbxNode* node) {
  model::Skeleton* skeleton = NEW model::Skeleton();
  parseBone(skeleton, node);
  return skeleton;
}
/***********************************************************************//**
	@brief 
***************************************************************************/
void Importer::parseBone(model::Skeleton* skeleton, FbxNode* node, 
                         model::Bone* parent) {
  model::Bone* bone = NEW model::Bone();
  bone->setName(node->GetName());
  if(parent) {
    parent->appendChild(bone);
  }
  else {
    skeleton->appendChild(bone);
  }
  for(int i = 0, n = node->GetChildCount(); i < n; i++) {
    parseBone(skeleton, node->GetChild(i), bone);
  }
}
/***********************************************************************//**
	@brief 
***************************************************************************/
model::Pose* Importer::parsePose(model::Skeleton* skeleton, 
                                 FbxNode* root, 
                                 const FbxTime& time) {
  model::Pose* pose = NEW model::Pose(skeleton);
  for(size_t i = 0, n = skeleton->countBone(); i < n; i++) {
    const model::Bone* bone = skeleton->getBone(i);
    if(FbxNode* node = root->FindChild(bone->getName())) {
      FbxAMatrix matrix = node->EvaluateLocalTransform(time);
      glm::dmat4 m = glm::make_mat4<double>(matrix);
      m[3] *= glm::dvec4(getScale(), 1.0);
      pose->getMatrix(bone->getIndex()) = m;
    }
  }
  return pose;
}
/***********************************************************************//**
	@brief 
***************************************************************************/
model::Animation*
Importer::parseAnimation(const FbxTakeInfo* take, FbxNode* root) {
  model::Animation* animation = NEW model::Animation();
  animation->setFps(fps_);
  model::Skeleton* skeleton = parseSkeleton(root);
  animation->setSkeleton(skeleton);
  for(FbxTime time = take->mLocalTimeSpan.GetStart(), 
        endTime = take->mLocalTimeSpan.GetStop();
      time <= endTime;
      time += period_) {
    model::Pose* pose = parsePose(skeleton, root, time);
    animation->appendPose(pose);
  }
  return animation;
}
/***********************************************************************//**
	$Id$
***************************************************************************/
}
}
