﻿/***********************************************************************//**
	@file
***************************************************************************/
#pragma once

namespace twb {
namespace thread {
/***********************************************************************//**
	@brief スレッド基底クラス
***************************************************************************/
class ThreadBase {
 private:
  enum {
    FLAG_STOP, 
    FLAG_MAX
  };

 private:
  std::bitset<FLAG_MAX> flag_;

 public:
  virtual ~ThreadBase();

  void start();
  void stop();

 protected:
  ThreadBase() {}

  bool isStop() const {
    return flag_.test(FLAG_STOP);
  }

  virtual void onStart() {}
  virtual void onStop() {}
  virtual void execute() = 0;

  /**
   * スレッドを生成する
   */
  virtual void create() = 0;

  /**
   * スレッドの終了を待つ
   */
  virtual void join() = 0;
};
/***********************************************************************//**
	$Id$
***************************************************************************/
}
}
