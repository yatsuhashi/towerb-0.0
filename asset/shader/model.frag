#ifdef GL_ES
precision mediump float;
#endif

uniform sampler2D u_texture;
uniform vec4 u_color;
uniform vec2 u_uvOffset;

varying vec2 v_uv;
varying vec3 v_normal;
varying vec4 v_color;

void main() {
  vec4 color = texture2D(u_texture, v_uv);
  gl_FragColor = color * v_color;
}
