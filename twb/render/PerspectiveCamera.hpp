﻿/***********************************************************************//**
	@file
***************************************************************************/
#pragma once

#include "twb/render/Camera.hpp"

namespace twb {
namespace render {
/***********************************************************************//**
	@brief 透視投影(3D)カメラ
***************************************************************************/
class PerspectiveCamera : public Camera {
  typedef Camera super;

 private:
  float fov_;
  float near_;
  float far_;
  glm::vec3 eye_;
  glm::vec3 at_;
  glm::vec3 up_;
  glm::vec2 projectionSize_;

 public:
  PerspectiveCamera();
  virtual ~PerspectiveCamera() {}

  PerspectiveCamera& setFov(float fov) {
    fov_ = fov;
    return *this;
  }

  float getFov() const {
    return fov_;
  }

  PerspectiveCamera& setNear(float near) {
    near_ = near;
    return *this;
  }

  float getNear() const {
    return near_;
  }

  PerspectiveCamera& setFar(float far) {
    far_ = far;
    return *this;
  }

  float getFar() const {
    return far_;
  }

  PerspectiveCamera& setEye(const glm::vec3& eye) {
    eye_ = eye;
    return *this;
  }

  const glm::vec3& getEye() const {
    return eye_;
  }

  PerspectiveCamera& setAt(const glm::vec3& at) {
    at_ = at;
    return *this;
  }

  const glm::vec3& getAt() const {
    return at_;
  }

  PerspectiveCamera& setUp(const glm::vec3& up) {
    up_ = up;
    return *this;
  }

  const glm::vec3& getUp() const {
    return up_;
  }

  virtual void updateMatrix() override;
};
/***********************************************************************//**
	$Id$
***************************************************************************/
}
}
