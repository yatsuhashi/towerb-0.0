﻿/***********************************************************************//**
	@file
***************************************************************************/
#pragma once

namespace giraffe {
/***********************************************************************//**
	@brief コンテキスト
***************************************************************************/
class Context : public twb::canvas::Context {
  typedef twb::canvas::Context super;

 private:
  twb::Shared<twb::render::Shader> defaultShader_;
  twb::Shared<twb::render::Shader> modelShader_;
  twb::Shared<twb::render::Material> defaultMaterial_;
  twb::Shared<twb::render::geometry::Axis> axis_;
  twb::Shared<twb::render::Image> dummyImage_;

 public:
  Context();
  virtual ~Context();

  twb::render::Shader* getDefaultShader() const {
    return defaultShader_;
  }

  twb::render::Shader* getModelShader() const {
    return modelShader_;
  }

  twb::render::Material* getDefaultMaterial() const {
    return defaultMaterial_;
  }

  twb::render::Image* getDummyImage() const {
    return dummyImage_;
  }

  void renderAxis(const glm::mat4& matrix);

 protected:
  virtual void onUpdate() override;
  virtual void onSetupRenderer() override;
  virtual void onRender() override;

 private:
  void createDummyImage();
};
/***********************************************************************//**
	$Id$
***************************************************************************/
}
