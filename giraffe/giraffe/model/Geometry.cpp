﻿/***********************************************************************//**
	@file
***************************************************************************/
#include "stdafx.hpp"
#include "giraffe/Application.hpp"
#include "giraffe/Context.hpp"
#include "giraffe/Project.hpp"
#include "giraffe/model/Geometry.hpp"
#include "giraffe/model/Material.hpp"
#include "giraffe/property/Property.hpp"

namespace giraffe {
namespace model {
/***********************************************************************//**
	@brief コンストラクタ
***************************************************************************/
Geometry::Geometry(Material* material)
  : super("model.Geometry"), 
    material_(material)
{
  property::Property* property = getProperty();
  property->appendMember("vertexNum", "頂点数", int(0))->
    setReadOnly(true);
  property->appendMember("polygonNum", "ポリゴン数", int(0))->
    setReadOnly(true);
  setName(Application::Get()->getProject()->createName("Geometry"));
}
/***********************************************************************//**
	@brief デストラクタ
***************************************************************************/
Geometry::~Geometry() {
}
/***********************************************************************//**
	@brief 
***************************************************************************/
void Geometry::appendTriangle(const Vertex& p1, 
                              const Vertex& p2, 
                              const Vertex& p3) {
  vertexes_.push_back(p1);
  vertexes_.push_back(p2);
  vertexes_.push_back(p3);
}
/***********************************************************************//**
	@brief 
***************************************************************************/
twb::render::Geometry* Geometry::createGeometry(Context* context) {
  twb::render::Material* material = material_->getMaterial(context);
  twb::render::Geometry* geometry = NEW twb::render::Geometry(material);
  for(size_t i = 0, n = vertexes_.size(); i < n; i++) {
    twb::render::Geometry::Vertex des;
    createRenderVertex(des, vertexes_.at(i));
    auto iter = 
      std::find(renderVertexes_.begin(), renderVertexes_.end(), des);
    if(iter == renderVertexes_.end()) {
      renderIndexes_.push_back(uint16_t(renderVertexes_.size()));
      renderVertexes_.push_back(des);
    }
    else {
      renderIndexes_.push_back(uint16_t(iter - renderVertexes_.begin()));
    }
  }
  geometry->setupVertex(renderVertexes_.size(), renderVertexes_.data());
  geometry->setupIndex(twb::render::Geometry::MODE_TRIANGLES, 
                       renderIndexes_.size(), renderIndexes_.data());
  for(auto index : matrixIndexes_) {
    geometry->getMatrixIndexes().push_back(index);
  }
  getProperty()->setValue("vertexNum", int(renderVertexes_.size()));
  getProperty()->setValue("polygonNum", int(renderIndexes_.size() / 3));
  vertexes_.clear();
  geometry_.reset(geometry);
  return geometry;
}
/***********************************************************************//**
	@brief 
***************************************************************************/
void Geometry::onUpdateProperty(const wxString& name) {
  super::onUpdateProperty(name);
  if(geometry_) {
    geometry_->setShow(isShow());
  }
}
/***********************************************************************//**
	@brief 
***************************************************************************/
void Geometry::createRenderVertex(twb::render::Geometry::Vertex& des, 
                                  Vertex& src) {
  des.pos = src.pos;
  des.uv = src.uv;
  des.setNormal(src.normal);
  des.setColor(src.color);
  std::sort(src.weights.begin(), src.weights.end());
  size_t weightNum = src.weights.size();
  if(weightNum > twb::render::Geometry::WEIGHT_MAX) {
    wxLogWarning("頂点のウェイトは%dつのボーンまでです", 
                 twb::render::Geometry::WEIGHT_MAX);
    weightNum = twb::render::Geometry::WEIGHT_MAX;
  }
  for(size_t i = 0; i < weightNum; i++) {
    const Weight& weight = src.weights[i];
    des.matrixIndexes[i] = getMatrixIndex(weight.bone);
    des.matrixWeights[i] = int(weight.weight * 0xffff) >> 8;
  }
}
/***********************************************************************//**
	@brief 
***************************************************************************/
int Geometry::getMatrixIndex(int index) {
  auto iter = 
    std::find(matrixIndexes_.begin(), matrixIndexes_.end(), index);
  if(iter != matrixIndexes_.end()) {
    return int(iter - matrixIndexes_.begin());
  }
  matrixIndexes_.push_back(index);
  return int(matrixIndexes_.size() - 1);
}
/***********************************************************************//**
	$Id$
***************************************************************************/
}
}
