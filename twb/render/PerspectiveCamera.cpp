﻿/***********************************************************************//**
	@file
***************************************************************************/
#include "stdafx.hpp"
#include "twb/render/PerspectiveCamera.hpp"

namespace twb {
namespace render {
/***********************************************************************//**
	@brief コンストラクタ
***************************************************************************/
PerspectiveCamera::PerspectiveCamera()
  : fov_(30.0f), 
    near_(1.0f), 
    far_(10000.0f), 
    eye_(0.0f, 0.0f, 0.0f), 
    at_(0.0f, 0.0f, 1.0f), 
    up_(0.0f, 1.0f, 0.0f)
{}
/***********************************************************************//**
	@brief 
***************************************************************************/
void PerspectiveCamera::updateMatrix() {
  int w = getViewport().getSize().x;
  int h = getViewport().getSize().y;
  float size = glm::tan(fov_ * 0.5f);
  if(w > h) {
    projectionSize_.x = size * w / h;
    projectionSize_.y = size;
  }
  else {
    projectionSize_.x = size;
    projectionSize_.y = size * h / w;
  }
  setProjectionMatrix(glm::frustum(-projectionSize_.x, 
                                   projectionSize_.x, 
                                   -projectionSize_.y, 
                                   projectionSize_.y, 
                                   near_, far_));
  setModelViewMatrix(glm::lookAt(eye_, at_, up_));
}
/***********************************************************************//**
	$Id$
***************************************************************************/
}
}
