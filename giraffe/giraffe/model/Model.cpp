﻿/***********************************************************************//**
	@file
***************************************************************************/
#include "stdafx.hpp"
#include "giraffe/Context.hpp"
#include "giraffe/gui/Application.hpp"
#include "giraffe/gui/Option.hpp"
#include "giraffe/model/Animation.hpp"
#include "giraffe/model/Bone.hpp"
#include "giraffe/model/Material.hpp"
#include "giraffe/model/Mesh.hpp"
#include "giraffe/model/Model.hpp"
#include "giraffe/model/Pose.hpp"
#include "giraffe/model/Skeleton.hpp"
#include "giraffe/property/Property.hpp"

namespace giraffe {
namespace model {
/***********************************************************************//**
	@brief コンストラクタ
***************************************************************************/
Model::Model()
  : super("model.Model"), 
    frame_(0)
{
  setCanDelete(true);
  property::Property* property = getProperty();
  property->appendMember("vertexNum", "頂点数", int(0))->
    setReadOnly(true);
  property->appendMember("polygonNum", "ポリゴン数", int(0))->
    setReadOnly(true);
}
/***********************************************************************//**
	@brief デストラクタ
***************************************************************************/
Model::~Model() {
}
/***********************************************************************//**
	@brief 
***************************************************************************/
Model& Model::setSkeleton(Skeleton* skeleton) {
  TWB_ASSERT(skeleton);
  if(skeleton_) {
    removeChild(skeleton_);
  }
  skeleton_.reset(skeleton);
  appendChild(skeleton);
  return *this;
}
/***********************************************************************//**
	@brief 
***************************************************************************/
void Model::setBasePose(Pose* pose) {
  basePose_.reset(pose);
  renderPose_.reset(pose->createGlobalPose());
  inverseBasePose_.reset(NEW Pose(*renderPose_.get()));
  inverseBasePose_->inverse();
}
/***********************************************************************//**
	@brief 
***************************************************************************/
void Model::setPose(const Skeleton* skeleton, const Pose* pose) {
}
/***********************************************************************//**
	@brief 
***************************************************************************/
void Model::appendMaterial(Material* material) {
  material->setIndex(int(countMaterial()));
  getFolder("Material")->appendChild(material);
}
/***********************************************************************//**
	@brief 
***************************************************************************/
Material* Model::findMaterial(const wxString& name) {
  return static_cast<Material*>(getFolder("Material")->findChild(name));
}
/***********************************************************************//**
	@brief 
***************************************************************************/
size_t Model::countMaterial() const {
  return countFolderNode("Material");
}
/***********************************************************************//**
	@brief 
***************************************************************************/
Material* Model::getMaterial(size_t index) const {
  return static_cast<Material*>(getFolderNode("Material", index));
}
/***********************************************************************//**
	@brief 
***************************************************************************/
void Model::appendMesh(Mesh* mesh) {
  getFolder("Mesh")->appendChild(mesh);
}
/***********************************************************************//**
	@brief 
***************************************************************************/
size_t Model::countMesh() const {
  return countFolderNode("Mesh");
}
/***********************************************************************//**
	@brief 
***************************************************************************/
Mesh* Model::getMesh(size_t index) const {
  return static_cast<Mesh*>(getFolderNode("Mesh", index));
}
/***********************************************************************//**
	@brief 
***************************************************************************/
void Model::appendAnimation(Animation* animation) {
  getFolder("Animation")->appendChild(animation);
  setAnimation(animation);
}
/***********************************************************************//**
	@brief 
***************************************************************************/
void Model::setAnimation(Animation* animation) {
  animation_.reset(animation);
  animationBones_.clear();
  if(const Skeleton* skeleton = getSkeleton()) {
    for(size_t i = 0, n = skeleton->countBone(); i < n; i++) {
      const wxString& name = skeleton->getBone(i)->getName();
      animationBones_.push_back(animation->getSkeleton()->findBone(name));
    }
  }
  frame_ = 0;
}
/***********************************************************************//**
	@brief 
***************************************************************************/
size_t Model::countAnimation() const {
  return countFolderNode("Animation");
}
/***********************************************************************//**
	@brief 
***************************************************************************/
Animation* Model::getAnimation(size_t index) const {
  return static_cast<Animation*>(getFolderNode("Animation", index));
}
/***********************************************************************//**
	@brief 
***************************************************************************/
void Model::update(Context* context) {
  super::update(context);
  if(model_ && animation_) {
    updateAnimation();
  }
}
/***********************************************************************//**
	@brief 
***************************************************************************/
void Model::render(Context* context) {
  if(!model_) {
    createModel(context);
  }
  twb::render::Renderer* renderer = context->getRenderer();
  renderer->setModelMatrix(glm::mat4());
  model_->render(renderer);
  {
    gui::Option* option = gui::Application::Get()->getOption();
    if(option->getBool("MENU_VIEW_NODE")) {
      renderNode(context);
    }
    if(option->getBool("MENU_VIEW_SKELETON")) {
      renderSkeleton(context);
    }
  }
}
/***********************************************************************//**
	@brief 
***************************************************************************/
void Model::createModel(Context* context) {
  int vertexNum = 0;
  int polygonNum = 0;
  model_.reset(NEW twb::render::Model(getSkeleton()->countBone()));
  Node* folder = getFolder("Mesh");
  for(Node* iter = folder->getChild(); iter; iter = iter->getNext()) {
    Mesh* mesh = static_cast<Mesh*>(iter);
    model_->appendMesh(mesh->createMesh(context));
    vertexNum += 
      mesh->getProperty()->getValue("vertexNum").As<int>();
    polygonNum += 
      mesh->getProperty()->getValue("polygonNum").As<int>();
  }
  getProperty()->setValue("vertexNum", vertexNum);
  getProperty()->setValue("polygonNum", polygonNum);
}
/***********************************************************************//**
	@brief 
***************************************************************************/
void Model::updateAnimation() {
  frame_++;
  if(frame_ >= animation_->countPose()) {
    frame_ = 0;
  }
  const Pose* pose = animation_->getPose(frame_);
  const Skeleton* skeleton = getSkeleton();
  for(size_t i = 0, n = skeleton->countBone(); i < n; i++) {
    glm::mat4 m;
    if(const Bone* bone = skeleton->getBone(i)) {
      if(const Bone* parent = bone->getParentBone()) {
        m = renderPose_->getMatrix(parent->getIndex());
      }
    }
    if(const Bone* bone = animationBones_[i]) {
      m *= pose->getMatrix(bone->getIndex());
    }
    else {
      m *= basePose_->getMatrix(i);
    }
    renderPose_->getMatrix(i) = m;
    model_->getMatrix(i) = m * inverseBasePose_->getMatrix(i);
  }
}
/***********************************************************************//**
	@brief 
***************************************************************************/
void Model::renderNode(Context* context) {
  for(size_t i = 0, n = renderPose_->countMatrix(); i < n; i++) {
    context->renderAxis(glm::scale(renderPose_->getMatrix(i), 
                                   glm::vec3(0.1f)));
  }
}
/***********************************************************************//**
	@brief 
***************************************************************************/
void Model::renderSkeleton(Context* context) {
  std::vector<twb::render::Geometry::Vertex> vertexes;
  std::vector<uint16_t> indexes;
  const Skeleton* skeleton = getSkeleton();
  for(size_t i = 0, n = skeleton->countBone(); i < n; i++) {
    if(const Bone* bone = skeleton->getBone(i)) {
      if(const Bone* parent = bone->getParentBone()) {
        twb::render::Geometry::Vertex vertex;
        vertex.pos = 
          glm::vec3(renderPose_->getMatrix(bone->getIndex())[3]);
        indexes.push_back(vertexes.size());
        vertexes.push_back(vertex);
        vertex.pos = 
          glm::vec3(renderPose_->getMatrix(parent->getIndex())[3]);
        indexes.push_back(vertexes.size());
        vertexes.push_back(vertex);
      }
    }
  }
  if(vertexes.empty()) {
    return;
  }
  twb::render::Renderer* renderer = context->getRenderer();
  twb::Shared<twb::render::Geometry>
    geometry(NEW twb::render::Geometry(context->getDefaultMaterial()));
  geometry->setupVertex(vertexes.size(), vertexes.data());
  geometry->setupIndex(twb::render::Geometry::MODE_LINES, 
                       indexes.size(), indexes.data());
  renderer->beginShader(context->getDefaultShader());
  renderer->setModelMatrix(glm::mat4());
  geometry->render(renderer);
}
/***********************************************************************//**
	$Id$
***************************************************************************/
}
}
