﻿/***********************************************************************//**
	@file
***************************************************************************/
#pragma once

#include "twb/Rect.hpp"

namespace twb {
namespace canvas {
/***********************************************************************//**
	@brief レイアウト
***************************************************************************/
class Layout {
 private:
  glm::vec2 pos_;
  glm::vec2 size_;
  glm::vec2 alignment_;
  IRect rect_;

 public:
  Layout() {}
  ~Layout() {}

  Layout& setPos(const glm::vec2& pos) {
    pos_ = pos;
    return *this;
  }

  const glm::vec2& getPos() const {
    return pos_;
  }

  Layout& setSize(const glm::vec2& size) {
    size_ = size;
    return *this;
  }

  const glm::vec2& getSize() const {
    return size_;
  }

  Layout& setAlignment(const glm::vec2& alignment) {
    alignment_ = alignment;
    return *this;
  }

  const glm::vec2& getAlignment() const {
    return alignment_;
  }

  void update(const IRect& rect);

  const IRect& getRect() const {
    return rect_;
  }

 private:
  static glm::ivec2 RelativeToAbsolute(const glm::ivec2& size, 
                                       const glm::vec2& v);
};
/***********************************************************************//**
	$Id$
***************************************************************************/
}
}
