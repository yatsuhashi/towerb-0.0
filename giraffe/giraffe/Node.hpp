﻿/***********************************************************************//**
	@file
***************************************************************************/
#pragma once

#include "twb/TreeNode.hpp"

namespace giraffe {
/***********************************************************************//**
	@brief ノード
***************************************************************************/
class Node : public twb::TreeNodeBase<Node> {
  typedef twb::TreeNodeBase<Node> super;

 private:
  enum {
    FLAG_ERROR, 
    FLAG_CAN_DELETE, 
    FLAG_MAX
  };

 private:
  wxString type_;
  int index_;
  twb::Shared<property::Property> property_;
  wxFileName path_;
  std::bitset<FLAG_MAX> flag_;

 public:
  Node(const wxString& type);
  virtual ~Node();

  const wxString& getType() const {
    return type_;
  }

  Node& setIndex(int index) {
    index_ = index;
    return *this;
  }

  int getIndex() const {
    return index_;
  }

  property::Property* getProperty() const {
    return property_;
  }

  virtual void onUpdateProperty(const wxString& name) {}

  Node& setName(const wxString& name);
  wxString getName() const;

  Node& setPath(const wxFileName& path);
  const wxFileName& getPath() const {
    return path_;
  }

  Node& setExpand(bool isExpand);
  bool isExpand() const;

  Node& setShow(bool isShow);
  bool isShow() const;

  Node& setError(bool isError) {
    flag_.set(FLAG_ERROR, isError);
    return *this;
  }

  bool isError() const {
    return flag_.test(FLAG_ERROR);
  }

  bool canDelete() const {
    return flag_.test(FLAG_CAN_DELETE);
  }

  template <class T>
  T* getNode() {
    if(T* node = dynamic_cast<T*>(this)) {
      return node;
    }
    return getParent() ? getParent()->getNode<T>() : 0;
  }

  void appendChild(Node* child);
  Node* findChild(const wxString& name, bool isRecursive = false) const;

  Node* getFolder(const wxString& name);
  Node* getFolder(const wxString& name) const;

  virtual void update(Context* context);
  virtual void render(Context* context);

 protected:
  virtual void onAppendChild(Node* child) {}
  virtual void onUpdateTree() override;

  size_t countFolderNode(const wxString& name) const;
  Node* getFolderNode(const wxString& name, size_t index) const;

  void setCanDelete(bool canDelete) {
    flag_.set(FLAG_CAN_DELETE, canDelete);
  }
};
/***********************************************************************//**
	$Id$
***************************************************************************/
}
