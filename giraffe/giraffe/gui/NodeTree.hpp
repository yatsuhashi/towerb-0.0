﻿/***********************************************************************//**
	@file
***************************************************************************/
#pragma once

namespace giraffe {
namespace gui {
/***********************************************************************//**
	@brief ノードツリー
***************************************************************************/
class NodeTree : public wxTreeCtrl {
  typedef wxTreeCtrl super;

 public:
  NodeTree(wxWindow* parent);
  virtual ~NodeTree();

  void update(Node* node);
  void onSelectNode(Node* node);

 private:
  void appendNode(Node* node, wxTreeItemId parent = wxTreeItemId());

  void onSelect(wxTreeEvent& event);
  void onCollapse(wxTreeEvent& event);
  void onExpand(wxTreeEvent& event);

  Node* getNode(const wxTreeEvent& event);
  Node* getNode(const wxTreeItemId& id);

  wxTreeItemId getId(const Node* node);
  wxTreeItemId getId(const wxTreeItemId& id, const Node* node);
};
/***********************************************************************//**
	$Id$
***************************************************************************/
}
}
