﻿/***********************************************************************//**
	@file
***************************************************************************/
#include "stdafx.hpp"
#include "twb/canvas/Layout.hpp"

namespace twb {
namespace canvas {
/***********************************************************************//**
	@brief 更新
***************************************************************************/
void Layout::update(const IRect& rect) {
  glm::ivec2 size = RelativeToAbsolute(rect.getSize(), size_);
  if(size.x <= 0) {
    size.x += rect.getSize().x;
  }
  if(size.y <= 0) {
    size.y += rect.getSize().y;
  }
  rect_.setSize(size);
  glm::vec2 offset = glm::vec2(rect.getSize() - size) * alignment_;
  rect_.setPos(RelativeToAbsolute(rect.getSize(), pos_) + 
               rect.getPos() + glm::ivec2(offset));
}
/***********************************************************************//**
	@brief 相対値を絶対値に変換する
***************************************************************************/
glm::ivec2 Layout::RelativeToAbsolute(const glm::ivec2& size, 
                                      const glm::vec2& v) {
  return glm::ivec2(int((glm::abs(v.x) < 1.0f) ? size.x * v.x : v.x), 
                    int((glm::abs(v.y) < 1.0f) ? size.y * v.y : v.y));
}
/***********************************************************************//**
	$Id$
***************************************************************************/
}
}
