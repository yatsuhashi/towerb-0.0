﻿/***********************************************************************//**
	@file
***************************************************************************/
#include "stdafx.hpp"
#include "twb/render/Camera.hpp"

namespace twb {
namespace render {
/***********************************************************************//**
	@brief 
***************************************************************************/
Camera::Camera()
  : viewport_(glm::ivec2(0, 0), glm::ivec2(0, 0))
{}
/***********************************************************************//**
	@brief ワールド座標をスクリーン座標に変換する
***************************************************************************/
glm::vec3 Camera::worldToScreen(const glm::vec3& pos) const {
  const IRect& viewport = getViewport();
  return glm::project(pos, getModelViewMatrix(), getProjectionMatrix(), 
                      glm::vec4(viewport.getPos(), viewport.getSize()));
}
/***********************************************************************//**
	@brief スクリーン座標をワールド座標に変換する
***************************************************************************/
glm::vec3 Camera::screenToWorld(const glm::vec3& pos) const {
  const IRect& viewport = getViewport();
  return glm::unProject(glm::vec3(pos.x - viewport.getLeft(), 
                                  viewport.getBottom() - pos.y, 
                                  pos.z), 
                        getModelViewMatrix(), 
                        getProjectionMatrix(), 
                        glm::vec4(viewport.getPos(), viewport.getSize()));
}
/***********************************************************************//**
	@brief 
***************************************************************************/
void Camera::setModelViewMatrix(const glm::mat4& matrix) {
  modelViewMatrix_ = matrix;
}
/***********************************************************************//**
	$Id$
***************************************************************************/
}
}
