﻿/***********************************************************************//**
	@file
***************************************************************************/
#include "stdafx.hpp"
#include "giraffe/Project.hpp"
#include "giraffe/gui/Application.hpp"
#include "giraffe/gui/NodeTree.hpp"

namespace giraffe {
namespace gui {
class Item : public wxTreeItemData {
 public:
  Node* node;

 public:
  Item(Node* node)
    : node(node)
  {}
};
/***********************************************************************//**
	@brief コンストラクタ
***************************************************************************/
NodeTree::NodeTree(wxWindow* parent)
  : super(parent)
{
  Bind(wxEVT_TREE_SEL_CHANGED, &NodeTree::onSelect, this);
  Bind(wxEVT_TREE_ITEM_COLLAPSED, &NodeTree::onCollapse, this);
  Bind(wxEVT_TREE_ITEM_EXPANDED, &NodeTree::onExpand, this);
}
/***********************************************************************//**
	@brief デストラクタ
***************************************************************************/
NodeTree::~NodeTree() {
}
/***********************************************************************//**
	@brief 更新
***************************************************************************/
void NodeTree::update(Node* node) {
  Freeze();
  DeleteAllItems();
  appendNode(node);
  Thaw();
}
/***********************************************************************//**
	@brief 
***************************************************************************/
void NodeTree::onSelectNode(Node* node) {
  wxTreeItemId id = getId(node);
  if(id.IsOk() && id != GetSelection()) {
    EnsureVisible(id);
    SelectItem(id);
  }
}
/***********************************************************************//**
	@brief 
***************************************************************************/
void NodeTree::appendNode(Node* node, wxTreeItemId parent) {
  wxTreeItemId id = 
    parent.IsOk()
    ? AppendItem(parent, node->getName())
    : AddRoot(node->getName());
  SetItemData(id, NEW Item(node));
  for(auto child = node->getChild(); child; child = child->getNext()) {
    appendNode(child, id);
  }
  if(node->isExpand()) {
    Expand(id);
  }
}
/***********************************************************************//**
	@brief 
***************************************************************************/
void NodeTree::onSelect(wxTreeEvent& event) {
  if(Node* node = getNode(event)) {
    Application::Get()->selectNode(node);
  }
}
/***********************************************************************//**
	@brief 
***************************************************************************/
void NodeTree::onCollapse(wxTreeEvent& event) {
  if(Node* node = getNode(event)) {
    node->setExpand(false);
  }
}
/***********************************************************************//**
	@brief 
***************************************************************************/
void NodeTree::onExpand(wxTreeEvent& event) {
  if(Node* node = getNode(event)) {
    node->setExpand(true);
  }
}
/***********************************************************************//**
	@brief 
***************************************************************************/
Node* NodeTree::getNode(const wxTreeEvent& event) {
  return getNode(event.GetItem());
}
/***********************************************************************//**
	@brief 
***************************************************************************/
Node* NodeTree::getNode(const wxTreeItemId& id) {
  if(!id.IsOk()) {
    return 0;
  }
  const Item* item = static_cast<Item*>(GetItemData(id));
  return item ? item->node : 0;
}
/***********************************************************************//**
	@brief 
***************************************************************************/
wxTreeItemId NodeTree::getId(const Node* node) {
  return getId(GetRootItem(), node);
}
/***********************************************************************//**
	@brief 
***************************************************************************/
wxTreeItemId NodeTree::getId(const wxTreeItemId& id, const Node* node) {
  if(getNode(id) == node) {
    return id;
  }
  wxTreeItemIdValue cookie;
  for(wxTreeItemId iter = GetFirstChild(id, cookie);
      iter.IsOk();
      iter = GetNextChild(id, cookie)) {
    wxTreeItemId found = getId(iter, node);
    if(found.IsOk()) {
      return found;
    }
  }
  return wxTreeItemId();
}
/***********************************************************************//**
	$Id$
***************************************************************************/
}
}
