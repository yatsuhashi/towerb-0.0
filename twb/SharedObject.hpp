﻿/***********************************************************************//**
	@file
***************************************************************************/
#pragma once

namespace twb {
/***********************************************************************//**
        @brief 参照カウンタを使ったオブジェクト
***************************************************************************/
class SharedObject {
 public:
  typedef unsigned int ReferenceCount;

 private:
  mutable ReferenceCount referenceCount_;

 public:
  SharedObject();
  virtual ~SharedObject();

  void addRef() const;
  void release() const;

  /**
   * 参照カウンタを取得する
   * @return 参照カウンタの値
   */
  ReferenceCount getReferenceCount() const {
    return referenceCount_;
  }

  /**
   * 参照されなくなったときの処理
   */
  virtual void destroy() const {
    delete this;
  }
};
/***********************************************************************//**
        $Id$
***************************************************************************/
}
