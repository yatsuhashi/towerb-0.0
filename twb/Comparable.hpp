﻿/***********************************************************************//**
	@file
***************************************************************************/
#pragma once

namespace twb {
/***********************************************************************//**
	@brief 比較関数インターフェース
***************************************************************************/
template <class T, class V>
class Comparable {
 public:
  virtual V compare(const T& rhs) const = 0;

  bool operator<(const T& rhs) const {
    return compare(rhs) < 0;
  }

  bool operator<=(const T& rhs) const {
    return compare(rhs) <= 0;
  }

  bool operator>(const T& rhs) const {
    return compare(rhs) > 0;
  }

  bool operator>=(const T& rhs) const {
    return compare(rhs) >= 0;
  }

  bool operator==(const T& rhs) const {
    return compare(rhs) == 0;
  }

  bool operator!=(const T& rhs) const {
    return compare(rhs) != 0;
  }
};
/***********************************************************************//**
	$Id$
***************************************************************************/
}
