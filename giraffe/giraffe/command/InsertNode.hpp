﻿/***********************************************************************//**
	@file
***************************************************************************/
#pragma once

namespace giraffe {
namespace command {
/***********************************************************************//**
	@brief ノードを挿入するコマンド
***************************************************************************/
class InsertNode : public wxCommand {
  typedef wxCommand super;

 private:
  twb::Shared<Node> parent_;
  twb::Shared<Node> child_;

 public:
  InsertNode(Node* parent, Node* child, 
             const wxString& name = wxEmptyString);
  virtual ~InsertNode();

 protected:
  virtual bool Do() override;
  virtual bool Undo() override;
};
/***********************************************************************//**
	$Id$
***************************************************************************/
}
}
