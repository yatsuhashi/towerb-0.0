﻿/***********************************************************************//**
	@file
***************************************************************************/
#include "stdafx.hpp"
#include "giraffe/Node.hpp"
#include "giraffe/command/DeleteNode.hpp"

namespace giraffe {
namespace command {
/***********************************************************************//**
	@brief コンストラクタ
***************************************************************************/
DeleteNode::DeleteNode(Node* node)
  : super(true), 
    parent_(node->getParent()), 
    child_(node)
{}
/***********************************************************************//**
	@brief デストラクタ
***************************************************************************/
DeleteNode::~DeleteNode() {
}
/***********************************************************************//**
	@brief 
***************************************************************************/
bool DeleteNode::Do() {
  parent_->removeChild(child_);
  return true;
}
/***********************************************************************//**
	@brief 
***************************************************************************/
bool DeleteNode::Undo() {
  parent_->appendChild(child_);
  return true;
}
/***********************************************************************//**
	$Id$
***************************************************************************/
}
}
