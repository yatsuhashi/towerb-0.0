﻿/***********************************************************************//**
	@file
***************************************************************************/
#include "stdafx.hpp"
#include "giraffe/Importer.hpp"
#include "giraffe/fbx/Importer.hpp"
#include "giraffe/gui/Application.hpp"
#include "giraffe/gui/MainFrame.hpp"

namespace giraffe {
/***********************************************************************//**
	@brief デストラクタ
***************************************************************************/
Importer::~Importer() {
}
/***********************************************************************//**
	@brief 
***************************************************************************/
model::Model* Importer::ImportModel() {
  wxFileDialog dialog(gui::Application::Get()->getMainFrame(), 
                      "インポートするモデルのファイルを選んで下さい", 
                      "", "", 
                      "FBXファイル(*.fbx)|*.fbx", 
                      wxFD_OPEN | wxFD_FILE_MUST_EXIST);
  if(dialog.ShowModal() == wxID_OK) {
    wxString path = dialog.GetPath();
    return ImportModel(path);
  }
  return 0;
}
/***********************************************************************//**
	@brief 
***************************************************************************/
model::Model* Importer::ImportModel(const char* path) {
  twb::Shared<Importer> importer(Create(path));
  if(importer) {
    return importer->importModel();
  }
  return 0;
}
/***********************************************************************//**
	@brief 
***************************************************************************/
model::Animation* Importer::ImportAnimation() {
  wxFileDialog dialog(gui::Application::Get()->getMainFrame(), 
                      "インポートするアニメーションのファイルを選んで下さい", 
                      "", "", 
                      "FBXファイル(*.fbx)|*.fbx", 
                      wxFD_OPEN | wxFD_FILE_MUST_EXIST);
  if(dialog.ShowModal() == wxID_OK) {
    wxString path = dialog.GetPath();
    return ImportAnimation(path);
  }
  return 0;
}
/***********************************************************************//**
	@brief 
***************************************************************************/
model::Animation* Importer::ImportAnimation(const char* path) {
  twb::Shared<Importer> importer(Create(path));
  if(importer) {
    return importer->importAnimation();
  }
  return 0;
}
/***********************************************************************//**
	@brief コンストラクタ
***************************************************************************/
Importer::Importer(const wxFileName& path)
  : path_(path), 
    scale_(0.1)
{}
/***********************************************************************//**
	@brief 
***************************************************************************/
wxFileName Importer::getPath(wxString path) {
  path.Replace("/", "\\");
  return wxFileName(path, wxPATH_WIN);
}
/***********************************************************************//**
	@brief 
***************************************************************************/
wxFileName Importer::expandPath(wxString _path) {
  wxFileName path = getPath(_path);
  path.Normalize(wxPATH_NORM_ALL, getPath().GetPath());
  return path;
}
/***********************************************************************//**
	@brief 読み込める画像のパスを拾得する
	@param[in] filename
	@return 画像のパス
***************************************************************************/
wxFileName Importer::getImagePath(const wxString& filename) {
  wxFileName path;
  if(getImagePath(path, filename)) {
    return path;
  }
  static const char* SEARCH_PATH[] = {
    "/", 
    "/tex000a/"
  };
  wxString dir = getPath().GetPath();
  wxString basename = getPath(filename).GetFullName();
  for(auto iter : SEARCH_PATH) {
    if(getImagePath(path,  dir + iter + basename)) {
      return path;
    }
  }
  return expandPath(filename);
}
/***********************************************************************//**
	@brief 
***************************************************************************/
bool Importer::getImagePath(wxFileName& des, const wxString& src) {
  wxLogNull log;
  des = expandPath(src);
  if(wxImage::CanRead(des.GetFullPath())) {
    return true;
  }
  des.SetExt("png");
  if(wxImage::CanRead(des.GetFullPath())) {
    return true;
  }
  return false;
}
/***********************************************************************//**
	@brief 
***************************************************************************/
Importer* Importer::Create(const char* _path) {
  wxFileName path(_path);
  wxString ext = path.GetExt();
  if(ext == "fbx") {
    return NEW fbx::Importer(path);
  }
  return 0;
}
/***********************************************************************//**
	$Id$
***************************************************************************/
}
