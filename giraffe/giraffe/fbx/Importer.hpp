﻿/***********************************************************************//**
	@file
***************************************************************************/
#pragma once

#include <fbxsdk.h>
#include "giraffe/Importer.hpp"
#include "giraffe/model/Geometry.hpp"

namespace giraffe {
namespace fbx {
/***********************************************************************//**
	@brief FBXインポーター
***************************************************************************/
class Importer : public giraffe::Importer {
  typedef giraffe::Importer super;

 private:
  FbxManager* manager_;
  FbxImporter* importer_;
  FbxScene* scene_;
  FbxTime::EMode timeMode_;
  FbxTime period_;
  int fps_;
  FbxStringList uvSetNames_;
  std::vector<model::Geometry::Vertex> vertexes_;

 public:
  Importer(const wxFileName& path);
  virtual ~Importer();

  virtual model::Model* importModel() override;
  virtual model::Animation* importAnimation() override;

 private:
  void parseMaterial(model::Model* model);
  void parseMesh(model::Model* model, FbxNode* node);
  void parseSkin(model::Model* model, FbxMesh* mesh);
  void parseVertex(model::Geometry::Vertex& vertex, FbxMesh* mesh, 
                   int polygonIndex, int vertexIndex);
  model::Skeleton* parseSkeleton(FbxNode* node);
  void parseBone(model::Skeleton* skeleton, FbxNode* node, 
                 model::Bone* parent = 0);
  model::Pose* parsePose(model::Skeleton* skeleton, 
                         FbxNode* root, 
                         const FbxTime& time = FBXSDK_TIME_INFINITE);
  model::Animation* parseAnimation(const FbxTakeInfo* take, 
                                   FbxNode* root);
};
/***********************************************************************//**
	$Id$
***************************************************************************/
}
}
