﻿/***********************************************************************//**
	@file
***************************************************************************/
#include "stdafx.hpp"
#include "twb/render/Env.hpp"

namespace twb {
namespace render {
/***********************************************************************//**
	@brief コンストラクタ
***************************************************************************/
Env::Env()
  : alphaMode_(ALPHA_NULL), 
    cullFace_(CULL_NULL)
{}
/***********************************************************************//**
	@brief 
***************************************************************************/
Env::Env(const Env& src, const Env& des)
  : alphaMode_((des.getAlphaMode() == ALPHA_NULL)
               ? src.getAlphaMode()
               : des.getAlphaMode()), 
    cullFace_((des.getCullFace() == CULL_NULL)
              ? src.getCullFace()
              : des.getCullFace()), 
    flag_(src.flag_)
{}
/***********************************************************************//**
	@brief 
***************************************************************************/
void Env::apply() {
  applyAlphaMode();
  applyCullFace();
  applyFlag(FLAG_DEPTH_TEST, GL_DEPTH_TEST);
  applyFlag(FLAG_POINT_SMOOTH, GL_POINT_SMOOTH);
  applyFlag(FLAG_LINE_SMOOTH, GL_LINE_SMOOTH);
  applyFlag(FLAG_POLYGON_SMOOTH, GL_POLYGON_SMOOTH);
  glDepthMask(!isDepthWrite());
}
/***********************************************************************//**
	@brief 
***************************************************************************/
void Env::applyAlphaMode() {
  switch(alphaMode_) {
  case ALPHA_BLEND:
    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    break;
  case ALPHA_ADD:
    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE);
    break;
  case ALPHA_MUL:
    glEnable(GL_BLEND);
    glBlendFunc(GL_ZERO, GL_SRC_COLOR);
    break;
  case ALPHA_REVERSE:
    glEnable(GL_BLEND);
    glBlendFunc(GL_ONE_MINUS_DST_COLOR, GL_ZERO);
    break;
  case ALPHA_SCREEN:
    glEnable(GL_BLEND);
    glBlendFunc(GL_ONE_MINUS_DST_COLOR, GL_ONE);
    break;
  case ALPHA_NONE:
    glDisable(GL_BLEND);
    break;
  }
}
/***********************************************************************//**
	@brief 
***************************************************************************/
void Env::applyCullFace() {
  switch(cullFace_) {
  case CULL_NONE:
    glDisable(GL_CULL_FACE);
    break;
  case CULL_BACK:
    glEnable(GL_CULL_FACE);
    glCullFace(GL_BACK);
    break;
  case CULL_FRONT:
    glEnable(GL_CULL_FACE);
    glCullFace(GL_FRONT);
    break;
  }
}
/***********************************************************************//**
	@brief 
***************************************************************************/
Env& Env::setFlag(Flag flag, bool value) {
  flag_.set(flag, value);
  change_.set(flag, true);
  return *this;
}
/***********************************************************************//**
	@brief 
***************************************************************************/
void Env::applyFlag(Flag flag, GLenum cap) {
  if(flag_.test(flag)) {
    glEnable(cap);
  }
  else {
    glDisable(cap);
  }
}
/***********************************************************************//**
	$Id$
***************************************************************************/
}
}
