﻿/***********************************************************************//**
	@file
***************************************************************************/
#include "stdafx.hpp"
#include "giraffe/Context.hpp"
#include "giraffe/Image.hpp"
#include "giraffe/property/Property.hpp"

namespace giraffe {
/***********************************************************************//**
	@brief コンストラクタ
***************************************************************************/
Image::Image(const wxFileName& path)
  : super("Image")
{
  property::Property* property = getProperty();
  property->appendMember("path", "パス", wxFileName());
  setPath(path);
}
/***********************************************************************//**
	@brief デストラクタ
***************************************************************************/
Image::~Image() {
}
/***********************************************************************//**
	@brief 
***************************************************************************/
twb::render::Image* Image::getImage(Context* context) {
  if(!image_) {
    createImage(context);
  }
  return image_;
}
/***********************************************************************//**
	@brief 
***************************************************************************/
void Image::createImage(Context* context) {
  if(!wxImage_.IsOk()) {
    if(!wxImage_.LoadFile(getPath().GetFullPath())) {
      image_.reset(context->getDummyImage());
      setError(true);
      return;
    }
  }
  const wxSize size = wxImage_.GetSize();
  size_t len = size.x * size.y;
  twb::render::Image::Pixel* pixels = new twb::render::Image::Pixel[len];
  {
    unsigned char* src = wxImage_.GetData();
    twb::render::Image::Pixel* des = pixels;
    for(size_t i = 0; i < len; i++, src += 3, des++) {
      des->r = src[0];
      des->g = src[1];
      des->b = src[2];
    }
  }
  if(unsigned char* src = wxImage_.GetAlpha()) {
    twb::render::Image::Pixel* des = pixels;
    for(size_t i = 0; i < len; i++) {
      (des++)->a = *src++;
    }
  }
  else if(wxImage_.HasMask()) {
    twb::render::Image::Pixel mask;
    mask.r = wxImage_.GetMaskRed();
    mask.g = wxImage_.GetMaskGreen();
    mask.b = wxImage_.GetMaskBlue();
    twb::render::Image::Pixel* des = pixels;
    for(size_t i = 0; i < len; i++, des++) {
      if(des->r == mask.r && 
         des->g == mask.g && 
         des->b == mask.b) {
        des->r = 127;
        des->g = 127;
        des->b = 127;
        des->a = 0;
      }
    }
  }
  twb::render::Image* image = NEW twb::render::Image();
  image->load(glm::ivec2(size.x, size.y), pixels);
  image_.reset(image);
}
/***********************************************************************//**
	$Id$
***************************************************************************/
}
