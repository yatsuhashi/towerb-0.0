﻿/***********************************************************************//**
	@file
***************************************************************************/
#include "stdafx.hpp"
#include "giraffe/Node.hpp"
#include "giraffe/property/Property.hpp"
#include "giraffe/xml/Exporter.hpp"

namespace giraffe {
namespace xml {
/***********************************************************************//**
	@brief コンストラクタ
***************************************************************************/
Exporter::Exporter() {
}
/***********************************************************************//**
	@brief デストラクタ
***************************************************************************/
Exporter::~Exporter() {
}
/***********************************************************************//**
	@brief 
***************************************************************************/
bool Exporter::exportNode() {
  wxXmlDocument doc;
  doc.SetRoot(exportNode(getNode()));
  if(!open()) {
    return false;
  }
  return doc.Save(*getOut());
}
/***********************************************************************//**
	@brief 
***************************************************************************/
wxXmlNode* Exporter::exportNode(const Node* node) {
  wxXmlNode* xml = NEW wxXmlNode(wxXML_ELEMENT_NODE, node->getType());
  node->getProperty()->toXml(xml, this);
  for(const Node* iter = node->getChild(); iter; iter = iter->getNext()) {
    xml->AddChild(exportNode(iter));
  }
  return xml;
}
/***********************************************************************//**
	$Id$
***************************************************************************/
}
}
