﻿/***********************************************************************//**
	@file
***************************************************************************/
#pragma once

#include "twb/FrameTime.hpp"
#include "twb/Shared.hpp"
#include "twb/canvas/Widget.hpp"

namespace twb {
namespace canvas {
/***********************************************************************//**
	@brief 
***************************************************************************/
class Context : public Widget {
  typedef Widget super;

 private:
  std::auto_ptr<render::Renderer> renderer_;
  FrameTime time_;
  FrameTime deltaTime_;

 public:
  Context();
  virtual ~Context();

  render::Renderer* getRenderer() const {
    return renderer_.get();
  }

  const FrameTime& getDeltaTime() const {
    return deltaTime_;
  }

  void update();
  void render(const IRect& rect);

 protected:
  virtual void onSetupRenderer() {}
};
/***********************************************************************//**
	$Id$
***************************************************************************/
}
}
