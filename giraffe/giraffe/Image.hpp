﻿/***********************************************************************//**
	@file
***************************************************************************/
#pragma once

#include "giraffe/Node.hpp"

namespace giraffe {
/***********************************************************************//**
	@brief 画像ノード
***************************************************************************/
class Image : public Node {
  typedef Node super;

 private:
  wxImage wxImage_;
  twb::Shared<twb::render::Image> image_;

 public:
  Image(const wxFileName& path);
  virtual ~Image();

  twb::render::Image* getImage(Context* context);

 private:
  void createImage(Context* context);
};
/***********************************************************************//**
	$Id$
***************************************************************************/
}
