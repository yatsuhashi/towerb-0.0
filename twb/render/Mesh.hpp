﻿/***********************************************************************//**
	@file
***************************************************************************/
#pragma once

#include "twb/SharedArray.hpp"
#include "twb/render/Renderable.hpp"

namespace twb {
namespace render {
/***********************************************************************//**
	@brief メッシュ
***************************************************************************/
class Mesh : public Renderable {
  typedef Renderable super;

 private:
  SharedArray<Geometry> geometries_;

 public:
  Mesh();
  virtual ~Mesh();

  void appendGeometry(Geometry* geometry);

  virtual void render(Renderer* renderer) override;
  void render(const Model* model, Renderer* renderer);
};
/***********************************************************************//**
	$Id$
***************************************************************************/
}
}
