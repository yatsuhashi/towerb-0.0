﻿/***********************************************************************//**
	@file
***************************************************************************/
#include "stdafx.hpp"
#include "giraffe/Node.hpp"
#include "giraffe/gui/TimeLine.hpp"
#include "giraffe/gui/TimeLineTree.hpp"
#include "giraffe/gui/TimeLineView.hpp"
#include "giraffe/model/Animation.hpp"

namespace giraffe {
namespace gui {
/***********************************************************************//**
	@brief コンストラクタ
***************************************************************************/
TimeLine::TimeLine(wxWindow* parent)
  : super(parent), 
    toolBar_(NEW wxToolBar(this, wxID_ANY)), 
    tree_(0), 
    view_(0)
{
  wxBoxSizer* sizer = NEW wxBoxSizer(wxVERTICAL);
  setupToolBar();
  sizer->Add(toolBar_, wxSizerFlags().Expand());
  wxSplitterWindow* splitter = NEW wxSplitterWindow(this);
  tree_ = NEW TimeLineTree(splitter);
  view_ = NEW TimeLineView(splitter);
  splitter->SplitVertically(tree_, view_);
  sizer->Add(splitter, wxSizerFlags().Expand().Proportion(1));
  SetSizerAndFit(sizer);
}
/***********************************************************************//**
	@brief デストラクタ
***************************************************************************/
TimeLine::~TimeLine() {
}
/***********************************************************************//**
	@brief 
***************************************************************************/
void TimeLine::onSelectNode(Node* node) {
  if(model::Animation* animation = node->getNode<model::Animation>()) {
    tree_->update(animation);
  }
}
/***********************************************************************//**
	@brief 
***************************************************************************/
void TimeLine::setupToolBar() {
  wxStaticText* text = NEW wxStaticText(toolBar_, wxID_ANY, "ツールバー");
  toolBar_->AddControl(text);
}
/***********************************************************************//**
	$Id$
***************************************************************************/
}
}
