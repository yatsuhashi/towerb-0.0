﻿/***********************************************************************//**
	@file
***************************************************************************/
#include "stdafx.hpp"
#include "giraffe/Exporter.hpp"
#include "giraffe/property/Property.hpp"

namespace giraffe {
namespace property {
/***********************************************************************//**
	@brief コンストラクタ
***************************************************************************/
Property::Property() {
}
/***********************************************************************//**
	@brief デストラクタ
***************************************************************************/
Property::~Property() {
}
/***********************************************************************//**
	@brief メンバーを追加する
	@param[in] member 追加するメンバー
	@return 追加したメンバー
***************************************************************************/
Member* Property::appendMember(Member* member) {
  if(findMember(member->getName())) {
    wxLogWarning("duplicate property member, '%s'", member->getName());
    return 0;
  }
  members_.push_back(member);
  return member;
}
/***********************************************************************//**
	@brief 
***************************************************************************/
size_t Property::countMember() const {
  return members_.size();
}
/***********************************************************************//**
	@brief 
***************************************************************************/
Member* Property::getMember(size_t index) const {
  return members_.at(index);
}
/***********************************************************************//**
	@brief 
***************************************************************************/
Member* Property::findMember(const wxString& name) const {
  for(auto member : members_) {
    if(member->getName() == name) {
      return member;
    }
  }
  return 0;
}
/***********************************************************************//**
	@brief 
***************************************************************************/
const wxAny& Property::getValue(const wxString& name) const {
  const Member* member = findMember(name);
  TWB_ASSERT(member);
  return member->getValue();
}
/***********************************************************************//**
	@brief 
***************************************************************************/
Property& Property::setValue(const wxString& name, const wxAny& value) {
  Member* member = findMember(name);
  TWB_ASSERT(member);
  member->setValue(value);
  return *this;
}
/***********************************************************************//**
	@brief 
***************************************************************************/
void Property::toXml(wxXmlNode* xml, const Exporter* exporter, 
                     const wxString& prefix) const {
  for(auto member : members_) {
    wxString name = prefix + member->getName();
    if(!member->isReadOnly()) {
      const wxAny& value = member->getValue();
      if(!value.IsNull()) {
        if(value.CheckType<wxFileName>()) {
          wxFileName path = 
            exporter->getRelativePath(value.As<wxFileName>());
          xml->AddAttribute(name, path.GetFullPath());
        }
        else {
          xml->AddAttribute(name, member->getValueString());
        }
      }
    }
    if(const Property* property = member->getProperty()) {
      property->toXml(xml, exporter, name + ".");
    }
  }
}
/***********************************************************************//**
	@brief 
***************************************************************************/
void Property::fromXml(const wxXmlNode* xml, const wxString& cwd) {
}
/***********************************************************************//**
	$Id$
***************************************************************************/
}
}
