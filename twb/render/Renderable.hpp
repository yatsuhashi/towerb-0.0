﻿/***********************************************************************//**
	@file
***************************************************************************/
#pragma once

#include "twb/SharedObject.hpp"

namespace twb {
namespace render {
/***********************************************************************//**
	@brief 描画できるもの
***************************************************************************/
class Renderable : public SharedObject {
 private:
  enum {
    FLAG_SHOW, 
    FLAG_MAX
  };

 private:
  float priority_;
  std::bitset<FLAG_MAX> flag_;

 public:
  Renderable(float priority = 0.0f) {
    setShow(true);
  }
  virtual ~Renderable() {}

  float getPriority() const {
    return priority_;
  }

  void setShow(bool isShow) {
    flag_.set(FLAG_SHOW, isShow);
  }

  bool isShow() const {
    return flag_.test(FLAG_SHOW);
  }

  virtual void render(Renderer* renderer) = 0;
};
/***********************************************************************//**
	$Id$
***************************************************************************/
}
}
