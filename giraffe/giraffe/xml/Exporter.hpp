﻿/***********************************************************************//**
	@file
***************************************************************************/
#pragma once

#include "giraffe/Exporter.hpp"

namespace giraffe {
namespace xml {
/***********************************************************************//**
	@brief XMLエクスポーター
***************************************************************************/
class Exporter : public giraffe::Exporter {
  typedef giraffe::Exporter super;

 public:
  Exporter();
  virtual ~Exporter();

 protected:
  virtual bool exportNode();

 private:
  wxXmlNode* exportNode(const Node* node);
};
/***********************************************************************//**
	$Id$
***************************************************************************/
}
}
