﻿/***********************************************************************//**
	@file
***************************************************************************/
#pragma once

#include "giraffe/Node.hpp"

namespace giraffe {
namespace model {
/***********************************************************************//**
	@brief スケルトンノード
***************************************************************************/
class Skeleton : public Node {
  typedef Node super;

 private:
  twb::SharedArray<Bone> bones_;

 public:
  Skeleton();
  virtual ~Skeleton();

  size_t countBone() const {
    return bones_.size();
  }

  Bone* getBone(size_t index) const {
    return bones_.at(index);
  }

  Bone* findBone(const wxString& name) const;

 protected:
  virtual void onAppendChild(Node* child) override;
};
/***********************************************************************//**
	$Id$
***************************************************************************/
}
}
