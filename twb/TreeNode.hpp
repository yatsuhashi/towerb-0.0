﻿/***********************************************************************//**
	@file
***************************************************************************/
#pragma once

#include "twb/Shared.hpp"
#include "twb/SharedObject.hpp"

namespace twb {
/***********************************************************************//**
        @brief 木構造
***************************************************************************/
class TreeNode : public SharedObject {
 private:
  TreeNode* parent_;
  Shared<TreeNode> child_;
  Shared<TreeNode> next_;
  size_t childNum_;

 public:
  TreeNode();
  virtual ~TreeNode();

  TreeNode* getParent() const {
    return parent_;
  }

  TreeNode* getChild() const {
    return child_;
  }

  TreeNode* getNext() const {
    return next_;
  }

  void appendChild(TreeNode* child);
  void prependChild(TreeNode* child);
  void insertChild(TreeNode* pos, TreeNode* child);

  bool removeChild(TreeNode* child);
  void removeAllChild();
  void remove();

  void shiftPrev();
  void shiftNext();

  size_t countChild() const {
    return childNum_;
  }

  TreeNode* getChild(size_t index) const;
  bool hasNode(const TreeNode* node) const;

  virtual int compare(const TreeNode& rhs) const {
    return 0;
  }

  void sortChild();

 protected:
  virtual void onUpdateTree() {}

 private:
  struct CompareFunc {
    bool operator()(const TreeNode* lhs, const TreeNode* rhs) const {
      return lhs->compare(*rhs) < 0;
    }
  };

  void setParent(TreeNode* node);
  void setNext(TreeNode* node);
  void unlink();
  void unlink(TreeNode* child);
};
/***********************************************************************//**
        @brief 木構造継承用テンプレート
***************************************************************************/
template <class T>
class TreeNodeBase : public TreeNode {
  typedef TreeNode super;

 public:
  T* getParent() const {
    return static_cast<T*>(super::getParent());
  }

  T* getChild() const {
    return static_cast<T*>(super::getChild());
  }

  T* getNext() const {
    return static_cast<T*>(super::getNext());
  }

  T* getChild(size_t index) const {
    return static_cast<T*>(super::getChild(index));
  }
};
/***********************************************************************//**
	$Id$
***************************************************************************/
}
