﻿/***********************************************************************//**
	@file
***************************************************************************/
#pragma once

namespace twb {
/***********************************************************************//**
	@brief 
***************************************************************************/
template <class T>
class SharedArray : protected std::vector<T*> {
  typedef std::vector<T*> super;

 public:
  typedef typename super::iterator iterator;
  typedef typename super::const_iterator const_iterator;
  typedef typename super::reverse_iterator reverse_iterator;
  typedef typename super::const_reverse_iterator const_reverse_iterator;

 public:
  /*
   *	デフォルトコンストラクタ
   */
  SharedArray() {}

  /*
   *	コピーコンストラクタ
   */
  SharedArray(const SharedArray& src) {
    for(auto iter : src) {
      push_back(iter);
    }
  }

  /*
   *	デストラクタ
   */
  ~SharedArray() {
    clear();
  }

  /*
   *	要素をクリアする
   */
  void clear() {
    for(auto iter : *this) {
      if(iter) {
        iter->release();
      }
    }
    super::clear();
  }

  SharedArray& operator=(const SharedArray& src) {
    copy(src);
    return *this;
  }

  /*
   *	最後尾に要素を追加する
   */
  void push_back(T* value) {
    if(value) {
      value->addRef();
    }
    super::push_back(value);
  }

  /*
   *	最後尾の要素を削除する
   */
  void pop_back() {
    if(T* value = super::back()) {
      value->release();
    }
    super::pop_back();
  }

  /*
   *	指定位置に要素を挿入する
   */
  iterator insert(iterator pos, T* value) {
    if(value) {
      value->addRef();
    }
    return super::insert(pos, value);
  }

  iterator begin() {
    return super::begin();
  }

  iterator end() {
    return super::end();
  }

  const_iterator begin() const {
    return super::begin();
  }

  const_iterator end() const {
    return super::end();
  }

  reverse_iterator rbegin() {
    return super::rbegin();
  }

  reverse_iterator rend() {
    return super::rend();
  }

  const_reverse_iterator rbegin() const {
    return super::rbegin();
  }

  const_reverse_iterator rend() const {
    return super::rend();
  }

  size_t size() const {
    return super::size();
  }

  bool empty() const {
    return super::empty();
  }

  T* at(size_t index) const {
    return super::at(index);
  }

  T* operator[](size_t index) const {
    return at(index);
  }

  T* back() const {
    return super::back();
  }

  void resize(size_t size) {
    super::resize(size, static_cast<T*>(0));
  }

  void reset(size_t index, T* value) {
    if(value) {
      value->addRef();
    }
    if(super::at(index)) {
      super::at(index)->release();
    }
    super::at(index) = value;
  }

  iterator erase(iterator& iter) {
    (*iter)->release();
    return super::erase(iter);
  }

  void copy(const SharedArray& other) {
    clear();
    concat(other);
  }

  void concat(const SharedArray& other) {
    for(auto iter : other) {
      push_back(iter);
    }
  }

  bool isExist(const T* value) const {
    auto iter = std::find(begin(), end(), value);
    return iter != end();
  }

  iterator find(const T* value) {
    return std::find(begin(), end(), value);
  }

  bool remove(const T* value) {
    iterator iter = find(value);
    if(iter == end()) {
      return false;
    }
    erase(iter);
    return true;
  }
};
/***********************************************************************//**
	$Id$
***************************************************************************/
}
