﻿/***********************************************************************//**
	@file
***************************************************************************/
#include "stdafx.hpp"
#include "twb/thread/ThreadBase.hpp"

namespace twb {
namespace thread {
/***********************************************************************//**
	@brief デストラクタ
***************************************************************************/
ThreadBase::~ThreadBase() {
}
/***********************************************************************//**
	@brief 
***************************************************************************/
void ThreadBase::start() {
  stop();
  flag_.reset();
  create();
}
/***********************************************************************//**
	@brief 
***************************************************************************/
void ThreadBase::stop() {
  flag_.set(FLAG_STOP);
  join();
}
/***********************************************************************//**
	$Id$
***************************************************************************/
}
}
