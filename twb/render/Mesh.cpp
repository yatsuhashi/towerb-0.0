﻿/***********************************************************************//**
	@file
***************************************************************************/
#include "stdafx.hpp"
#include "twb/render/Geometry.hpp"
#include "twb/render/Mesh.hpp"

namespace twb {
namespace render {
/***********************************************************************//**
	@brief コンストラクタ
***************************************************************************/
Mesh::Mesh() {
}
/***********************************************************************//**
	@brief デストラクタ
***************************************************************************/
Mesh::~Mesh() {
}
/***********************************************************************//**
	@brief 
***************************************************************************/
void Mesh::appendGeometry(Geometry* geometry) {
  geometries_.push_back(geometry);
}
/***********************************************************************//**
	@brief 描画
***************************************************************************/
void Mesh::render(Renderer* renderer) {
  for(auto geometry : geometries_) {
    geometry->render(renderer);
  }
}
/***********************************************************************//**
	@brief 
***************************************************************************/
void Mesh::render(const Model* model, Renderer* renderer) {
  if(!isShow()) {
    return;
  }
  for(auto geometry : geometries_) {
    geometry->render(model, renderer);
  }
}
/***********************************************************************//**
	$Id$
***************************************************************************/
}
}
