﻿/***********************************************************************//**
	@file
***************************************************************************/
#pragma once

#include "giraffe/Node.hpp"

namespace giraffe {
namespace model {
/***********************************************************************//**
	@brief マテリアルノード
***************************************************************************/
class Material : public Node {
  typedef Node super;

 public:
  enum Type {
    TYPE_DIFFUSE, 
    TYPE_MAX
  };

 private:
  twb::Shared<Texture> textures_[TYPE_MAX];
  twb::Shared<twb::render::Material> material_;

 public:
  Material();
  virtual ~Material();

  void setTexture(Type type, const wxFileName& path);
  Texture* getTexture(Type type) const;

  twb::render::Material* getMaterial(Context* context);

 private:
  void createMaterial(Context* context);
};
/***********************************************************************//**
	$Id$
***************************************************************************/
}
}
