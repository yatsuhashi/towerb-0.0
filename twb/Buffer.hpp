﻿/***********************************************************************//**
	@file
***************************************************************************/
#pragma once

#include "twb/SharedObject.hpp"

namespace twb {
/***********************************************************************//**
	@brief 可変長バッファ
***************************************************************************/
class Buffer : public SharedObject {
 private:
  size_t size_;
  void* buff_;

 public:
  Buffer();
  Buffer(size_t size);
  Buffer(const Buffer& src);
  virtual ~Buffer();

  void allocate(size_t size);
  void clear();

  size_t getSize() const {
    return size_;
  }

  Buffer& operator=(const Buffer& src);

  template <class T>
  size_t getLength() const {
    return size_ / sizeof(T);
  }

  template <class T>
  T* get(size_t offset = 0) {
    return reinterpret_cast<T*>(uintptr_t(buff_) + offset);
  }

  template <class T>
  const T* get(size_t offset = 0) const {
    return const_cast<Buffer*>(this)->get<T>(offset);
  }
};
/***********************************************************************//**
	$Id$
***************************************************************************/
}
