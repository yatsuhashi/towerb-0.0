﻿/***********************************************************************//**
	@file
***************************************************************************/
#pragma once

#include "twb/Shared.hpp"
#include "twb/SharedObject.hpp"

namespace twb {
namespace render {
/***********************************************************************//**
	@brief マテリアル基底クラス
***************************************************************************/
class Material : public twb::SharedObject {
 private:
  enum {
    FLAG_DEPTH_TEST, 
    FLAG_MAX
  };

 private:
  Shared<Shader> shader_;
  Shared<Texture> texture_;
  std::bitset<FLAG_MAX> flag_;

 public:
  Material();
  virtual ~Material();

  void setShader(Shader* shader);
  void setTexture(Texture* texture);

  Material& setDepthTest(bool isTest) {
    flag_.set(FLAG_DEPTH_TEST, isTest);
    return *this;
  }

  virtual void use(Renderer* renderer);
  virtual void unuse(Renderer* renderer);
};
/***********************************************************************//**
	$Id$
***************************************************************************/
}
}
