﻿/***********************************************************************//**
	@file
***************************************************************************/
#include "stdafx.hpp"
#include "twb/render/geometry/Grid.hpp"

namespace twb {
namespace render {
namespace geometry {
/***********************************************************************//**
	@brief コンストラクタ
***************************************************************************/
Grid::Grid(Material* material, int size)
  : super(material)
{
  for(int i = 1; i <= size; i++) {
    appendLine(glm::vec3(i, 0, -size), glm::vec3(i, 0, size), 
               glm::vec4(1));
    appendLine(glm::vec3(-i, 0, -size), glm::vec3(-i, 0, size), 
               glm::vec4(1));
    appendLine(glm::vec3(-size, 0, i), glm::vec3(size, 0, i), 
               glm::vec4(1));
    appendLine(glm::vec3(-size, 0, -i), glm::vec3(size, 0, -i), 
               glm::vec4(1));
  }
  appendLine(glm::vec3(0, 0, -size), glm::vec3(0, 0, size), 
             glm::vec4(0.3, 0.3, 1, 1));
  appendLine(glm::vec3(-size, 0, 0), glm::vec3(size, 0, 0), 
             glm::vec4(1, 0.3, 0.3, 1));
  setup(MODE_LINES);
}
/***********************************************************************//**
	@brief デストラクタ
***************************************************************************/
Grid::~Grid() {
}
/***********************************************************************//**
	$Id$
***************************************************************************/
}
}
}
