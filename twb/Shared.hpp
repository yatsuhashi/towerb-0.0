﻿/***********************************************************************//**
	@file
***************************************************************************/
#pragma once

namespace twb {
/***********************************************************************//**
	@brief SharedObject参照用テンプレート
***************************************************************************/
template <class T>
class Shared {
 private:
  T* instance_;

 public:
  Shared()
    : instance_(0)
  {}

  Shared(T* instance)
    : instance_(0)
  {
    reset(instance);
  }

  Shared(const Shared& orig)
    : instance_(0)
  {
    reset(orig.get());
  }

  ~Shared() {
    reset();
  }

  Shared& operator=(const Shared& orig) {
    reset(orig.get());
    return *this;
  }

  void reset(T* instance = 0) {
    if(instance != instance_) {
      if(instance_) {
        instance_->release();
      }
      if(instance) {
        instance->addRef();
      }
      instance_ = instance;
    }
  }

  T* get() const {
    return instance_;
  }

  T* operator->() const {
    return get();
  }

  operator bool() const {
    return instance_ != 0;
  }

  operator T*() const {
    return get();
  }
};
/***********************************************************************//**
	$Id$
***************************************************************************/
}
