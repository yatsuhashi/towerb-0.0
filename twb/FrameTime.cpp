﻿/***********************************************************************//**
	@file
***************************************************************************/
#include "stdafx.hpp"
#include "twb/FrameTime.hpp"

namespace twb {
/***********************************************************************//**
	@brief 時間を更新する
	@param[in] now 新しい時間
	@return 進んだ時間
***************************************************************************/
FrameTime FrameTime::update(const FrameTime& now) {
  FrameTime delta(isZero() ? 0 : (now - *this));
  *this = now;
  return delta;
}
/***********************************************************************//**
	@brief 現在の時間を取得する
	@return 現在の時間
***************************************************************************/
FrameTime FrameTime::GetNow() {
#if defined(TWB_PLATFORM_APPLE)
  uint64_t time = mach_absolute_time();
  mach_timebase_info_data_t base;
  mach_timebase_info(&base);
  return FrameTime(FrameTime_t(time) * 
                   FrameTime_t(base.numer) / 
                   FrameTime_t(base.denom) /
                   1000000000);
#elif defined(TWB_PLATFORM_WINDOWS)
  LARGE_INTEGER freq;
  LARGE_INTEGER count;
  QueryPerformanceFrequency(&freq);
  QueryPerformanceCounter(&count);
  return FrameTime(FrameTime_t(count.QuadPart) / 
                   FrameTime_t(freq.QuadPart));
#else
  struct timeval t;
  gettimeofday(&t, 0);
  return FrameTime(FrameTime_t(t.tv_sec) + 
                   FrameTime_t(t.tv_usec) / 1000000);
#endif
}
/***********************************************************************//**
	@brief 
***************************************************************************/
const FrameTime& FrameTime::GetZero() {
  static const FrameTime ZERO(0);
  return ZERO;
}
/***********************************************************************//**
	$Id$
***************************************************************************/
}
