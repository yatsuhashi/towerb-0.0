﻿/***********************************************************************//**
	@file
***************************************************************************/
#pragma once

namespace giraffe {
/***********************************************************************//**
	@brief アプリケーション
***************************************************************************/
class Application : public wxApp {
  typedef wxApp super;

 public:
  static const wxString VERSION;

 private:
  twb::Shared<Project> project_;
  std::stack<wxLog*> logTargetStack_;

 public:
  Application();
  virtual ~Application();

  Project* getProject() const {
    return project_;
  }

  void pushLogTarget(wxLog* target);
  void popLogTarget();

  virtual void update() {}

  static Application* Get() {
    return static_cast<Application*>(super::GetInstance());
  }

 protected:
  virtual bool OnInit() override;
  virtual int OnExit() override;
};
/***********************************************************************//**
	$Id$
***************************************************************************/
}
