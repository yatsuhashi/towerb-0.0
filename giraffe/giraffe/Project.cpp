﻿/***********************************************************************//**
	@file
***************************************************************************/
#include "stdafx.hpp"
#include "giraffe/Color.hpp"
#include "giraffe/Context.hpp"
#include "giraffe/Project.hpp"
#include "giraffe/property/Camera.hpp"
#include "giraffe/property/Property.hpp"

namespace giraffe {
/***********************************************************************//**
	@brief コンストラクタ
***************************************************************************/
Project::Project()
  : super("Project"), 
    camera_(NEW property::Camera())
{
  setName("Project");
  getProperty()->appendMember("camera", "カメラ", camera_.get());
  getProperty()->appendMember("background", "背景色", 
                              Color(63, 63, 63, 255));
  setUpdateTree(true);
}
/***********************************************************************//**
	@brief デストラクタ
***************************************************************************/
Project::~Project() {
}
/***********************************************************************//**
	@brief 
***************************************************************************/
wxString Project::createName(wxString name) {
  int index = (suffixes_.count(name) == 0) ? 1 : (suffixes_[name] + 1);
  return name << "_" << index;
}
/***********************************************************************//**
	@brief 
***************************************************************************/
void Project::render(Context* context) {
  twb::render::Renderer* renderer = context->getRenderer();
  Color color = getProperty()->getValue("background").As<Color>();
  renderer->clear(color);
  twb::render::Camera* camera = camera_->getCamera();
  camera->setViewport(context->getLayout().getRect());
  camera->updateMatrix();
  renderer->pushCamera(camera);
  if(!grid_) {
    createGrid(context);
  }
  renderer->beginShader(context->getDefaultShader());
  renderer->setModelMatrix(glm::mat4());
  grid_->render(renderer);
  super::render(context);
  renderer->popCamera();
}
/***********************************************************************//**
	@brief 
***************************************************************************/
void Project::onAppendChild(Node* child) {
  super::onAppendChild(child);
  wxString name = child->getName();
  int suffix = name.Find('_', true);
  if(suffix != wxNOT_FOUND) {
    wxString base(name, suffix);
    int index = atoi(name.Mid(suffix + 1));
    if(suffixes_.count(base) == 0) {
      suffixes_[base] = index;
    }
    else {
      suffixes_[base] = std::max(suffixes_[base], index);
    }
  }
}
/***********************************************************************//**
	@brief 
***************************************************************************/
void Project::onUpdateProperty(const wxString& name) {
  super::onUpdateProperty(name);
  camera_->update();
}
/***********************************************************************//**
	@brief 
***************************************************************************/
void Project::createGrid(Context* context) {
  grid_.reset
    (NEW twb::render::geometry::Grid(context->getDefaultMaterial(), 10));
}
/***********************************************************************//**
	$Id$
***************************************************************************/
}
