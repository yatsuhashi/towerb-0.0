﻿/***********************************************************************//**
	@file
***************************************************************************/
#pragma once

#include "twb/Shared.hpp"
#include "twb/SharedArray.hpp"
#include "twb/render/Renderable.hpp"

namespace twb {
namespace render {
/***********************************************************************//**
	@brief 描画シーン
***************************************************************************/
class Scene : public Renderable {
  typedef Renderable super;

 private:
  Shared<Camera> camera_;
  SharedArray<Renderable> renderables_;

 public:
  Scene();
  virtual ~Scene();

  void setCamera(Camera* camera);

  void appendRenderable(Renderable* renderable);
  void removeRenderable(Renderable* renderable);

  virtual void render(Renderer* renderer) override;
};
/***********************************************************************//**
	$Id$
***************************************************************************/
}
}
