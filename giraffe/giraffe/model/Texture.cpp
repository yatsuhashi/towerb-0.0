﻿/***********************************************************************//**
	@file
***************************************************************************/
#include "stdafx.hpp"
#include "giraffe/model/Texture.hpp"

namespace giraffe {
namespace model {
/***********************************************************************//**
	@brief コンストラクタ
***************************************************************************/
Texture::Texture(const wxFileName& path)
  : super(path), 
    magFilter_(twb::render::Texture::FILTER_LINEAR), 
    minFilter_(twb::render::Texture::FILTER_LINEAR), 
    wrapU_(twb::render::Texture::WRAP_REPEAT), 
    wrapV_(twb::render::Texture::WRAP_REPEAT)
{}
/***********************************************************************//**
	@brief デストラクタ
***************************************************************************/
Texture::~Texture() {
}
/***********************************************************************//**
	@brief 
***************************************************************************/
twb::render::Texture* Texture::getTexture(Context* context) {
  if(!texture_) {
    createTexture(context);
  }
  return texture_;
}
/***********************************************************************//**
	@brief 
***************************************************************************/
void Texture::createTexture(Context* context) {
  twb::render::Texture* texture = 
    NEW twb::render::Texture(getImage(context));
  texture_.reset(texture);
}
/***********************************************************************//**
	$Id$
***************************************************************************/
}
}
