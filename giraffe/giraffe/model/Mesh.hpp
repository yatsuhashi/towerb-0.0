﻿/***********************************************************************//**
	@file
***************************************************************************/
#pragma once

#include "giraffe/Node.hpp"

namespace giraffe {
namespace model {
/***********************************************************************//**
	@brief メッシュノード
***************************************************************************/
class Mesh : public Node {
  typedef Node super;

 public:
  Mesh();
  virtual ~Mesh();

  void appendGeometry(Geometry* geometry);
  size_t countGeometry() const;
  Geometry* getGeometry(size_t index) const;

  twb::render::Mesh* createMesh(Context* context);
};
/***********************************************************************//**
	$Id$
***************************************************************************/
}
}
