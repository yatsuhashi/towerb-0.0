﻿/***********************************************************************//**
	@file
***************************************************************************/
#include "twb/twb.hpp"
/***********************************************************************//**
	@brief 
***************************************************************************/
namespace giraffe {
class Application;
class Color;
class Context;
class Exporter;
class Image;
class Importer;
class Node;
class Project;
class Scene;

namespace command {
class InsertNode;
}

namespace fbx {
class Importer;
}

namespace gui {
class Application;
class Canvas;
class MainFrame;
class Menu;
class Option;
class Outliner;
class PropertyGrid;
class TimeLine;
class TimeLineTree;
class TimeLineView;
}

namespace model {
class Animation;
class Bone;
class Geometry;
class Material;
class Mesh;
class Model;
class Pose;
class Skeleton;
class Texture;
}

namespace property {
class Camera;
class Member;
class Property;
}

namespace threejs {
class Exporter;
}
}
/***********************************************************************//**
	$Id$
***************************************************************************/
