﻿/***********************************************************************//**
	@file
***************************************************************************/
#pragma once

namespace twb {
/***********************************************************************//**
	@brief ストリーム基底クラス
***************************************************************************/
class Stream {
 public:
  virtual ~Stream() {}

  virtual size_t read(void* buff, size_t size) {
    return 0;
  }

  virtual size_t write(const void* buff, size_t size) {
    return 0;
  }

  virtual bool setPos(size_t pos) {
    return false;
  }

  virtual size_t getPos() const {
    return 0;
  }

  template <class T>
  bool readValue(T& value) {
    size_t size = read(&value, sizeof(value));
    return size == sizeof(value);
  }

  template <class T>
  T readValue() {
    T value;
    readValue<T>(value);
    return value;
  }

  template <class T>
  bool writeValue(const T& value) {
    size_t size = write(&value, sizeof(value));
    return size == sizeof(value);
  }

 protected:
  Stream() {}
};
/***********************************************************************//**
	$Id$
***************************************************************************/
}
