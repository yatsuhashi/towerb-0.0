﻿/***********************************************************************//**
	@file
***************************************************************************/
#include "stdafx.hpp"
#include "twb/String.hpp"

namespace twb {
const String String::EMPTY;
/***********************************************************************//**
	@brief 
***************************************************************************/
String String::operator+(const char* rhs) const {
  String result = *this;
  result.append(rhs);
  return result;
}
/***********************************************************************//**
        @brief 
***************************************************************************/
bool String::to(long& value) const {
  if(isEmpty()) {
    return false;
  }
  char* endPt;
  value = strtol(c_str(), &endPt, 0);
  return *endPt == '\0';
}
/***********************************************************************//**
        @brief 
***************************************************************************/
bool String::to(double& value) const {
  if(isEmpty()) {
    return false;
  }
  char* endPt;
  value = strtod(c_str(), &endPt);
  return *endPt == '\0';
}
/***********************************************************************//**
        @brief 
***************************************************************************/
String String::Format(const char* format, ...) {
  va_list args;
  va_start(args, format);
  String result(Format(format, args));
  va_end(args);
  return result;
}
/***********************************************************************//**
        @brief フォーマットから文字列作成(printf)
        @param[in] size バッファサイズ
        @param[in] format フォーマット
        @param[in] args 引数
***************************************************************************/
String String::Format(const char* format, va_list& args) {
  int size = 1024 / 2;
  char* buff = 0;
  int length;
  do {
    size *= 2;
    buff = static_cast<char*>(realloc(buff, size));
    length = vsnprintf(buff, size, format, args);
  } while(length >= size);
  String result(buff);
  free(buff);
  return result;
}
/***********************************************************************//**
	$Id$
***************************************************************************/
}
