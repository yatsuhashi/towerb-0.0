﻿/***********************************************************************//**
	@file
***************************************************************************/
#include "stdafx.hpp"
#include "giraffe/model/Geometry.hpp"
#include "giraffe/model/Mesh.hpp"
#include "giraffe/property/Property.hpp"

namespace giraffe {
namespace model {
/***********************************************************************//**
	@brief コンストラクタ
***************************************************************************/
Mesh::Mesh()
  : super("model.Mesh")
{
  property::Property* property = getProperty();
  property->appendMember("vertexNum", "頂点数", int(0))->
    setReadOnly(true);
  property->appendMember("polygonNum", "ポリゴン数", int(0))->
    setReadOnly(true);
}
/***********************************************************************//**
	@brief デストラクタ
***************************************************************************/
Mesh::~Mesh() {
}
/***********************************************************************//**
	@brief 
***************************************************************************/
void Mesh::appendGeometry(Geometry* geometry) {
  getFolder("Geometry")->appendChild(geometry);
}
/***********************************************************************//**
	@brief 
***************************************************************************/
size_t Mesh::countGeometry() const {
  return countFolderNode("Geometry");
}
/***********************************************************************//**
	@brief 
***************************************************************************/
Geometry* Mesh::getGeometry(size_t index) const {
  return static_cast<Geometry*>(getFolderNode("Geometry", index));
}
/***********************************************************************//**
	@brief 
***************************************************************************/
twb::render::Mesh* Mesh::createMesh(Context* context) {
  int vertexNum = 0;
  int polygonNum = 0;
  twb::render::Mesh* mesh = NEW twb::render::Mesh();
  for(auto iter = getFolder("Geometry")->getChild();
      iter;
      iter = iter->getNext()) {
    Geometry* geometry = static_cast<Geometry*>(iter);
    mesh->appendGeometry(geometry->createGeometry(context));
    vertexNum += 
      geometry->getProperty()->getValue("vertexNum").As<int>();
    polygonNum += 
      geometry->getProperty()->getValue("polygonNum").As<int>();
  }
  getProperty()->setValue("vertexNum", vertexNum);
  getProperty()->setValue("polygonNum", polygonNum);
  return mesh;
}
/***********************************************************************//**
	$Id$
***************************************************************************/
}
}
