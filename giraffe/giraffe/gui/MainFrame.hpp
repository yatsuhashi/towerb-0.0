﻿/***********************************************************************//**
	@file
***************************************************************************/
#pragma once

namespace giraffe {
namespace gui {
/***********************************************************************//**
	@brief メインフレーム
***************************************************************************/
class MainFrame : public wxFrame {
  typedef wxFrame super;

 private:
  wxAuiManager auiManager_;
  Canvas* canvas_;
  Outliner* outliner_;
  PropertyGrid* propertyGrid_;
  TimeLine* timeLine_;
  wxTextCtrl* log_;
  wxLogTextCtrl* logTarget_;
  wxTimer timer_;

 public:
  MainFrame();
  virtual ~MainFrame();

  Canvas* getCanvas() const {
    return canvas_;
  }

  Outliner* getOutliner() const {
    return outliner_;
  }

  PropertyGrid* getPropertyGrid() const {
    return propertyGrid_;
  }

  void onSelectNode(Node* node);
  void update();

 private:
  void setupMenuBar();
  void onOpenMenu(wxMenuEvent& event);
  void onSelectMenu(wxCommandEvent& event);
  void onClose(wxCloseEvent& event);
  void onTimer(wxTimerEvent& event);
};
/***********************************************************************//**
	$Id$
***************************************************************************/
}
}
