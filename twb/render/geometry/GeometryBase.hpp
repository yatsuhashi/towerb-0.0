﻿/***********************************************************************//**
	@file
***************************************************************************/
#pragma once

#include "twb/render/Geometry.hpp"

namespace twb {
namespace render {
namespace geometry {
/***********************************************************************//**
	@brief 
***************************************************************************/
class GeometryBase : public Geometry {
  typedef Geometry super;

 private:
  std::vector<Vertex> vertexes_;
  std::vector<uint16_t> indexes_;

 public:
  GeometryBase(Material* material);
  virtual ~GeometryBase();

 protected:
  void appendLine(const glm::vec3& p1, const glm::vec3& p2, 
                  const glm::vec4& color);
  void setup(Mode mode);
};
/***********************************************************************//**
	$Id$
***************************************************************************/
}
}
}
