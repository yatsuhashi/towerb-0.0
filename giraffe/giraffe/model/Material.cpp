﻿/***********************************************************************//**
	@file
***************************************************************************/
#include "stdafx.hpp"
#include "giraffe/Context.hpp"
#include "giraffe/model/Material.hpp"
#include "giraffe/model/Texture.hpp"

namespace giraffe {
namespace model {
/***********************************************************************//**
	@brief コンストラクタ
***************************************************************************/
Material::Material()
  : super("model.Material")
{
}
/***********************************************************************//**
	@brief デストラクタ
***************************************************************************/
Material::~Material() {
}
/***********************************************************************//**
	@brief 
***************************************************************************/
void Material::setTexture(Type type, const wxFileName& path) {
  static const char* TYPES[TYPE_MAX] = {
    "Diffuse"
  };
  auto& item = textures_[type];
  if(item) {
    wxLogWarning("duplicate texture");
    return;
  }
  Texture* texture = NEW Texture(path);
  texture->setName(TYPES[type]);
  appendChild(texture);
  item.reset(texture);
}
/***********************************************************************//**
	@brief 
***************************************************************************/
Texture* Material::getTexture(Type type) const {
  return textures_[type];
}
/***********************************************************************//**
	@brief 
***************************************************************************/
twb::render::Material* Material::getMaterial(Context* context) {
  if(!material_) {
    createMaterial(context);
  }
  return material_;
}
/***********************************************************************//**
	@brief 
***************************************************************************/
void Material::createMaterial(Context* context) {
  twb::render::Material* material = NEW twb::render::Material();
  material->setShader(context->getModelShader());
  if(textures_[TYPE_DIFFUSE]) {
    material->setTexture(textures_[TYPE_DIFFUSE]->getTexture(context));
  }
  material->setDepthTest(true);
  material_.reset(material);
}
/***********************************************************************//**
	$Id$
***************************************************************************/
}
}
