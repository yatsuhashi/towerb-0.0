﻿/***********************************************************************//**
	@file
***************************************************************************/
#pragma once

#include "giraffe/property/Member.hpp"

namespace giraffe {
namespace property {
/***********************************************************************//**
	@brief プロパティ
***************************************************************************/
class Property : public twb::SharedObject {
 private:
  twb::SharedArray<Member> members_;

 public:
  Property();
  virtual ~Property();

  Member* appendMember(Member* member);

  template <class... Args>
  Member* appendMember(const Args&... args) {
    return appendMember(NEW Member(args...));
  }

  size_t countMember() const;
  Member* getMember(size_t index) const;
  Member* findMember(const wxString& name) const;

  const wxAny& getValue(const wxString& name) const;
  Property& setValue(const wxString& name, const wxAny& value);

  void toXml(wxXmlNode* xml, const Exporter* exporter, 
             const wxString& prefix = wxEmptyString) const;
  void fromXml(const wxXmlNode* xml, const wxString& cwd);
};
/***********************************************************************//**
	$Id$
***************************************************************************/
}
}
