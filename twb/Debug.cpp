﻿/***********************************************************************//**
	@file
***************************************************************************/
#include "stdafx.hpp"
#include "twb/Debug.hpp"
#include "twb/String.hpp"

namespace twb {
std::auto_ptr<Debug> Debug::Instance_;
std::string Debug::LineBuff_;
/***********************************************************************//**
	@brief デストラクタ
***************************************************************************/
Debug::~Debug() {
}
/***********************************************************************//**
	@brief 文字列を出力する
	@param[in] format フォーマット記述子
***************************************************************************/
void Debug::Printf(const char* format, ...) {
  Debug* debug = GetInstance();
  va_list args;
  va_start(args, format);
  String text(String::Format(format, args));
  if(!text.isEmpty()) {
    for(auto c : text) {
      if(c == 0x0a || c == 0x0d) {
        if(!LineBuff_.empty()) {
          debug->print(LineBuff_.c_str());
          LineBuff_.clear();
        }
      }
      else {
        LineBuff_.append(1, c);
      }
    }
  }
  va_end(args);
}
/***********************************************************************//**
        @brief 
***************************************************************************/
void Debug::Suspend() {
  GetInstance()->suspend();
}
/***********************************************************************//**
	@brief コンストラクタ
***************************************************************************/
Debug::Debug() {
  Instance_.reset(this);
}
/***********************************************************************//**
	@brief 1行表示
        @param[in] text 表示する文字列
***************************************************************************/
void Debug::print(const char* text) {
#if defined(TWB_PLATFORM_WINDOWS)
  OutputDebugStringA(string);
  OutputDebugStringA("\n");
#elif defined(TWB_PLATFORM_ANDROID)
  __android_log_print(ANDROID_LOG_INFO, "twb", "%s", string);
#else
  fprintf(stderr, "%s\n", text);
#endif
}
/***********************************************************************//**
        @brief 
***************************************************************************/
void Debug::suspend() {
#if defined(TWB_PLATFORM_WINDOWS)
  DebugBreak();
#else
  __builtin_trap();
#endif
}
/***********************************************************************//**
	@brief 
***************************************************************************/
Debug* Debug::GetInstance() {
  Debug* instance = Instance_.get();
  if(!instance) {
    instance = new Debug();
  }
  return instance;
}
/***********************************************************************//**
	$Id$
***************************************************************************/
}
