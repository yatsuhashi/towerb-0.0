﻿/***********************************************************************//**
	@file
***************************************************************************/
#include "stdafx.hpp"
#include "giraffe/model/Bone.hpp"
#include "giraffe/model/Pose.hpp"
#include "giraffe/model/Skeleton.hpp"

namespace giraffe {
namespace model {
/***********************************************************************//**
	@brief 
***************************************************************************/
Pose::Pose(Skeleton* skeleton)
  : skeleton_(skeleton), 
    matrixes_(skeleton->countBone())
{}
/***********************************************************************//**
	@brief 
***************************************************************************/
Pose::Pose(const Pose& src)
  : skeleton_(src.getSkeleton()), 
    matrixes_(src.matrixes_)
{}
/***********************************************************************//**
	@brief 
***************************************************************************/
Pose* Pose::createGlobalPose() const {
  Skeleton* skeleton = getSkeleton();
  Pose* pose = NEW Pose(skeleton);
  for(size_t i = 0, n = skeleton->countBone(); i < n; i++) {
    pose->getMatrix(i) = getMatrix(i);
    if(const Bone* bone = skeleton->getBone(i)) {
      if(const Bone* parent = bone->getParentBone()) {
        pose->getMatrix(i) = pose->getMatrix(parent->getIndex()) * 
          pose->getMatrix(i);
      }
    }
  }
  return pose;
}
/***********************************************************************//**
	@brief 
***************************************************************************/
void Pose::inverse() {
  for(size_t i = 0, n = matrixes_.size(); i < n; i++) {
    matrixes_[i] = glm::inverse(matrixes_[i]);
  }
}
/***********************************************************************//**
	$Id$
***************************************************************************/
}
}
