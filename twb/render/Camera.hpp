﻿/***********************************************************************//**
	@file
***************************************************************************/
#pragma once

#include "twb/Rect.hpp"
#include "twb/SharedObject.hpp"

namespace twb {
namespace render {
/***********************************************************************//**
	@brief カメラ基底クラス
***************************************************************************/
class Camera : public SharedObject {
 private:
  IRect viewport_;
  glm::mat4 projectionMatrix_;
  glm::mat4 modelViewMatrix_;

 public:
  virtual ~Camera() {}

  Camera& setViewport(const IRect& viewport) {
    viewport_ = viewport;
    return *this;
  }

  const IRect& getViewport() const {
    return viewport_;
  }

  const glm::mat4& getProjectionMatrix() const {
    return projectionMatrix_;
  }

  const glm::mat4& getModelViewMatrix() const {
    return modelViewMatrix_;
  }

  virtual void updateMatrix() {}

  glm::vec3 worldToScreen(const glm::vec3& pos) const;
  glm::vec3 screenToWorld(const glm::vec3& pos) const;

 protected:
  Camera();

  void setProjectionMatrix(const glm::mat4& matrix) {
    projectionMatrix_ = matrix;
  }

  void setModelViewMatrix(const glm::mat4& matrix);
};
/***********************************************************************//**
	$Id$
***************************************************************************/
}
}
