﻿/***********************************************************************//**
	@file
***************************************************************************/
#include "stdafx.hpp"
#include "giraffe/Color.hpp"
#include "giraffe/Node.hpp"
#include "giraffe/gui/Application.hpp"
#include "giraffe/gui/PropertyGrid.hpp"
#include "giraffe/property/Property.hpp"

namespace giraffe {
namespace gui {
/***********************************************************************//**
	@brief コンストラクタ
***************************************************************************/
PropertyGrid::PropertyGrid(wxWindow* parent)
  : super(parent)
{
  Bind(wxEVT_PG_CHANGED, &PropertyGrid::onChange, this);
  Bind(wxEVT_PG_ITEM_EXPANDED, &PropertyGrid::onExpand, this);
  Bind(wxEVT_PG_ITEM_COLLAPSED, &PropertyGrid::onCollapse, this);
}
/***********************************************************************//**
	@brief デストラクタ
***************************************************************************/
PropertyGrid::~PropertyGrid() {
}
/***********************************************************************//**
	@brief 
***************************************************************************/
void PropertyGrid::onSelectNode(Node* node) {
  if(node != node_.get()) {
    node_.reset(node);
    Freeze();
    Clear();
    if(node) {
      appendProperty(node->getProperty());
    }
    Thaw();
  }
  update();
}
/***********************************************************************//**
	@brief 
***************************************************************************/
void PropertyGrid::update() {
  updateProperty(GetRoot());
}
/***********************************************************************//**
	@brief 
***************************************************************************/
void PropertyGrid::appendProperty(const property::Property* property, 
                                  wxPGProperty* parent) {
  for(size_t i = 0, num = property->countMember(); i < num; i++) {
    appendMember(property->getMember(i), parent);
  }
}
/***********************************************************************//**
	@brief 
***************************************************************************/
void PropertyGrid::appendMember(const property::Member* member, 
                                wxPGProperty* parent) {
  const wxString& label = member->getLabel();
  if(label.IsEmpty()) {
    return;
  }
  wxString name = parent
    ? (parent->GetName() + "." + member->getName())
    : member->getName();
  const wxAny& value = member->getValue();
  wxPGProperty* property = 0;
  if(value.CheckType<bool>()) {
    property = NEW wxBoolProperty(label, name);
    property->SetAttribute(wxPG_BOOL_USE_CHECKBOX, true);
  }
  else if(value.CheckType<int>()) {
    property = NEW wxIntProperty(label, name);
  }
  else if(value.CheckType<float>()) {
    property = NEW wxFloatProperty(label, name);
  }
  else if(value.CheckType<wxString>()) {
    property = NEW wxStringProperty(label, name);
  }
  else if(value.CheckType<glm::vec3>()) {
    glm::vec3 v = value.As<glm::vec3>();
    property = NEW wxStringProperty(label, name);
    property->AppendChild(NEW wxFloatProperty("X", name + ".x"));
    property->AppendChild(NEW wxFloatProperty("Y", name + ".y", v.y));
    property->AppendChild(NEW wxFloatProperty("Z", name + ".z", v.z));
  }
  else if(value.CheckType<Color>()) {
    property = NEW wxColourProperty(label, name);
    property->SetAttribute(wxPG_COLOUR_HAS_ALPHA, true);
    property->AppendChild(NEW wxUIntProperty("R", name + ".r"));
    property->AppendChild(NEW wxUIntProperty("G", name + ".g"));
    property->AppendChild(NEW wxUIntProperty("B", name + ".b"));
    property->AppendChild(NEW wxUIntProperty("A", name + ".a"));
  }
  else if(value.CheckType<wxFileName>()) {
    property = NEW wxFileProperty(label, name);
    property->SetAttribute(wxPG_FILE_SHOW_FULL_PATH, false);
  }
  else {
    property = NEW wxPGProperty(label, name);
  }
  if(parent) {
    parent->AppendChild(property);
  }
  else {
    Append(property);
  }
  property->SetClientData(const_cast<property::Member*>(member));

  if(const property::Property* child = member->getProperty()) {
    appendProperty(child, property);
  }
  property->ChangeFlag(wxPG_PROP_COLLAPSED, !member->isExpand());
  property->ChangeFlag(wxPG_PROP_READONLY, member->isReadOnly());
}
/***********************************************************************//**
	@brief 
***************************************************************************/
void PropertyGrid::updateProperty(wxPGProperty* property) {
  const property::Member* member = 
    static_cast<property::Member*>(property->GetClientData());
  if(member) {
    const wxAny& value = member->getValue();
    if(!value.IsNull()) {
      if(value.CheckType<bool>()) {
        property->SetValue(value.As<bool>());
      }
      else {
        property->SetValueFromString(member->getValueString());
      }
      if(value.CheckType<glm::vec3>()) {
        glm::vec3 v = value.As<glm::vec3>();
        for(unsigned int i = 0; i < 3; i++) {
          property->Item(i)->SetValueFromString(wxString::FromDouble(v[i]));
        }
      }
      else if(value.CheckType<Color>()) {
        Color color = value.As<Color>();
        property->Item(0)->SetValueFromInt(color.Red());
        property->Item(1)->SetValueFromInt(color.Green());
        property->Item(2)->SetValueFromInt(color.Blue());
        property->Item(3)->SetValueFromInt(color.Alpha());
      }
    }
  }
  for(unsigned int i = 0, n = property->GetChildCount(); i < n; i++) {
    updateProperty(property->Item(i));
  }
}
/***********************************************************************//**
	@brief 
***************************************************************************/
void PropertyGrid::onChange(wxPropertyGridEvent& event) {
  wxPGProperty* property = event.GetProperty();
  property::Member* member = 
    static_cast<property::Member*>(property->GetClientData());
  wxString value = event.GetPropertyValue().GetString();
  if(!member) {
    property = property->GetParent();
    TWB_ASSERT(property);
    member = static_cast<property::Member*>(property->GetClientData());
    TWB_ASSERT(member);
    value = "(";
    for(unsigned int i = 0, n = property->GetChildCount(); i < n; i++) {
      if(i > 0) {
        value.Append(",");
      }
      value.Append(property->Item(i)->GetValueAsString());
    }
    value.Append(")");
    property->SetValueFromString(value);
  }
  member->setValueString(value);
  node_->onUpdateProperty(member->getName());
  Application::Get()->update();
}
/***********************************************************************//**
	@brief 
***************************************************************************/
void PropertyGrid::onExpand(wxPropertyGridEvent& event) {
  setExpand(event, true);
}
/***********************************************************************//**
	@brief 
***************************************************************************/
void PropertyGrid::onCollapse(wxPropertyGridEvent& event) {
  setExpand(event, false);
}
/***********************************************************************//**
	@brief 
***************************************************************************/
void PropertyGrid::setExpand(wxPropertyGridEvent& event, bool isExpand) {
  wxPGProperty* property = event.GetProperty();
  property::Member* member = 
    static_cast<property::Member*>(property->GetClientData());
  if(member) {
    member->setExpand(isExpand);
  }
}
/***********************************************************************//**
	$Id$
***************************************************************************/
}
}
