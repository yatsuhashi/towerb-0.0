﻿/***********************************************************************//**
	@file
***************************************************************************/
#pragma once

#include "twb/SharedObject.hpp"

namespace twb {
namespace render {
/***********************************************************************//**
	@brief シェーダー
***************************************************************************/
class Shader : public SharedObject {
 public:
  enum {
    ATTRIB_POS, 
    ATTRIB_UV, 
    ATTRIB_NORMAL, 
    ATTRIB_COLOR, 
    ATTRIB_MATRIX_INDEXES, 
    ATTRIB_MATRIX_WEIGHTS
  };

  enum Uniform {
    UNIFORM_PROJECTION_MATRIX, 
    UNIFORM_MODEL_VIEW_MATRIX, 
    UNIFORM_NORMAL_MATRIX, 
    UNIFORM_MATRIXES, 
    UNIFORM_TEXTURE, 
    UNIFORM_COLOR, 
    UNIFORM_UV_OFFSET, 
    UNIFORM_MAX
  };

 private:
  enum {
    FLAG_USE, 
    FLAG_MAX
  };

 private:
  GLuint id_;
  GLint uniforms_[UNIFORM_MAX];
  std::bitset<FLAG_MAX> flag_;

 public:
  Shader();
  virtual ~Shader();

  void setup(const char* vert, 
             const char* frag, 
             size_t vertSize = 0, 
             size_t fragSize = 0);

  void use();
  void unuse();
  bool isUse() const {
    return flag_.test(FLAG_USE);
  }

  GLint getUniformLocation(const char* name);
  GLint getUniformLocation(Uniform uniform) const {
    return uniforms_[uniform];
  }
  void setUniform(GLint location, int value);
  void setUniform(GLint location, const glm::vec2& value);
  void setUniform(GLint location, const glm::vec3& value);
  void setUniform(GLint location, const glm::mat3& value);
  void setUniform(GLint location, const glm::mat4& value);
  void setUniform(GLint location, size_t num, const glm::vec4* values);

  template <class... Args>
  void setUniform(Uniform uniform, const Args&... args) {
    setUniform(getUniformLocation(uniform), args...);
  }

  void setMatrix(const Camera* camera, const glm::mat4& matrix);

 private:
  GLuint compile(GLenum type, const char* src, size_t size);
  void setupUniform();
};
/***********************************************************************//**
	$Id$
***************************************************************************/
}
}
