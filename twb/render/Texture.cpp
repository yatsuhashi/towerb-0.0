﻿/***********************************************************************//**
	@file
***************************************************************************/
#include "stdafx.hpp"
#include "twb/render/Image.hpp"
#include "twb/render/Texture.hpp"

namespace twb {
namespace render {
/***********************************************************************//**
	@brief コンストラクタ
***************************************************************************/
Texture::Texture(Image* image)
  : image_(image)
{
  setMagFilter(FILTER_LINEAR).
    setMinFilter(FILTER_LINEAR).
    setWrapU(WRAP_REPEAT).
    setWrapV(WRAP_REPEAT);
}
/***********************************************************************//**
	@brief デストラクタ
***************************************************************************/
Texture::~Texture() {
}
/***********************************************************************//**
	@brief 
***************************************************************************/
void Texture::bind(int unit) const {
  if(image_) {
    image_->bind(unit);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, magFilter_);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, minFilter_);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, wrapU_);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, wrapV_);
  }
}
/***********************************************************************//**
	@brief 
***************************************************************************/
void Texture::unbind(int unit) const {
  if(image_) {
    image_->unbind(unit);
  }
}
/***********************************************************************//**
	@brief 
***************************************************************************/
GLenum Texture::GetFilter(Filter filter) {
  switch(filter) {
  case FILTER_NEAREST:
    return GL_NEAREST;
  default:
    return GL_LINEAR;
  }
}
/***********************************************************************//**
	@brief 
***************************************************************************/
Texture::Filter Texture::GetFilter(GLenum filter) {
  switch(filter) {
  case GL_NEAREST:
    return FILTER_NEAREST;
  default:
    return FILTER_LINEAR;
  }
}
/***********************************************************************//**
	@brief 
***************************************************************************/
GLenum Texture::GetWrap(Wrap wrap) {
  switch(wrap) {
  case WRAP_REPEAT:
    return GL_REPEAT;
  default:
    return GL_CLAMP;
  }
}
/***********************************************************************//**
	@brief 
***************************************************************************/
Texture::Wrap Texture::GetWrap(GLenum wrap) {
  switch(wrap) {
  case GL_REPEAT:
    return WRAP_REPEAT;
  default:
    return WRAP_CLAMP;
  }
}
/***********************************************************************//**
	$Id$
***************************************************************************/
}
}
