﻿/***********************************************************************//**
	@file
***************************************************************************/
#include "stdafx.hpp"
#include "giraffe/Project.hpp"
#include "giraffe/gui/Application.hpp"
#include "giraffe/gui/MainFrame.hpp"
#include "giraffe/gui/Option.hpp"

namespace giraffe {
namespace gui {
/***********************************************************************//**
	@brief コンストラクタ
***************************************************************************/
Application::Application()
  : mainFrame_(0), 
    option_(NEW Option())
{
}
/***********************************************************************//**
	@brief デストラクタ
***************************************************************************/
Application::~Application() {
}
/***********************************************************************//**
	@brief 
***************************************************************************/
void Application::update() {
  super::update();
  mainFrame_->update();
}
/***********************************************************************//**
	@brief ノードを選択する
***************************************************************************/
void Application::selectNode(Node* node) {
  node_.reset(node);
  mainFrame_->onSelectNode(node);
}
/***********************************************************************//**
	@brief コマンドを実行する
***************************************************************************/
bool Application::submitCommand(wxCommand* command) {
  if(!commandProcessor_.Submit(command)) {
    return false;
  }
  update();
  return true;
}
/***********************************************************************//**
	@brief 直前のコマンドを元に戻す
***************************************************************************/
bool Application::undoCommand() {
  if(!commandProcessor_.Undo()) {
    return false;
  }
  update();
  return true;
}
/***********************************************************************//**
	@brief 元に戻すをやり直す
***************************************************************************/
bool Application::redoCommand() {
  if(!commandProcessor_.Redo()) {
    return false;
  }
  update();
  return true;
}
/***********************************************************************//**
	@brief 初期化
***************************************************************************/
bool Application::OnInit() {
  if(!super::OnInit()) {
    return false;
  }
  wxInitAllImageHandlers();
  mainFrame_ = NEW MainFrame();
  mainFrame_->Show(true);
  SetTopWindow(mainFrame_);
  selectNode(getProject());
  return true;
}
/***********************************************************************//**
	@brief 
***************************************************************************/
void Application::OnInitCmdLine(wxCmdLineParser& parser) {
  super::OnInitCmdLine(parser);
}
/***********************************************************************//**
	@brief 
***************************************************************************/
bool Application::OnCmdLineParsed(wxCmdLineParser& parser) {
  return super::OnCmdLineParsed(parser);
}
/***********************************************************************//**
	@brief 
***************************************************************************/
int Application::OnExit() {
  return super::OnExit();
}
/***********************************************************************//**
	$Id$
***************************************************************************/
}
}
