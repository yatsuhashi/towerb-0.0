﻿/***********************************************************************//**
	@file
***************************************************************************/
#pragma once

#include "giraffe/Image.hpp"

namespace giraffe {
namespace model {
/***********************************************************************//**
	@brief テクスチャノード
***************************************************************************/
class Texture : public Image {
  typedef Image super;

 private:
  twb::Shared<twb::render::Texture> texture_;
  twb::render::Texture::Filter magFilter_;
  twb::render::Texture::Filter minFilter_;
  twb::render::Texture::Wrap wrapU_;
  twb::render::Texture::Wrap wrapV_;

 public:
  Texture(const wxFileName& path);
  virtual ~Texture();

  twb::render::Texture* getTexture(Context* context);

 private:
  void createTexture(Context* context);
};
/***********************************************************************//**
	$Id$
***************************************************************************/
}
}
