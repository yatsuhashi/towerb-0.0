﻿/***********************************************************************//**
	@file
***************************************************************************/
#pragma once

namespace giraffe {
namespace model {
/***********************************************************************//**
	@brief 
***************************************************************************/
class Pose : public twb::SharedObject {
 private:
  twb::Shared<Skeleton> skeleton_;
  std::vector<glm::mat4> matrixes_;

 public:
  Pose(Skeleton* skeleton);
  Pose(const Pose& src);
  virtual ~Pose() {}

  Skeleton* getSkeleton() const {
    return skeleton_;
  }

  size_t countMatrix() const {
    return matrixes_.size();
  }

  glm::mat4& getMatrix(size_t index) {
    return matrixes_.at(index);
  }

  const glm::mat4& getMatrix(size_t index) const {
    return matrixes_.at(index);
  }

  Pose* createGlobalPose() const;
  void inverse();
};
/***********************************************************************//**
	$Id$
***************************************************************************/
}
}
