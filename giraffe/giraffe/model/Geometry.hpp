﻿/***********************************************************************//**
	@file
***************************************************************************/
#pragma once

#include "giraffe/Node.hpp"

namespace giraffe {
namespace model {
/***********************************************************************//**
	@brief 
***************************************************************************/
class Geometry : public Node {
  typedef Node super;

 public:
  struct Weight {
    int bone;
    double weight;
    
    Weight(int bone, double weight)
      : bone(bone), 
        weight(weight)
    {}
    bool operator<(const Weight& rhs) const {
      return (weight < rhs.weight ||
              (weight == rhs.weight && bone < rhs.bone));
    }
  };

  struct Vertex {
    glm::vec3 pos;
    glm::vec2 uv;
    glm::vec3 normal;
    glm::vec4 color;
    std::vector<Weight> weights;

    Vertex()
      : pos(0.0f), 
        uv(0.0f), 
        normal(0.0f, 1.0f, 0.0f), 
        color(1.0f)
    {}
  };

 private:
  twb::Shared<Material> material_;
  std::vector<Vertex> vertexes_;
  std::vector<int> matrixIndexes_;
  std::vector<twb::render::Geometry::Vertex> renderVertexes_;
  std::vector<uint16_t> renderIndexes_;
  twb::Shared<twb::render::Geometry> geometry_;

 public:
  Geometry(Material* material);
  virtual ~Geometry();

  Material* getMaterial() const {
    return material_;
  }

  void appendTriangle(const Vertex& p1, 
                      const Vertex& p2, 
                      const Vertex& p3);

  size_t countVertex() const {
    return vertexes_.size();
  }

  const Vertex& getVertex(size_t index) const {
    return vertexes_[index];
  }

  const std::vector<twb::render::Geometry::Vertex>&
  getRenderVertexes() const {
    return renderVertexes_;
  }

  const std::vector<int>& getMatrixIndexes() const {
    return matrixIndexes_;
  }

  const std::vector<uint16_t>& getRenderIndexes() const {
    return renderIndexes_;
  }

  twb::render::Geometry* createGeometry(Context* context);

 protected:
  virtual void onUpdateProperty(const wxString& name) override;

 private:
  void createRenderVertex(twb::render::Geometry::Vertex& des, 
                          Vertex& src);
  int getMatrixIndex(int index);
};
/***********************************************************************//**
	$Id$
***************************************************************************/
}
}
