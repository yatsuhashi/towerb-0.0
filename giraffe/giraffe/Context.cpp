﻿/***********************************************************************//**
	@file
***************************************************************************/
#include "stdafx.hpp"
#include "giraffe/Context.hpp"
#include "giraffe/Project.hpp"
#include "giraffe/gui/Application.hpp"

namespace giraffe {
/***********************************************************************//**
	@brief コンストラクタ
***************************************************************************/
Context::Context()
  : super()
{}
/***********************************************************************//**
	@brief デストラクタ
***************************************************************************/
Context::~Context() {
}
/***********************************************************************//**
	@brief 
***************************************************************************/
void Context::renderAxis(const glm::mat4& matrix) {
  twb::render::Renderer* renderer = getRenderer();
  renderer->beginShader(getDefaultShader());
  renderer->setModelMatrix(matrix);
  axis_->render(renderer);
}
/***********************************************************************//**
	@brief 
***************************************************************************/
void Context::onUpdate() {
  Application::Get()->getProject()->update(this);
}
/***********************************************************************//**
	@brief 
***************************************************************************/
void Context::onSetupRenderer() {
  wxString resourcesDir = wxStandardPaths::Get().GetResourcesDir();
  {
    twb::Buffer vert;
    twb::File::Read(resourcesDir + "/shader/default.vert", vert);
    twb::Buffer frag;
    twb::File::Read(resourcesDir + "/shader/default.frag", frag);
    twb::render::Shader* shader = new twb::render::Shader();
    shader->setup(vert.get<char>(), frag.get<char>(), 
                  vert.getSize(), frag.getSize());
    defaultShader_.reset(shader);
  }
  {
    twb::Buffer vert;
    twb::File::Read(resourcesDir + "/shader/model.vert", vert);
    twb::Buffer frag;
    twb::File::Read(resourcesDir + "/shader/model.frag", frag);
    twb::render::Shader* shader = new twb::render::Shader();
    shader->setup(vert.get<char>(), frag.get<char>(), 
                  vert.getSize(), frag.getSize());
    modelShader_.reset(shader);
  }
  {
    twb::render::Material* material = NEW twb::render::Material();
    material->setShader(getDefaultShader());
    defaultMaterial_.reset(material);
  }
  axis_.reset(NEW twb::render::geometry::Axis(getDefaultMaterial()));
  createDummyImage();
}
/***********************************************************************//**
	@brief 
***************************************************************************/
void Context::onRender() {
  twb::render::Renderer* renderer = getRenderer();
  Application::Get()->getProject()->render(this);
  renderer->endShader();
}
/***********************************************************************//**
	@brief 
***************************************************************************/
void Context::createDummyImage() {
  static const size_t SIZE = 32;
  twb::render::Image::Pixel* pixels = 
    NEW twb::render::Image::Pixel[SIZE * SIZE];
  twb::render::Image::Pixel* pt = pixels;
  for(size_t i = 0; i < SIZE; i++) {
    for(size_t j = 0; j < SIZE; j++, pt++) {
      if((i + j) % 2 == 0) {
        pt->set(0xff, 0xff, 0xff);
      }
      else {
        pt->set(0x00, 0x00, 0x00);
      }
    }
  }
  twb::render::Image* image = NEW twb::render::Image();
  image->load(glm::ivec2(SIZE), pixels);
  dummyImage_.reset(image);
  delete[] pixels;
}
/***********************************************************************//**
	$Id$
***************************************************************************/
}
