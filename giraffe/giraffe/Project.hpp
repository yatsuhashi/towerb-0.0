﻿/***********************************************************************//**
	@file
***************************************************************************/
#pragma once

#include "giraffe/Scene.hpp"

namespace giraffe {
/***********************************************************************//**
	@brief プロジェクトノード
***************************************************************************/
class Project : public Scene {
  typedef Scene super;

 private:
  enum {
    FLAG_UPDATE_TREE, 
    FLAG_MAX
  };

 private:
  std::map<wxString, int> suffixes_;
  twb::Shared<property::Camera> camera_;
  twb::Shared<twb::render::Geometry> grid_;
  std::bitset<FLAG_MAX> flag_;

 public:
  Project();
  virtual ~Project();

  wxString createName(wxString name);

  property::Camera* getCamera() const {
    return camera_;
  }

  Project& setUpdateTree(bool isUpdate) {
    flag_.set(FLAG_UPDATE_TREE, isUpdate);
    return *this;
  }

  bool isUpdateTree() const {
    return flag_.test(FLAG_UPDATE_TREE);
  }

  virtual void render(Context* context) override;

 protected:
  virtual void onAppendChild(Node* child) override;
  virtual void onUpdateProperty(const wxString& name) override;

 private:
  void createGrid(Context* context);
};
/***********************************************************************//**
	$Id$
***************************************************************************/
}
