#define BONE_MAX 128

attribute vec3 a_pos;
attribute vec2 a_uv;
attribute vec4 a_normal;
attribute vec4 a_color;
attribute vec4 a_matrixIndexes;
attribute vec4 a_matrixWeights;

uniform mat4 u_modelViewMatrix;
uniform mat4 u_projectionMatrix;
uniform mat3 u_normalMatrix;
uniform vec4 u_matrixes[BONE_MAX * 3];

varying vec2 v_uv;
varying vec3 v_normal;
varying vec4 v_color;

void skinningPos(in vec4 vertex, int index, float weight, inout vec4 pos) {
  vec4 v;
  v.x = dot(vertex, u_matrixes[index]);
  v.y = dot(vertex, u_matrixes[index + 1]);
  v.z = dot(vertex, u_matrixes[index + 2]);
  v.w = vertex.w;
  pos += v * weight;
}

void skinning(in vec4 vertex, inout vec4 pos) {
  pos = vec4(0.0);
  for(int i = 0; i < 4; i++) {
    skinningPos(vertex, int(a_matrixIndexes[i]) * 3, 
                a_matrixWeights[i], pos);
  }
}

void main() {
  vec4 pos;
  skinning(vec4(a_pos, 1.0), pos);
  gl_Position = u_projectionMatrix * u_modelViewMatrix * pos;
  v_uv = a_uv;
  v_normal = u_normalMatrix * vec3(a_normal);
  v_color = a_color;
}
