﻿/***********************************************************************//**
	@file
***************************************************************************/
#pragma once

namespace giraffe {
/***********************************************************************//**
	@brief インポーター基底クラス
***************************************************************************/
class Importer : public twb::SharedObject {
 private:
  wxFileName path_;
  glm::vec3 scale_;

 public:
  virtual ~Importer();

  static model::Model* ImportModel();
  static model::Model* ImportModel(const char* path);

  static model::Animation* ImportAnimation();
  static model::Animation* ImportAnimation(const char* path);

 protected:
  Importer(const wxFileName& path);

  const wxFileName& getPath() const {
    return path_;
  }

  const glm::vec3& getScale() const {
    return scale_;
  }

  virtual model::Model* importModel() {
    return 0;
  }

  virtual model::Animation* importAnimation() {
    return 0;
  }

  wxFileName getPath(wxString path);
  wxFileName expandPath(wxString path);
  wxFileName getImagePath(const wxString& filename);

 private:
  bool getImagePath(wxFileName& des, const wxString& src);

  static Importer* Create(const char* path);
};
/***********************************************************************//**
	$Id$
***************************************************************************/
}
