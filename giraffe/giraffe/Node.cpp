﻿/***********************************************************************//**
	@file
***************************************************************************/
#include "stdafx.hpp"
#include "giraffe/Node.hpp"
#include "giraffe/Project.hpp"
#include "giraffe/property/Property.hpp"

namespace giraffe {
/***********************************************************************//**
	@brief コンストラクタ
***************************************************************************/
Node::Node(const wxString& type)
  : type_(type), 
    index_(-1), 
    property_(NEW property::Property())
{
  property_->appendMember("name", "名前", wxString(""));
  property_->appendMember("isExpand", wxEmptyString, false);
  property_->appendMember("isShow", "表示", true);
}
/***********************************************************************//**
	@brief デストラクタ
***************************************************************************/
Node::~Node() {
}
/***********************************************************************//**
	@brief 
***************************************************************************/
Node& Node::setName(const wxString& name) {
  property_->setValue("name", name);
  return *this;
}
/***********************************************************************//**
	@brief 
***************************************************************************/
wxString Node::getName() const {
  return property_->getValue("name").As<wxString>();
}
/***********************************************************************//**
	@brief 
***************************************************************************/
Node& Node::setPath(const wxFileName& path) {
  path_ = path;
  if(property::Member* member = property_->findMember("path")) {
    member->setValue(path);
  }
  return *this;
}
/***********************************************************************//**
	@brief 
***************************************************************************/
Node& Node::setExpand(bool isExpand) {
  property_->setValue("isExpand", isExpand);
  return *this;
}
/***********************************************************************//**
	@brief 
***************************************************************************/
bool Node::isExpand() const {
  return property_->getValue("isExpand").As<bool>();
}
/***********************************************************************//**
	@brief 
***************************************************************************/
Node& Node::setShow(bool isShow) {
  property_->setValue("isShow", isShow);
  return *this;
}
/***********************************************************************//**
	@brief 
***************************************************************************/
bool Node::isShow() const {
  return property_->getValue("isShow").As<bool>();
}
/***********************************************************************//**
	@brief 
***************************************************************************/
void Node::appendChild(Node* child) {
  super::appendChild(child);
  for(Node* iter = this; iter; iter = iter->getParent()) {
    iter->onAppendChild(child);
  }
}
/***********************************************************************//**
	@brief 名前からノードを検索する
	@param[in] name 検索する名前
	@param[in] isRecursive 再帰的に検索するとき真
	@return 見つかったノード(見つからなかったときは0)
***************************************************************************/
Node* Node::findChild(const wxString& name, bool isRecursive) const {
  for(Node* iter = getChild(); iter; iter = iter->getNext()) {
    if(iter->getName() == name) {
      return iter;
    }
    if(isRecursive) {
      if(Node* node = iter->findChild(name, true)) {
        return node;
      }
    }
  }
  return 0;
}
/***********************************************************************//**
	@brief フォルダを取得する(存在しないときは生成する)
	@param[in] name フォルダ名
	@return フォルダノード
***************************************************************************/
Node* Node::getFolder(const wxString& name) {
  Node* folder = findChild(name);
  if(!folder) {
    folder = NEW Node("Folder");
    folder->setName(name);
    appendChild(folder);
  }
  return folder;
}
/***********************************************************************//**
	@brief フォルダを取得する
	@param[in] name フォルダ名
	@return フォルダノード(存在しないときは0)
***************************************************************************/
Node* Node::getFolder(const wxString& name) const {
  return findChild(name);
}
/***********************************************************************//**
	@brief 
***************************************************************************/
void Node::update(Context* context) {
  for(auto child = getChild(); child; child = child->getNext()) {
    child->update(context);
  }
}
/***********************************************************************//**
	@brief 
***************************************************************************/
void Node::render(Context* context) {
  for(auto child = getChild(); child; child = child->getNext()) {
    child->render(context);
  }
}
/***********************************************************************//**
	@brief 
***************************************************************************/
void Node::onUpdateTree() {
  super::onUpdateTree();
  if(Project* project = getNode<Project>()) {
    project->setUpdateTree(true);
  }
}
/***********************************************************************//**
	@brief 
***************************************************************************/
size_t Node::countFolderNode(const wxString& name) const {
  Node* folder = getFolder(name);
  return folder ? folder->countChild() : 0;
}
/***********************************************************************//**
	@brief 
***************************************************************************/
Node* Node::getFolderNode(const wxString& name, size_t index) const {
  Node* folder = getFolder(name);
  return folder ? folder->getChild(index) : 0;
}
/***********************************************************************//**
	$Id$
***************************************************************************/
}
