﻿/***********************************************************************//**
	@file
***************************************************************************/
#pragma once

namespace giraffe {
namespace gui {
/***********************************************************************//**
	@brief タイムライン
***************************************************************************/
class TimeLine : public wxPanel {
  typedef wxPanel super;

 private:
  wxToolBar* toolBar_;
  TimeLineTree* tree_;
  TimeLineView* view_;

 public:
  TimeLine(wxWindow* parent);
  virtual ~TimeLine();

  void onSelectNode(Node* node);

 private:
  void setupToolBar();
};
/***********************************************************************//**
	$Id$
***************************************************************************/
}
}
