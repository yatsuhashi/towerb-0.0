﻿/***********************************************************************//**
	@file
***************************************************************************/
#pragma once

#include "twb/Shared.hpp"
#include "twb/render/Renderable.hpp"

namespace twb {
namespace render {
/***********************************************************************//**
	@brief ジオメトリ
***************************************************************************/
class Geometry : public Renderable {
  typedef Renderable super;

 public:
  static const size_t WEIGHT_MAX = 4;

 public:
  struct Vertex {
    glm::vec3 pos;
    glm::vec2 uv;
    uint32_t normal;
    uint32_t color;
    uint8_t matrixIndexes[WEIGHT_MAX];
    uint8_t matrixWeights[WEIGHT_MAX];

    Vertex();
    Vertex& setNormal(const glm::vec3& normal) {
      this->normal = 
        glm::packSnorm4x8(glm::vec4(glm::normalize(normal), 0));
      return *this;
    }
    glm::vec3 getNormal() const {
      return glm::vec3(glm::unpackSnorm4x8(normal));
    }
    Vertex& setColor(const glm::vec4& color) {
      this->color = glm::packUnorm4x8(color);
      return *this;
    }
    glm::vec4 getColor() const {
      return glm::unpackUnorm4x8(color);
    }
    Vertex& appendWeight(int index, float weight);
    bool operator==(const Vertex& rhs) const;
  };

  enum Mode {
    MODE_POINTS,
    MODE_LINE_STRIP,
    MODE_LINE_LOOP,
    MODE_LINES,
    MODE_TRIANGLE_STRIP,
    MODE_TRIANGLE_FAN,
    MODE_TRIANGLES
  };
  
 private:
  enum {
    BUFFER_VERTEX, 
    BUFFER_INDEX, 
    BUFFER_MAX
  };

 private:
  Shared<Material> material_;
  GLuint buffers_[BUFFER_MAX];
  GLenum mode_;
  GLsizei indexNum_;
  std::vector<int> matrixIndexes_;
#if defined(TWB_WITH_GLES_OES)
  GLuint vertexArray_;
#endif

 public:
  Geometry(Material* material);
  virtual ~Geometry();

  void setupVertex(size_t vertexNum, const Vertex* vertexes);
  void setupIndex(Mode mode, size_t indexNum, const uint16_t* indexes);

  std::vector<int>& getMatrixIndexes() {
    return matrixIndexes_;
  }

  virtual void render(Renderer* renderer) override;
  void render(const Model* model, Renderer* renderer);

 private:
  void setupVertexAttrib() const;
  void render();
  static GLenum GetMode(Mode mode);
};
/***********************************************************************//**
	$Id$
***************************************************************************/
}
}
