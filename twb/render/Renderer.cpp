﻿/***********************************************************************//**
	@file
***************************************************************************/
#include "stdafx.hpp"
#include "twb/render/Camera.hpp"
#include "twb/render/Material.hpp"
#include "twb/render/Renderer.hpp"
#include "twb/render/Shader.hpp"
#include "twb/render/Texture.hpp"

namespace twb {
namespace render {
/***********************************************************************//**
	@brief コンストラクタ
***************************************************************************/
Renderer::Renderer()
  : defaultFrameBuffer_(0), 
    textureUnitMax_(1)
{
  glGetIntegerv(GL_FRAMEBUFFER_BINDING, &defaultFrameBuffer_);
  glGetIntegerv(GL_MAX_TEXTURE_IMAGE_UNITS, &textureUnitMax_);
  textures_ = NEW Shared<Texture>[textureUnitMax_];
}
/***********************************************************************//**
	@brief デストラクタ
***************************************************************************/
Renderer::~Renderer() {
  endShader();
  delete[] textures_;
}
/***********************************************************************//**
	@brief 描画開始
***************************************************************************/
void Renderer::beginRender() {
  TWB_ASSERT(!isRendering());
  flag_.set(FLAG_RENDERING);
}
/***********************************************************************//**
	@brief 描画終了
***************************************************************************/
void Renderer::endRender() {
  TWB_ASSERT(isRendering());
  flag_.reset(FLAG_RENDERING);
}
/***********************************************************************//**
	@brief カメラを変更する
	@param[in] camera カメラ
***************************************************************************/
void Renderer::pushCamera(Camera* camera) {
  cameraStack_.push_back(camera);
}
/***********************************************************************//**
	@brief カメラを戻す
***************************************************************************/
void Renderer::popCamera() {
  TWB_ASSERT(!cameraStack_.empty());
  cameraStack_.pop_back();
}
/***********************************************************************//**
	@brief 現在のカメラを取得する
***************************************************************************/
Camera* Renderer::getCamera() const {
  return cameraStack_.empty() ? 0 : cameraStack_.back();
}
/***********************************************************************//**
	@brief シェーダーを開始する
	@param[in] shader 開始するシェーダー
***************************************************************************/
void Renderer::beginShader(Shader* shader) {
  TWB_ASSERT(shader);
  if(shader != getShader()) {
    endShader();
    shader_.reset(shader);
    shader->use();
  }
}
/***********************************************************************//**
	@brief シェーダーを終了する
***************************************************************************/
void Renderer::endShader() {
  if(shader_) {
    shader_->unuse();
    shader_.reset();
  }
}
/***********************************************************************//**
	@brief 
***************************************************************************/
void Renderer::setModelMatrix(const glm::mat4& matrix) {
  Camera* camera = getCamera();
  Shader* shader = getShader();
  if(camera && shader) {
    shader->setMatrix(camera, matrix);
  }
}
/***********************************************************************//**
	@brief 
***************************************************************************/
void Renderer::beginMaterial(Material* material) {
  TWB_ASSERT(material);
  if(material != getMaterial()) {
    endMaterial();
    material_.reset(material);
    material->use(this);
  }
}
/***********************************************************************//**
	@brief 
***************************************************************************/
void Renderer::endMaterial() {
  if(material_) {
    material_->unuse(this);
    material_.reset();
  }
}
/***********************************************************************//**
	@brief 
***************************************************************************/
void Renderer::beginTexture(Texture* texture, int unit) {
  TWB_ASSERT(unit >= 0 && unit < textureUnitMax_);
  TWB_ASSERT(texture);
  auto& cache = textures_[unit];
  if(cache.get() == texture) {
    return;
  }
  endTexture(unit);
  cache.reset(texture);
  texture->bind(unit);
  getShader()->setUniform(Shader::UNIFORM_TEXTURE, unit);
}
/***********************************************************************//**
	@brief 
***************************************************************************/
void Renderer::endTexture(int unit) {
  TWB_ASSERT(unit >= 0 && unit < textureUnitMax_);
  auto& cache = textures_[unit];
  if(cache) {
    cache->unbind(unit);
    cache.reset();
  }
}
/***********************************************************************//**
	@brief 画面をクリアする
***************************************************************************/
void Renderer::clear(const glm::vec4& color) {
  glClearColor(color.r, color.g, color.b, color.a);
  glClear(GL_COLOR_BUFFER_BIT |
          GL_DEPTH_BUFFER_BIT |
          GL_STENCIL_BUFFER_BIT);
}
/***********************************************************************//**
	$Id$
***************************************************************************/
}
}
