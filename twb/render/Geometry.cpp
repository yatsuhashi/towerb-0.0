﻿/***********************************************************************//**
	@file
***************************************************************************/
#include "stdafx.hpp"
#include "twb/render/Geometry.hpp"
#include "twb/render/Material.hpp"
#include "twb/render/Model.hpp"
#include "twb/render/Renderer.hpp"
#include "twb/render/Shader.hpp"

namespace twb {
namespace render {
/***********************************************************************//**
	@brief コンストラクタ
***************************************************************************/
Geometry::Geometry(Material* material)
  : material_(material), 
    mode_(GL_TRIANGLES), 
    indexNum_(0)
#if defined(TWB_WITH_GLES_OES)
  , vertexArray_(0)
#endif
{
  for(size_t i = 0; i < BUFFER_MAX; i++) {
    buffers_[i] = 0;
  }
}
/***********************************************************************//**
	@brief デストラクタ
***************************************************************************/
Geometry::~Geometry() {
#if defined(TWB_WITH_GLES_OES)
  if(vertexArray_ != 0) {
    glDeleteVertexArraysOES(1, &vertexArray_);
  }
#endif
  for(int i = 0; i < BUFFER_MAX; i++) {
    if(buffers_[i] != 0) {
      glDeleteBuffers(1, &buffers_[i]);
    }
  }
}
/***********************************************************************//**
	@brief 頂点をセットアップする
	@param[in] vertexNum 頂点数
	@param[in] vertexes 頂点の配列
***************************************************************************/
void Geometry::setupVertex(size_t vertexNum, const Vertex* vertexes) {
  TWB_ASSERT(buffers_[BUFFER_VERTEX] == 0);
#if defined(TWB_WITH_GLES_OES)
  TWB_ASSERT(vertexArray_ == 0);
  glGenVertexArraysOES(1, &vertexArray_);
  glBindVertexArrayOES(vertexArray_);
#endif
  glGenBuffers(1, &buffers_[BUFFER_VERTEX]);
  glBindBuffer(GL_ARRAY_BUFFER, buffers_[BUFFER_VERTEX]);
  glBufferData(GL_ARRAY_BUFFER, sizeof(Vertex) * vertexNum, vertexes, 
               GL_STATIC_DRAW);
  setupVertexAttrib();
#if defined(TWB_WITH_GLES_OES)
  glBindVertexArrayOES(0);
#endif
  glBindBuffer(GL_ARRAY_BUFFER, 0);
}
/***********************************************************************//**
	@brief インデックスをセットアップする
	@param[in] mode 描画モード
	@param[in] indexNum インデックスの数
	@param[in] indexes インデックスの配列
***************************************************************************/
void Geometry::setupIndex(Mode mode, size_t indexNum,
                          const uint16_t* indexes) {
  mode_ = GetMode(mode);
  indexNum_ = GLsizei(indexNum);
  TWB_ASSERT(buffers_[BUFFER_INDEX] == 0);
#if defined(TWB_WITH_GLES_OES)
  TWB_ASSERT(vertexArray_ != 0);
  glBindVertexArrayOES(vertexArray_);
#endif
  glGenBuffers(1, &buffers_[BUFFER_INDEX]);
  glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, buffers_[BUFFER_INDEX]);
  glBufferData(GL_ELEMENT_ARRAY_BUFFER, indexNum * sizeof(uint32_t),
               indexes, GL_STATIC_DRAW);
#if defined(TWB_WITH_GLES_OES)
  glBindVertexArrayOES(0);
#endif
  glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
}
/***********************************************************************//**
	@brief 描画
***************************************************************************/
void Geometry::render(Renderer* renderer) {
  renderer->beginMaterial(material_);
  render();
}
/***********************************************************************//**
	@brief 
***************************************************************************/
void Geometry::render(const Model* model, Renderer* renderer) {
  if(!isShow()) {
    return;
  }
  renderer->beginMaterial(material_);
  Shader* shader = renderer->getShader();
  shader->setMatrix(renderer->getCamera(), model->getMatrix());
  std::vector<glm::vec4> vectors;
  for(auto index : matrixIndexes_) {
    glm::mat4 m = glm::transpose(model->getMatrix(index));
    vectors.push_back(m[0]);
    vectors.push_back(m[1]);
    vectors.push_back(m[2]);
  }
  shader->setUniform(twb::render::Shader::UNIFORM_MATRIXES, 
                     vectors.size(), vectors.data());
  render();
}
/***********************************************************************//**
	@brief 頂点アトリビュートを設定する
***************************************************************************/
void Geometry::setupVertexAttrib() const {
  glEnableVertexAttribArray(Shader::ATTRIB_POS);
  glVertexAttribPointer(Shader::ATTRIB_POS, 3, 
                        GL_FLOAT, GL_FALSE, sizeof(Vertex), 
                        reinterpret_cast<const GLvoid*>
                        (offsetof(Vertex, pos)));
                        
  glEnableVertexAttribArray(Shader::ATTRIB_UV);
  glVertexAttribPointer(Shader::ATTRIB_UV, 2, 
                        GL_FLOAT, GL_FALSE, sizeof(Vertex), 
                        reinterpret_cast<const GLvoid*>
                        (offsetof(Vertex, uv)));

  glEnableVertexAttribArray(Shader::ATTRIB_NORMAL);
  glVertexAttribPointer(Shader::ATTRIB_NORMAL, 4, 
                        GL_BYTE, GL_TRUE, sizeof(Vertex), 
                        reinterpret_cast<const GLvoid*>
                        (offsetof(Vertex, normal)));

  glEnableVertexAttribArray(Shader::ATTRIB_COLOR);
  glVertexAttribPointer(Shader::ATTRIB_COLOR, 4, 
                        GL_UNSIGNED_BYTE, GL_TRUE, sizeof(Vertex), 
                        reinterpret_cast<const GLvoid*>
                        (offsetof(Vertex, color)));

  glEnableVertexAttribArray(Shader::ATTRIB_MATRIX_INDEXES);
  glVertexAttribPointer(Shader::ATTRIB_MATRIX_INDEXES, 4, 
                        GL_UNSIGNED_BYTE, GL_FALSE, sizeof(Vertex), 
                        reinterpret_cast<const GLvoid*>
                        (offsetof(Vertex, matrixIndexes)));

  glEnableVertexAttribArray(Shader::ATTRIB_MATRIX_WEIGHTS);
  glVertexAttribPointer(Shader::ATTRIB_MATRIX_WEIGHTS, 4, 
                        GL_UNSIGNED_BYTE, GL_TRUE, sizeof(Vertex), 
                        reinterpret_cast<const GLvoid*>
                        (offsetof(Vertex, matrixWeights)));
}
/***********************************************************************//**
	@brief 描画
***************************************************************************/
void Geometry::render() {
#if defined(TWB_WITH_GLES_OES)
  if(vertexArray_ == 0) {
    return;
  }
  glBindVertexArrayOES(vertexArray_);
#else
  if(buffers_[BUFFER_INDEX] == 0) {
    return;
  }
  glBindBuffer(GL_ARRAY_BUFFER, buffers_[BUFFER_VERTEX]);
  glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, buffers_[BUFFER_INDEX]);
  setupVertexAttrib();
#endif
  glDrawElements(mode_, indexNum_, GL_UNSIGNED_SHORT, 0);
}
/***********************************************************************//**
	@brief 
***************************************************************************/
GLenum Geometry::GetMode(Mode mode) {
  static const GLenum MODES[] = {
    GL_POINTS,
    GL_LINE_STRIP,
    GL_LINE_LOOP,
    GL_LINES,
    GL_TRIANGLE_STRIP,
    GL_TRIANGLE_FAN,
    GL_TRIANGLES
  };
  return MODES[mode];
}
/***********************************************************************//**
	@brief 
***************************************************************************/
Geometry::Vertex::Vertex()
  : pos(0.0f), 
    uv(0.0f), 
    normal(glm::packSnorm4x8(glm::vec4(0, 1, 0, 0))), 
    color(0xffffffffU)
{
  matrixIndexes[0] = 0;
  matrixIndexes[1] = 0;
  matrixIndexes[2] = 0;
  matrixIndexes[3] = 0;
  matrixWeights[0] = 0;
  matrixWeights[1] = 0;
  matrixWeights[2] = 0;
  matrixWeights[3] = 0;
}
/***********************************************************************//**
	@brief 
***************************************************************************/
Geometry::Vertex& Geometry::Vertex::appendWeight(int index, float _weight) {
  int weight = int(_weight * 0xffff) >> 8;
  size_t minIndex = 0;
  int minWeight = 0x100;
  for(size_t i = 0; i < WEIGHT_MAX; i++) {
    uint8_t w = matrixWeights[i];
    if(w == 0) {
      matrixIndexes[i] = index;
      matrixWeights[i] = weight;
      return *this;
    }
    else if(w < minWeight) {
      minWeight = w;
      minIndex = i;
    }
  }
  if(minWeight < weight) {
    matrixIndexes[minIndex] = index;
    matrixWeights[minIndex] = weight;
  }
  int t = 0;
  for(size_t i = 0; i < WEIGHT_MAX; i++) {
    t += matrixWeights[i];
  }
  t *= 0xff;
  for(size_t i = 0; i < WEIGHT_MAX; i++) {
    matrixWeights[i] = t / matrixWeights[i];
  }
  return *this;
}
/***********************************************************************//**
	@brief 
***************************************************************************/
bool Geometry::Vertex::operator==(const Vertex& rhs) const {
  return memcmp(this, &rhs, sizeof(*this)) == 0;
}
/***********************************************************************//**
	$Id$
***************************************************************************/
}
}
