﻿/***********************************************************************//**
	@file
***************************************************************************/
#pragma once

#include "twb/render/Camera.hpp"

namespace twb {
namespace render {
/***********************************************************************//**
	@brief 正射影(2D)カメラ
***************************************************************************/
class OrthoCamera : public Camera {
  typedef Camera super;

 public:
  OrthoCamera() {}
  virtual ~OrthoCamera() {}

  OrthoCamera& setViewport(const IRect& viewport) {
    return static_cast<OrthoCamera&>(super::setViewport(viewport));
  }

  virtual void updateMatrix() override;
};
/***********************************************************************//**
	$Id$
***************************************************************************/
}
}
