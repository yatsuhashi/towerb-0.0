﻿/***********************************************************************//**
	@file
***************************************************************************/
#include "stdafx.hpp"
#include "twb/render/geometry/GeometryBase.hpp"

namespace twb {
namespace render {
namespace geometry {
/***********************************************************************//**
	@brief コンストラクタ
***************************************************************************/
GeometryBase::GeometryBase(Material* material)
  : super(material)
{
}
/***********************************************************************//**
	@brief デストラクタ
***************************************************************************/
GeometryBase::~GeometryBase() {
}
/***********************************************************************//**
	@brief 
***************************************************************************/
void GeometryBase::appendLine(const glm::vec3& p1, const glm::vec3& p2, 
                              const glm::vec4& color) {
  Vertex vertex;
  vertex.setColor(color);
  vertex.pos = p1;
  vertexes_.push_back(vertex);
  vertex.pos = p2;
  vertexes_.push_back(vertex);
  indexes_.push_back(vertexes_.size() - 2);
  indexes_.push_back(vertexes_.size() - 1);
}
/***********************************************************************//**
	@brief 
***************************************************************************/
void GeometryBase::setup(Mode mode) {
  setupVertex(vertexes_.size(), vertexes_.data());
  setupIndex(mode, indexes_.size(), indexes_.data());
  vertexes_.clear();
  indexes_.clear();
}
/***********************************************************************//**
	$Id$
***************************************************************************/
}
}
}
