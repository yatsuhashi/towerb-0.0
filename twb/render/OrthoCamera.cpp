﻿/***********************************************************************//**
	@file
***************************************************************************/
#include "stdafx.hpp"
#include "twb/render/OrthoCamera.hpp"

namespace twb {
namespace render {
/***********************************************************************//**
	@brief 
***************************************************************************/
void OrthoCamera::updateMatrix() {
  const IRect& viewport = getViewport();
  setProjectionMatrix(glm::ortho<float>(viewport.getLeft(), 
                                        viewport.getRight(), 
                                        viewport.getBottom(), 
                                        viewport.getTop()));
}
/***********************************************************************//**
	$Id$
***************************************************************************/
}
}
