﻿/***********************************************************************//**
	@file
***************************************************************************/
#pragma once

#include "twb/SharedObject.hpp"

namespace twb {
namespace render {
/***********************************************************************//**
	@brief 画像
***************************************************************************/
class Image : public SharedObject {
 public:
  struct Pixel {
    uint8_t r;
    uint8_t g;
    uint8_t b;
    uint8_t a;

    Pixel()
      : r(0), 
        g(0), 
        b(0), 
        a(0xff)
    {}
    void set(uint8_t r, uint8_t g, uint8_t b, uint8_t a = 0xff) {
      this->r = r;
      this->g = g;
      this->b = b;
      this->a = a;
    }
  };

 private:
  GLuint id_;
  glm::ivec2 size_;

 public:
  Image();
  virtual ~Image();

  GLuint getId() const {
    return id_;
  }

  const glm::ivec2& getSize() const {
    return size_;
  }

  void bind(int unit = 0) const;
  void unbind(int unit = 0) const;

  void load(const glm::ivec2& size, const Pixel* pixels);

 protected:
  void setSize(const glm::ivec2& size) {
    size_ = size;
  }
};
/***********************************************************************//**
	$Id$
***************************************************************************/
}
}
