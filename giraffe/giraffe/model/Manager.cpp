﻿/***********************************************************************//**
	@file
***************************************************************************/
#include "stdafx.hpp"
#include "giraffe/Exporter.hpp"
#include "giraffe/Importer.hpp"
#include "giraffe/Scene.hpp"
#include "giraffe/command/InsertNode.hpp"
#include "giraffe/gui/Application.hpp"
#include "giraffe/gui/Menu.hpp"
#include "giraffe/model/Animation.hpp"
#include "giraffe/model/Manager.hpp"
#include "giraffe/model/Model.hpp"

namespace giraffe {
namespace model {
/***********************************************************************//**
	@brief 
***************************************************************************/
void Manager::OnSelectMenu(int id) {
  gui::Application* app = gui::Application::Get();
  Node* selectNode = app->getSelectNode();
  switch(id) {
  case gui::MENU_MODEL_IMPORT_MODEL:
    if(Scene* scene = selectNode->getNode<Scene>()) {
      if(Model* model = Importer::ImportModel()) {
        app->submitCommand(NEW command::InsertNode(scene, model));
        app->selectNode(model);
      }
    }
    break;
  case gui::MENU_MODEL_IMPORT_ANIMATION:
    if(Model* model = selectNode->getNode<Model>()) {
      if(Animation* animation = Importer::ImportAnimation()) {
        Node* folder = model->getFolder("Animation");
        app->submitCommand(NEW command::InsertNode(folder, animation));
        app->selectNode(animation);
      }
    }
    break;
  case gui::MENU_MODEL_EXPORT_MODEL:
    if(Model* model = selectNode->getNode<Model>()) {
      Exporter::ExportModel(model);
    }
    break;
  default:
    return;
  }
}
/***********************************************************************//**
	$Id$
***************************************************************************/
}
}
