﻿/***********************************************************************//**
	@file
***************************************************************************/
#pragma once

namespace giraffe {
namespace gui {
/***********************************************************************//**
	@brief プロパティグリッド
***************************************************************************/
class PropertyGrid : public wxPropertyGrid {
  typedef wxPropertyGrid super;

 private:
  twb::Shared<Node> node_;

 public:
  PropertyGrid(wxWindow* parent);
  virtual ~PropertyGrid();

  void onSelectNode(Node* node);
  void update();

 private:
  void appendProperty(const property::Property* property, 
                      wxPGProperty* parent = 0);
  void appendMember(const property::Member* member, 
                    wxPGProperty* parent = 0);

  void updateProperty(wxPGProperty* property);

  void onChange(wxPropertyGridEvent& event);
  void onExpand(wxPropertyGridEvent& event);
  void onCollapse(wxPropertyGridEvent& event);
  void setExpand(wxPropertyGridEvent& event, bool isExpand);
};
/***********************************************************************//**
	$Id$
***************************************************************************/
}
}
