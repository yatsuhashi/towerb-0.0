﻿/***********************************************************************//**
	@file
***************************************************************************/
#include "stdafx.hpp"
#include "twb/TreeNode.hpp"

namespace twb {
/***********************************************************************//**
        @brief デフォルトコンストラクタ
***************************************************************************/
TreeNode::TreeNode()
  : parent_(0), 
    childNum_(0)
{}
/***********************************************************************//**
        @brief デストラクタ
***************************************************************************/
TreeNode::~TreeNode() {
  removeAllChild();
}
/***********************************************************************//**
        @brief 末尾に子供を追加する
	@param[in] node 追加する子供
***************************************************************************/
void TreeNode::appendChild(TreeNode* node) {
  Shared<TreeNode> guard(node);
  node->setParent(this);
  TWB_ASSERT(!node->getNext());
  if(!getChild()) {
    child_.reset(node);
  }
  else {
    TreeNode* iter = getChild();
    while(iter->getNext()) {
      iter = iter->getNext();
    }
    iter->setNext(node);
  }
  childNum_++;
  onUpdateTree();
}
/***********************************************************************//**
        @brief 先頭に子供を追加する
	@param[in] node 追加する子供
***************************************************************************/
void TreeNode::prependChild(TreeNode* node) {
  Shared<TreeNode> guard(node);
  node->setParent(this);
  TWB_ASSERT(!node->getNext());
  node->setNext(getChild());
  child_.reset(node);
  childNum_++;
  onUpdateTree();
}
/***********************************************************************//**
        @brief posの後ろに子供を追加する
	@param[in] pos 追加する場所(0のときは先頭)
	@param[in] child 追加する子供
***************************************************************************/
void TreeNode::insertChild(TreeNode* pos, TreeNode* child) {
  if(pos == getChild()) {
    prependChild(child);
    return;
  }
  else if(!pos) {
    appendChild(child);
    return;
  }
  for(TreeNode* iter = getChild(); iter; iter = iter->getNext()) {
    if(iter->getNext() == pos) {
      Shared<TreeNode> guard(child);
      child->setParent(this);
      child->setNext(pos);
      iter->setNext(child);
      childNum_++;
      onUpdateTree();
      return;
    }
  }
  TWB_ERROR("not found insert pos");
  appendChild(child);
}
/***********************************************************************//**
        @brief 子供を削除する
	@param[in] node 削除する子供
	@return 削除したときは真(nodeが存在しなかったときは偽)
***************************************************************************/
bool TreeNode::removeChild(TreeNode* node) {
  Shared<TreeNode> guard(node);
  if(getChild() == node) {
    child_.reset(node->getNext());
    unlink(node);
    return true;
  }
  for(auto iter = getChild(); iter->getNext(); iter = iter->getNext()) {
    if(iter->getNext() == node) {
      iter->next_.reset(node->getNext());
      unlink(node);
      return true;
    }
  }
  return false;
}
/***********************************************************************//**
        @brief すべての子供を削除する
***************************************************************************/
void TreeNode::removeAllChild() {
  while(getChild()) {
    removeChild(getChild());
  }
}
/***********************************************************************//**
        @brief 自身を親から削除する
***************************************************************************/
void TreeNode::remove() {
  if(parent_) {
    parent_->removeChild(this);
  }
}
/***********************************************************************//**
        @brief 1つ前に移動する
***************************************************************************/
void TreeNode::shiftPrev() {
  for(auto iter = getParent()->getChild(); iter; iter = iter->getNext()) {
    if(iter->getNext() == this) {
      Shared<TreeNode> guard(this);
      TreeNode* parent = getParent();
      parent->removeChild(this);
      parent->insertChild(iter, this);
      onUpdateTree();
      return;
    }
  }
}
/***********************************************************************//**
        @brief 1つ後ろに移動する
***************************************************************************/
void TreeNode::shiftNext() {
  if(getNext()) {
    Shared<TreeNode> guard(this);
    TreeNode* parent = getParent();
    TreeNode* pos = getNext()->getNext();
    parent->removeChild(this);
    parent->insertChild(pos, this);
    onUpdateTree();
  }
}
/***********************************************************************//**
        @brief 
***************************************************************************/
TreeNode* TreeNode::getChild(size_t index) const {
  TreeNode* iter = getChild();
  for(size_t i = 0; iter && i < index; i++) {
    iter = iter->getNext();
  }
  return iter;
}
/***********************************************************************//**
	@brief 
***************************************************************************/
bool TreeNode::hasNode(const TreeNode* node) const {
  while(node) {
    if(node == this) {
      return true;
    }
    node = node->getParent();
  }
  return false;
}
/***********************************************************************//**
        @brief 
***************************************************************************/
void TreeNode::setParent(TreeNode* node) {
  if(parent_) {
    parent_->removeChild(this);
  }
  parent_ = node;
}
/***********************************************************************//**
        @brief 
***************************************************************************/
void TreeNode::setNext(TreeNode* node) {
  next_.reset(node);
}
/***********************************************************************//**
        @brief 
***************************************************************************/
void TreeNode::unlink() {
  parent_ = 0;
  next_.reset();
}
/***********************************************************************//**
	@brief 
***************************************************************************/
void TreeNode::unlink(TreeNode* child) {
  child->unlink();
  childNum_--;
  onUpdateTree();
}
/***********************************************************************//**
        @brief 子供をソートする
***************************************************************************/
void TreeNode::sortChild() {
  std::vector<TreeNode*> list;
  while(TreeNode* child = getChild()) {
    child->addRef();
    list.push_back(child);
    removeChild(child);
  }
  std::sort(list.begin(), list.end(), CompareFunc());
  while(!list.empty()) {
    TreeNode* child = list.back();
    list.pop_back();
    prependChild(child);
    child->release();
  }
}
/***********************************************************************//**
	$Id$
***************************************************************************/
}
