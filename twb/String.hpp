﻿/***********************************************************************//**
	@file
***************************************************************************/
#pragma once

namespace twb {
/***********************************************************************//**
	@brief 
***************************************************************************/
class String : public std::string {
  typedef std::string super;

 public:
  static const String EMPTY;

 public:
  /**
   * デフォルトコンストラクタ
   */
  String() {}

  /**
   * コンストラクタ
   * @param[in] string 文字列
   */
  String(const std::string& string)
    : super(string)
  {}

  /**
   * コンストラクタ
   * @param[in] string 文字列
   */
  String(const char* string)
    : super(string ? string : "")
  {}

  /**
   * コンストラクタ
   * @param[in] string 文字列
   * @param[in] len 長さ
   */
  String(const char* string, size_t len)
    : super(string, len)
  {}

  operator const char*() const {
    return c_str();
  }

  String operator+(const char* rhs) const;

  /**
   * 空文字列か調べる
   * @return 空文字列のとき真
   */
  bool isEmpty() const {
    return empty();
  }

  /**
   * 文字列の長さを取得する
   * @return 文字列の長さ
   */
  size_t getLength() const {
    return size();
  }

  bool to(long& value) const;
  bool to(double& value) const;

  static String Format(const char* format, ...);
  static String Format(const char* format, va_list& args);
};
/***********************************************************************//**
	$Id$
***************************************************************************/
}
