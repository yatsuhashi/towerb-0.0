﻿/***********************************************************************//**
	@file
***************************************************************************/
#include "stdafx.hpp"
#include "twb/canvas/Context.hpp"
#include "twb/render/Renderer.hpp"

namespace twb {
namespace canvas {
/***********************************************************************//**
	@brief コンストラクタ
***************************************************************************/
Context::Context()
  : super(this)
{}
/***********************************************************************//**
	@brief デストラクタ
***************************************************************************/
Context::~Context() {
}
/***********************************************************************//**
	@brief 更新
***************************************************************************/
void Context::update() {
  deltaTime_ = time_.update();
  super::update();
}
/***********************************************************************//**
	@brief 描画
***************************************************************************/
void Context::render(const IRect& rect) {
  if(!renderer_.get()) {
    renderer_.reset(NEW render::Renderer());
    onSetupRenderer();
  }
  if(getLayout().getRect() != rect) {
    super::updateLayout(rect);
  }
  super::render();
}
/***********************************************************************//**
	$Id$
***************************************************************************/
}
}
