﻿/***********************************************************************//**
	@file
***************************************************************************/
#pragma once

#include "giraffe/Node.hpp"

namespace giraffe {
namespace model {
/***********************************************************************//**
	@brief モデル
***************************************************************************/
class Model : public Node {
  typedef Node super;

 private:
  twb::Shared<Skeleton> skeleton_;
  twb::Shared<Pose> basePose_;
  twb::Shared<Pose> inverseBasePose_;
  twb::Shared<Animation> animation_;
  twb::SharedArray<Bone> animationBones_;
  twb::Shared<Pose> renderPose_;
  int frame_;
  twb::Shared<twb::render::Model> model_;

 public:
  Model();
  virtual ~Model();

  Model& setSkeleton(Skeleton* skeleton);

  const Skeleton* getSkeleton() const {
    return skeleton_;
  }

  void setBasePose(Pose* pose);

  const Pose* getBasePose() const {
    return basePose_;
  }

  void setPose(const Skeleton* skeleton, const Pose* pose);

  void appendMaterial(Material* material);
  Material* findMaterial(const wxString& name);
  size_t countMaterial() const;
  Material* getMaterial(size_t index) const;

  void appendMesh(Mesh* mesh);
  size_t countMesh() const;
  Mesh* getMesh(size_t index) const;

  void appendAnimation(Animation* animation);
  void setAnimation(Animation* animation);
  size_t countAnimation() const;
  Animation* getAnimation(size_t index) const;

 protected:
  virtual void update(Context* context) override;
  virtual void render(Context* context) override;

 private:
  void createModel(Context* context);
  void updateAnimation();
  void renderNode(Context* context);
  void renderSkeleton(Context* context);
};
/***********************************************************************//**
	$Id$
***************************************************************************/
}
}
