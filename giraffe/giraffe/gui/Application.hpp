﻿/***********************************************************************//**
	@file
***************************************************************************/
#pragma once

#include "giraffe/Application.hpp"

namespace giraffe {
namespace gui {
/***********************************************************************//**
	@brief アプリケーション
***************************************************************************/
class Application : public giraffe::Application {
  typedef giraffe::Application super;

 private:
  MainFrame* mainFrame_;
  twb::Shared<Node> node_;
  std::auto_ptr<Option> option_;
  wxCommandProcessor commandProcessor_;

 public:
  Application();
  virtual ~Application();

  MainFrame* getMainFrame() const {
    return mainFrame_;
  }

  virtual void update() override;

  void selectNode(Node* node);
  Node* getSelectNode() const {
    return node_;
  }

  Option* getOption() const {
    return option_.get();
  }

  const wxCommandProcessor& getCommandProcessor() const {
    return commandProcessor_;
  }

  bool submitCommand(wxCommand* command);
  bool undoCommand();
  bool redoCommand();

  static Application* Get() {
    return static_cast<Application*>(super::Get());
  }

 protected:
  virtual bool OnInit() override;
  virtual void OnInitCmdLine(wxCmdLineParser& parser) override;
  virtual bool OnCmdLineParsed(wxCmdLineParser& parser) override;
  virtual int OnExit() override;

 private:
  void setupOption();
};
/***********************************************************************//**
	$Id$
***************************************************************************/
}
}
