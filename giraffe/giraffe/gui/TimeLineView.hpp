﻿/***********************************************************************//**
	@file
***************************************************************************/
#pragma once

namespace giraffe {
namespace gui {
/***********************************************************************//**
	@brief 
***************************************************************************/
class TimeLineView : public wxPanel {
  typedef wxPanel super;

 public:
  TimeLineView(wxWindow* parent);
  virtual ~TimeLineView();
};
/***********************************************************************//**
	$Id$
***************************************************************************/
}
}
