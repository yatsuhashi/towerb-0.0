﻿/***********************************************************************//**
	@file
***************************************************************************/
#include "stdafx.hpp"
#include "giraffe/Color.hpp"
#include "giraffe/property/Member.hpp"
#include "giraffe/property/Property.hpp"

namespace giraffe {
namespace property {
/***********************************************************************//**
	@brief コンストラクタ
***************************************************************************/
Member::Member(const wxString& name, const wxString& label, 
               const wxAny& value)
  : name_(name), 
    label_(label), 
    value_(value)
{}
/***********************************************************************//**
	@brief コンストラクタ
***************************************************************************/
Member::Member(const wxString& name, const wxString& label, 
               Property* property)
  : name_(name), 
    label_(label), 
    property_(property)
{
  setExpand(true);
  setReadOnly(true);
}
/***********************************************************************//**
	@brief デストラクタ
***************************************************************************/
Member::~Member() {
}
/***********************************************************************//**
	@brief プロパティの値をセットする
	@param[in] value プロパティの値
	@return 自分自身
***************************************************************************/
Member& Member::setValue(const wxAny& value) {
  TWB_ASSERT(value.HasSameType(value_));
  value_ = value;
  return *this;
}
/***********************************************************************//**
	@brief プロパティの値を文字列からセットする
	@param[in] value プロパティの値を表す文字列
	@return 自分自身
***************************************************************************/
Member& Member::setValueString(const wxString& value) {
  if(value_.CheckType<bool>()) {
    value_ = bool(atoi(value));
  }
  else if(value_.CheckType<int>()) {
    value_ = int(atoi(value));
  }
  else if(value_.CheckType<float>()) {
    value_ = float(atof(value));
  }
  else if(value_.CheckType<glm::vec3>()) {
    glm::vec3 v;
    if(sscanf(value, "(%f,%f,%f)", &v.x, &v.y, &v.z) == 3) {
      value_ = v;
    }
  }
  else if(value_.CheckType<Color>()) {
    int r, g, b, a = 255;
    if(sscanf(value, "(%d,%d,%d,%d)", &r, &g, &b, &a) >= 3) {
      value_ = Color(r, g, b, a);
    }
  }
  else if(value_.CheckType<wxString>()) {
    value_ = value;
  }
  else if(value_.CheckType<wxFileName>()) {
    value_ = wxFileName(value);
  }
  else {
    TWB_ERROR("illegal property value");
  }
  return *this;
}
/***********************************************************************//**
	@brief 
***************************************************************************/
wxString Member::getValueString() const {
  if(value_.CheckType<bool>()) {
    return wxString(value_.As<bool>() ? "1" : "0");
  }
  else if(value_.CheckType<int>()) {
    return wxString::Format("%d", value_.As<int>());
  }
  else if(value_.CheckType<float>()) {
    return wxString::FromDouble(value_.As<float>());
  }
  else if(value_.CheckType<glm::vec3>()) {
    glm::vec3 value = value_.As<glm::vec3>();
    return wxString("(") + 
      wxString::FromDouble(value.x) + "," + 
      wxString::FromDouble(value.y) + "," + 
      wxString::FromDouble(value.z) + ")";
  }
  else if(value_.CheckType<Color>()) {
    Color value = value_.As<Color>();
    return wxString::Format("(%d,%d,%d,%d)", 
                            value.Red(), 
                            value.Green(), 
                            value.Blue(), 
                            value.Alpha());
  }
  else if(value_.CheckType<wxFileName>()) {
    return value_.As<wxFileName>().GetFullPath();
  }
  else if(value_.CheckType<wxString>()) {
    return value_.As<wxString>();
  }
  TWB_ERROR("illegal property value");
  return wxEmptyString;
}
/***********************************************************************//**
	$Id$
***************************************************************************/
}
}
