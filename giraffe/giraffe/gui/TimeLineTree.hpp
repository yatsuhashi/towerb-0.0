﻿/***********************************************************************//**
	@file
***************************************************************************/
#pragma once

#include "giraffe/gui/NodeTree.hpp"

namespace giraffe {
namespace gui {
/***********************************************************************//**
	@brief 
***************************************************************************/
class TimeLineTree : public NodeTree {
  typedef NodeTree super;

 public:
  TimeLineTree(wxWindow* parent);
  virtual ~TimeLineTree();
};
/***********************************************************************//**
	$Id$
***************************************************************************/
}
}
