﻿/***********************************************************************//**
	@file
***************************************************************************/
#pragma once

#include "twb/Shared.hpp"
#include "twb/SharedArray.hpp"
#include "twb/render/Renderable.hpp"
#include "twb/render/Shader.hpp"

namespace twb {
namespace render {
/***********************************************************************//**
	@brief モデル
***************************************************************************/
class Model : public Renderable {
  typedef Renderable super;

 private:
  glm::mat4 matrix_;
  std::vector<glm::mat4> matrixes_;
  SharedArray<Mesh> meshes_;

 public:
  Model(size_t matrixNum);
  virtual ~Model();

  void appendMesh(Mesh* mesh);

  const glm::mat4& getMatrix() const {
    return matrix_;
  }

  size_t countMatrix() const {
    return matrixes_.size();
  }

  glm::mat4& getMatrix(size_t index) {
    return matrixes_.at(index);
  }

  const glm::mat4& getMatrix(size_t index) const {
    return matrixes_.at(index);
  }

  virtual void render(Renderer* renderer) override;
};
/***********************************************************************//**
	$Id$
***************************************************************************/
}
}
