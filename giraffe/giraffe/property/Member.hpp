﻿/***********************************************************************//**
	@file
***************************************************************************/
#pragma once

namespace giraffe {
namespace property {
/***********************************************************************//**
	@brief メンバー
***************************************************************************/
class Member : public twb::SharedObject {
 private:
  enum {
    FLAG_EXPAND, 
    FLAG_READ_ONLY, 
    FLAG_MAX
  };

 private:
  wxString name_;
  wxString label_;
  wxAny value_;
  twb::Shared<Property> property_;
  std::bitset<FLAG_MAX> flag_;

 public:
  Member(const wxString& name, const wxString& label, 
         const wxAny& value);
  Member(const wxString& name, const wxString& label, 
         Property* property);
  virtual ~Member();

  const wxString& getName() const {
    return name_;
  }

  const wxString& getLabel() const {
    return label_;
  }

  Member& setValue(const wxAny& value);
  Member& setValueString(const wxString& value);

  const wxAny& getValue() const {
    return value_;
  }
  wxString getValueString() const;

  Property* getProperty() const {
    return property_;
  }

  Member& setExpand(bool isExpand) {
    flag_.set(FLAG_EXPAND, isExpand);
    return *this;
  }

  bool isExpand() const {
    return flag_.test(FLAG_EXPAND);
  }

  Member& setReadOnly(bool isReadOnly) {
    flag_.set(FLAG_READ_ONLY, isReadOnly);
    return *this;
  }

  bool isReadOnly() const {
    return flag_.test(FLAG_READ_ONLY);
  }
};
/***********************************************************************//**
	$Id$
***************************************************************************/
}
}
