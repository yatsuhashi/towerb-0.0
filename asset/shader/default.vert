attribute vec3 a_pos;
attribute vec2 a_uv;
attribute vec4 a_normal;
attribute vec4 a_color;
attribute vec4 a_matrixIndexes;
attribute vec4 a_matrixWeights;

uniform mat4 u_modelViewMatrix;
uniform mat4 u_projectionMatrix;
uniform mat3 u_normalMatrix;

varying vec2 v_uv;
varying vec3 v_normal;
varying vec4 v_color;

void main() {
  gl_Position = u_projectionMatrix * u_modelViewMatrix * vec4(a_pos, 1.0);
  v_uv = a_uv;
  v_normal = u_normalMatrix * vec3(a_normal);
  v_color = a_color;
}
