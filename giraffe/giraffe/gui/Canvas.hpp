﻿/***********************************************************************//**
	@file
***************************************************************************/
#pragma once

namespace giraffe {
namespace gui {
/***********************************************************************//**
	@brief キャンバス
***************************************************************************/
class Canvas : public wxGLCanvas {
  typedef wxGLCanvas super;

 private:
  wxGLContext glContext_;
  twb::Shared<Context> context_;
  glm::ivec2 dragPos_;

 public:
  Canvas(wxWindow* parent);
  virtual ~Canvas();

  void update();

 private:
  void onPaint(wxPaintEvent& event);
  void onMouse(wxMouseEvent& event);
  void onMouseWheel(wxMouseEvent& event);
};
/***********************************************************************//**
	$Id$
***************************************************************************/
}
}
