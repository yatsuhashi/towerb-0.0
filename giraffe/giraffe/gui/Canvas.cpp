﻿/***********************************************************************//**
	@file
***************************************************************************/
#include "stdafx.hpp"
#include "giraffe/Context.hpp"
#include "giraffe/Project.hpp"
#include "giraffe/gui/Application.hpp"
#include "giraffe/gui/Canvas.hpp"
#include "giraffe/property/Camera.hpp"

namespace giraffe {
namespace gui {
static const int GL_ARGS[] = {
  WX_GL_RGBA, WX_GL_DOUBLEBUFFER, WX_GL_DEPTH_SIZE, 16, 0
};
/***********************************************************************//**
	@brief コンストラクタ
***************************************************************************/
Canvas::Canvas(wxWindow* parent)
  : super(parent, wxID_ANY, GL_ARGS), 
    glContext_(this),
    context_(NEW Context())
{
  Bind(wxEVT_PAINT, &Canvas::onPaint, this);
  Bind(wxEVT_LEFT_DOWN, &Canvas::onMouse, this);
  Bind(wxEVT_RIGHT_DOWN, &Canvas::onMouse, this);
  Bind(wxEVT_MIDDLE_DOWN, &Canvas::onMouse, this);
  Bind(wxEVT_MOTION, &Canvas::onMouse, this);
  Bind(wxEVT_MOUSEWHEEL, &Canvas::onMouseWheel, this);
}
/***********************************************************************//**
	@brief デストラクタ
***************************************************************************/
Canvas::~Canvas() {
}
/***********************************************************************//**
	@brief 
***************************************************************************/
void Canvas::update() {
  context_->update();
  Refresh();
}
/***********************************************************************//**
	@brief 
***************************************************************************/
void Canvas::onPaint(wxPaintEvent& event) {
  wxPaintDC dc(this);
  SetCurrent(glContext_);
  const wxSize& size = GetClientSize();
  context_->render(twb::IRect(glm::ivec2(0, 0),
                              glm::ivec2(size.x, size.y)));
  SwapBuffers();
}
/***********************************************************************//**
	@brief 
***************************************************************************/
void Canvas::onMouse(wxMouseEvent& event) {
  Application* app = Application::Get();
  property::Camera* camera = app->getProject()->getCamera();

  glm::ivec2 pos(event.GetX(), event.GetY());
  if(event.ButtonDown()) {
    dragPos_ = pos;
    SetFocus();
  }
  else if(event.Dragging()) {
    glm::ivec2 move = pos - dragPos_;
    if(move.x != 0 || move.y != 0) {
      if(event.AltDown()) {
        glm::vec2 v(float(move.x) / float(GetClientSize().x), 
                    float(move.y) / float(GetClientSize().y));
        if(event.LeftIsDown()) {
          camera->dragLeft(v);
        }
        else if(event.MiddleIsDown()) {
          camera->dragMiddle(v);
        }
        else if(event.RightIsDown()) {
          camera->dragRight(v);
        }
        camera->update();
        app->update();
      }
      dragPos_ = pos;
    }
  }
}
/***********************************************************************//**
	@brief 
***************************************************************************/
void Canvas::onMouseWheel(wxMouseEvent& event) {
  Application* app = Application::Get();
  app->getProject()->getCamera()->wheel(float(event.GetWheelRotation()) / 
                                        float(event.GetWheelDelta()));
  app->update();
}
/***********************************************************************//**
	$Id$
***************************************************************************/
}
}
