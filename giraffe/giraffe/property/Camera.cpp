﻿/***********************************************************************//**
	@file
***************************************************************************/
#include "stdafx.hpp"
#include "giraffe/property/Camera.hpp"

namespace giraffe {
namespace property {
/***********************************************************************//**
	@brief コンストラクタ
***************************************************************************/
Camera::Camera()
  : camera_(NEW twb::render::PerspectiveCamera())
{
  appendMember("fov", "画角", 60.0f);
  appendMember("eye", "視点", glm::vec3(0.0f, 1.0f, 5.0f));
  appendMember("at", "注視点", glm::vec3(0.0f, 0.0f, 0.0f));
  appendMember("up", "上方向", glm::vec3(0.0f, 1.0f, 0.0f));
  update();
}
/***********************************************************************//**
	@brief デストラクタ
***************************************************************************/
Camera::~Camera() {
}
/***********************************************************************//**
	@brief 
***************************************************************************/
void Camera::update() {
  camera_->
    setFov(glm::radians(getValue("fov").As<float>())).
    setEye(getEye()).
    setAt(getAt()).
    setUp(getUp()).
    updateMatrix();
}
/***********************************************************************//**
	@brief 
***************************************************************************/
void Camera::dragLeft(const glm::vec2& move) {
  glm::vec2 rot(move.x * glm::pi<float>(), 
                move.y * glm::pi<float>());
  glm::vec3 eye = getEye();
  glm::vec3 at = getAt();
  glm::mat4 m = camera_->getModelViewMatrix();
  glm::vec3 x(m[1]);
  m = glm::inverse(m);
  m = glm::rotate(m, -rot.x, x);
  m = glm::rotate(m, -rot.y, glm::vec3(1, 0, 0));
  glm::vec4 v = glm::vec4(0, 0, glm::length(eye - at), 0);
  setValue("eye", at + glm::vec3(m * v));
  setValue("up", glm::vec3(m[1]));
}
/***********************************************************************//**
	@brief 
***************************************************************************/
void Camera::dragMiddle(const glm::vec2& move) {
  glm::vec3 v(getEye() - getAt());
  float len = glm::length(v);
  if(move.y < 0) {
    len /= 1 - move.y * 2;
  }
  else {
    len *= 1 + move.y * 2;
  }
  setValue("eye", getAt() + glm::normalize(v) * len);
}
/***********************************************************************//**
	@brief 
***************************************************************************/
void Camera::dragRight(const glm::vec2& move) {
  glm::mat4 m = glm::inverse(camera_->getProjectionMatrix() * 
                             camera_->getModelViewMatrix());
  glm::vec3 v = glm::vec3(m * glm::vec4(-move.x, move.y, 0, 0)) * 10.0f;
  setValue("eye", getEye() + v);
  setValue("at", getAt() + v);
}
/***********************************************************************//**
	@brief 
***************************************************************************/
void Camera::wheel(float rotation) {
  float fov = getValue("fov").As<float>();
  rotation = glm::sign(rotation) * glm::ceil(glm::abs(rotation));
  fov = glm::floor(fov) - rotation;
  setValue("fov", glm::clamp(fov, 1.0f, 179.0f));
  update();
}
/***********************************************************************//**
	@brief 
***************************************************************************/
glm::vec3 Camera::getEye() const {
  return getValue("eye").As<glm::vec3>();
}
/***********************************************************************//**
	@brief 
***************************************************************************/
glm::vec3 Camera::getAt() const {
  return getValue("at").As<glm::vec3>();
}
/***********************************************************************//**
	@brief 
***************************************************************************/
glm::vec3 Camera::getUp() const {
  return getValue("up").As<glm::vec3>();
}
/***********************************************************************//**
	$Id$
***************************************************************************/
}
}
