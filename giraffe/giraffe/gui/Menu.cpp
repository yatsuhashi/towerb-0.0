﻿/***********************************************************************//**
	@file
***************************************************************************/
#include "stdafx.hpp"
#include "giraffe/Exporter.hpp"
#include "giraffe/Project.hpp"
#include "giraffe/command/DeleteNode.hpp"
#include "giraffe/gui/Application.hpp"
#include "giraffe/gui/MainFrame.hpp"
#include "giraffe/gui/Menu.hpp"
#include "giraffe/gui/Option.hpp"
#include "giraffe/model/Manager.hpp"

namespace giraffe {
namespace gui {
/***********************************************************************//**
	@brief 
***************************************************************************/
wxMenuItem* Menu::append(int id) {
  const char* label = GetLabel(id);
  switch(id) {
  case MENU_VIEW_SKELETON:
  case MENU_VIEW_NODE:
    return super::AppendCheckItem(id, label);
  default:
    return super::Append(id, label);
  }
}
/***********************************************************************//**
	@brief 
***************************************************************************/
void Menu::OnOpen(wxMenuEvent& event) {
  if(Menu* menu = dynamic_cast<Menu*>(event.GetMenu())) {
    const Application* app = Application::Get();
    const Node* node = app->getSelectNode();
    const Option* option = app->getOption();
    menu->setCheck(MENU_VIEW_SKELETON, 
                   option->getBool("MENU_VIEW_SKELETON"));
    menu->setCheck(MENU_VIEW_NODE, 
                   option->getBool("MENU_VIEW_NODE"));

    menu->setEnable(MENU_EDIT_DELETE, node && node->canDelete());
    menu->setEnable(MENU_EDIT_UNDO, app->getCommandProcessor().CanUndo());
    menu->setEnable(MENU_EDIT_REDO, app->getCommandProcessor().CanRedo());
  }
}
/***********************************************************************//**
	@brief 
***************************************************************************/
void Menu::OnSelect(wxCommandEvent& event) {
  int id = event.GetId();
  if(id > MENU_MODEL_TOP && id < MENU_MODEL_BOTTOM) {
    model::Manager::OnSelectMenu(id);
  }
  else {
    Application* app = Application::Get();
    Node* node = app->getSelectNode();
    Option* option = app->getOption();
    switch(event.GetId()) {
    case MENU_FILE_SAVE_PROJECT:
      Exporter::ExportNode(app->getProject());
      break;
    case MENU_FILE_EXIT:
    case wxID_EXIT:
      app->getMainFrame()->Close();
      break;
    case MENU_VIEW_SKELETON:
      option->setBool("MENU_VIEW_SKELETON", event.IsChecked());
      break;
    case MENU_VIEW_NODE:
      option->setBool("MENU_VIEW_NODE", event.IsChecked());
      break;

    case MENU_EDIT_DELETE:
      if(node && node->canDelete()) {
        app->submitCommand(NEW command::DeleteNode(node));
      }
      break;
    case MENU_EDIT_UNDO:
      app->undoCommand();
      break;
    case MENU_EDIT_REDO:
      app->redoCommand();
      break;

    default:
      break;
    }
  }
}
/***********************************************************************//**
	@brief 
***************************************************************************/
void Menu::setCheck(int id, bool isCheck) {
  if(wxMenuItem* item = FindItem(id)) {
    item->Check(isCheck);
  }
}
/***********************************************************************//**
	@brief 
***************************************************************************/
void Menu::setEnable(int id, bool isEnable) {
  if(wxMenuItem* item = FindItem(id)) {
    item->Enable(isEnable);
  }
}
/***********************************************************************//**
	@brief 
***************************************************************************/
const char* Menu::GetLabel(int id) {
  static const char* LABELS[] = {
    0, 
    "プロジェクトを開く", 
    "プロジェクトを保存", 
    "Giraffeを終了", 
    0, 

    0, 
    "ノードを削除", 
    "元に戻す", 
    "やり直す", 
    0, 

    0, 
    "モデルをインポート", 
    "アニメーションをインポート", 
    "モデルをエクスポート", 
    "アニメーションをエクスポート", 
    0, 

    0, 
    "スケルトンを表示", 
    "ノードを表示", 
    0
  };
  static_assert(twb::GetArraySize(LABELS) == MENU_BOTTOM - MENU_TOP - 1, 
                "no match LABELS");
  int index = id - (MENU_TOP + 1);
  return (index >= 0 && index < twb::GetArraySize(LABELS))
    ? LABELS[index]
    : 0;
}
/***********************************************************************//**
	$Id$
***************************************************************************/
}
}
