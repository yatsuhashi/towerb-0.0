﻿/***********************************************************************//**
	@file
***************************************************************************/
#pragma once

#include "twb/TreeNode.hpp"
#include "twb/canvas/Layout.hpp"

namespace twb {
namespace canvas {
/***********************************************************************//**
	@brief ウィジェット基底クラス
***************************************************************************/
class Widget : public TreeNodeBase<Widget> {
  typedef TreeNodeBase<Widget> super;

 private:
  enum {
    FLAG_PAUSE, 
    FLAG_HIDE, 
    FLAG_MAX
  };

 private:
  Context* context_;
  Layout layout_;
  std::bitset<FLAG_MAX> flag_;

 public:
  Widget(Context* context);
  virtual ~Widget();

  Layout& getLayout() {
    return layout_;
  }

  const Layout& getLayout() const {
    return layout_;
  }

  void updateLayout();
  void updateLayout(const IRect& rect);
  virtual void onUpdateLayout() {}

  Widget& setPause(bool isPause) {
    flag_.set(FLAG_PAUSE, isPause);
    return *this;
  }

  bool isPause() const {
    return flag_.test(FLAG_PAUSE);
  }

  Widget& setHide(bool isHide) {
    flag_.set(FLAG_HIDE, isHide);
    return *this;
  }

  bool isHide() const {
    return flag_.test(FLAG_HIDE);
  }

  virtual void update();
  virtual void render();

  virtual void onUpdate() {}
  virtual void onRender() {}

 protected:
  Context* getContext() const {
    return context_;
  }
};
/***********************************************************************//**
	@brief 
***************************************************************************/
template <class T>
class WidgetBase : public Widget {
  typedef Widget super;

 public:
  WidgetBase(T* context)
    : super(context)
  {}

 protected:
  T* getContext() const {
    return static_cast<T*>(super::getContext());
  }
};
/***********************************************************************//**
	$Id$
***************************************************************************/
}
}
