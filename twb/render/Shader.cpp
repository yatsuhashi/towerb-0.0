﻿/***********************************************************************//**
	@file
***************************************************************************/
#include "stdafx.hpp"
#include "twb/Buffer.hpp"
#include "twb/render/Camera.hpp"
#include "twb/render/Shader.hpp"

namespace twb {
namespace render {
/***********************************************************************//**
	@brief デフォルトコンストラクタ
***************************************************************************/
Shader::Shader()
  : id_(0)
{
  for(size_t i = 0; i < UNIFORM_MAX; i++) {
    uniforms_[i] = -1;
  }
}
/***********************************************************************//**
	@brief デストラクタ
***************************************************************************/
Shader::~Shader() {
  if(id_ != 0) {
    glDeleteProgram(id_);
  }
}
/***********************************************************************//**
	@brief セットアップ
	@param[in] vert 頂点シェーダのポインタ
	@param[in] frag フラグメントシェーダのポインタ
	@param[in] vertSize 頂点シェーダのサイズ
	@param[in] fragSize フラグメントシェーダのサイズ
***************************************************************************/
void Shader::setup(const char* vert, 
                   const char* frag, 
                   size_t vertSize, 
                   size_t fragSize) {
  TWB_ASSERT(id_ == 0);
  id_ = glCreateProgram();
  glAttachShader(id_, compile(GL_VERTEX_SHADER, vert, vertSize));
  glAttachShader(id_, compile(GL_FRAGMENT_SHADER, frag, fragSize));
  glBindAttribLocation(id_, ATTRIB_POS, "a_pos");
  glBindAttribLocation(id_, ATTRIB_UV, "a_uv");
  glBindAttribLocation(id_, ATTRIB_NORMAL, "a_normal");
  glBindAttribLocation(id_, ATTRIB_COLOR, "a_color");
  glBindAttribLocation(id_, ATTRIB_MATRIX_INDEXES, "a_matrixIndexes");
  glBindAttribLocation(id_, ATTRIB_MATRIX_WEIGHTS, "a_matrixWeights");
  glLinkProgram(id_);
  setupUniform();
}
/***********************************************************************//**
	@brief 
***************************************************************************/
void Shader::use() {
  flag_.set(FLAG_USE);
  glUseProgram(id_);
}
/***********************************************************************//**
	@brief 
***************************************************************************/
void Shader::unuse() {
  flag_.reset(FLAG_USE);
  glUseProgram(0);
}
/***********************************************************************//**
	@brief 
***************************************************************************/
GLint Shader::getUniformLocation(const char* name) {
  TWB_ASSERT(id_ != 0);
  return glGetUniformLocation(id_, name);
}
/***********************************************************************//**
	@brief 
***************************************************************************/
void Shader::setUniform(GLint location, int value) {
  TWB_ASSERT(isUse());
  glUniform1i(location, value);
}
/***********************************************************************//**
	@brief 
***************************************************************************/
void Shader::setUniform(GLint location, const glm::vec2& value) {
  TWB_ASSERT(isUse());
  glUniform2fv(location, 1, glm::value_ptr(value));
}
/***********************************************************************//**
	@brief 
***************************************************************************/
void Shader::setUniform(GLint location, const glm::vec3& value) {
  TWB_ASSERT(isUse());
  glUniform3fv(location, 1, glm::value_ptr(value));
}
/***********************************************************************//**
	@brief 
***************************************************************************/
void Shader::setUniform(GLint location, const glm::mat3& value) {
  TWB_ASSERT(isUse());
  glUniformMatrix3fv(location, 1, GL_FALSE, glm::value_ptr(value));
}
/***********************************************************************//**
	@brief 
***************************************************************************/
void Shader::setUniform(GLint location, const glm::mat4& value) {
  TWB_ASSERT(isUse());
  glUniformMatrix4fv(location, 1, GL_FALSE, glm::value_ptr(value));
}
/***********************************************************************//**
	@brief 
***************************************************************************/
void Shader::setUniform(GLint location, size_t num, 
                        const glm::vec4* values) {
  TWB_ASSERT(isUse());
  glUniform4fv(location, GLsizei(num), 
               reinterpret_cast<const GLfloat*>(values));
}
/***********************************************************************//**
	@brief 
***************************************************************************/
void Shader::setMatrix(const Camera* camera, const glm::mat4& matrix) {
  if(uniforms_[UNIFORM_PROJECTION_MATRIX] >= 0) {
    setUniform(uniforms_[UNIFORM_PROJECTION_MATRIX], 
               camera->getProjectionMatrix());
  }
  glm::mat4 modelViewMatrix = camera->getModelViewMatrix() * matrix;
  if(uniforms_[UNIFORM_MODEL_VIEW_MATRIX] >= 0) {
    setUniform(uniforms_[UNIFORM_MODEL_VIEW_MATRIX], modelViewMatrix);
  }
  if(uniforms_[UNIFORM_NORMAL_MATRIX] >= 0) {
    glm::mat3 normalMatrix = glm::inverse(glm::mat3(modelViewMatrix));
    setUniform(uniforms_[UNIFORM_NORMAL_MATRIX], normalMatrix);
  }
}
/***********************************************************************//**
	@brief 
***************************************************************************/
GLuint Shader::compile(GLenum type, const char* src, size_t srcSize) {
  GLuint id = glCreateShader(type);
  const GLchar* pt = static_cast<const GLchar*>(src);
  GLint size = GLint((srcSize == 0) ? strlen(src) : srcSize);
  glShaderSource(id, 1, &pt, &size);
  glCompileShader(id);
#ifndef TWB_MASTER
  {
    GLint length;
    glGetShaderiv(id, GL_INFO_LOG_LENGTH, &length);
    if(length > 1) {
      Buffer buffer(length);
      GLchar* text = buffer.get<GLchar>();
      glGetShaderInfoLog(id, length, 0, text);
      TWB_PRINTF("Shader compile error: %s\n", text);
    }
  }
#endif
  return id;
}
/***********************************************************************//**
	@brief 
***************************************************************************/
void Shader::setupUniform() {
  static const char* UNIFORMS[UNIFORM_MAX] = {
    "u_projectionMatrix", 
    "u_modelViewMatrix", 
    "u_normalMatrix", 
    "u_matrixes", 
    "u_texture", 
    "u_color", 
    "u_uvOffset"
  };
  for(size_t i = 0; i < UNIFORM_MAX; i++) {
    uniforms_[i] = getUniformLocation(UNIFORMS[i]);
  }
}
/***********************************************************************//**
	$Id$
***************************************************************************/
}
}
