﻿/***********************************************************************//**
	@file
***************************************************************************/
#pragma once

#include "giraffe/property/Property.hpp"

namespace giraffe {
namespace property {
/***********************************************************************//**
	@brief カメラプロパティ
***************************************************************************/
class Camera : public Property {
  typedef Property super;

 private:
  twb::Shared<twb::render::PerspectiveCamera> camera_;

 public:
  Camera();
  virtual ~Camera();

  twb::render::Camera* getCamera() const {
    return camera_;
  }

  void update();

  void dragLeft(const glm::vec2& move);
  void dragMiddle(const glm::vec2& move);
  void dragRight(const glm::vec2& move);
  void wheel(float rotation);

 private:
  glm::vec3 getEye() const;
  glm::vec3 getAt() const;
  glm::vec3 getUp() const;
};
/***********************************************************************//**
	$Id$
***************************************************************************/
}
}
