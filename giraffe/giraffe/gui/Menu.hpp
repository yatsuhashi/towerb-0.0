﻿/***********************************************************************//**
	@file
***************************************************************************/
#pragma once

namespace giraffe {
namespace gui {
/***********************************************************************//**
	@brief 
***************************************************************************/
enum {
  MENU_TOP = wxID_HIGHEST, 

  MENU_FILE_TOP, 
  MENU_FILE_OPEN_PROJECT, 
  MENU_FILE_SAVE_PROJECT, 
  MENU_FILE_EXIT, 
  MENU_FILE_BOTTOM, 

  MENU_EDIT_TOP, 
  MENU_EDIT_DELETE, 
  MENU_EDIT_UNDO, 
  MENU_EDIT_REDO, 
  MENU_EDIT_BOTTOM, 

  MENU_MODEL_TOP, 
  MENU_MODEL_IMPORT_MODEL, 
  MENU_MODEL_IMPORT_ANIMATION, 
  MENU_MODEL_EXPORT_MODEL, 
  MENU_MODEL_EXPORT_ANIMATION, 
  MENU_MODEL_BOTTOM, 

  MENU_VIEW_TOP, 
  MENU_VIEW_SKELETON, 
  MENU_VIEW_NODE, 
  MENU_VIEW_BOTTOM, 

  MENU_BOTTOM
};
/***********************************************************************//**
	@brief 
***************************************************************************/
class Menu : public wxMenu {
  typedef wxMenu super;

 public:
  wxMenuItem* append(int id);

  static void OnOpen(wxMenuEvent& event);
  static void OnSelect(wxCommandEvent& event);

 private:
  void setCheck(int id, bool isCheck);
  void setEnable(int id, bool isEnable);

  static const char* GetLabel(int id);
};
/***********************************************************************//**
	$Id$
***************************************************************************/
}
}
