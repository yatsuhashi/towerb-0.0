﻿/***********************************************************************//**
	@file
***************************************************************************/
#pragma once

namespace giraffe {
namespace command {
/***********************************************************************//**
	@brief ノードを削除するコマンド
***************************************************************************/
class DeleteNode : public wxCommand {
  typedef wxCommand super;

 private:
  twb::Shared<Node> parent_;
  twb::Shared<Node> child_;

 public:
  DeleteNode(Node* node);
  virtual ~DeleteNode();

  virtual bool Do() override;
  virtual bool Undo() override;
};
/***********************************************************************//**
	$Id$
***************************************************************************/
}
}
