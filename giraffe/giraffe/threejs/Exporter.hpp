﻿/***********************************************************************//**
	@file
***************************************************************************/
#pragma once

#include "giraffe/Exporter.hpp"

namespace giraffe {
namespace threejs {
/***********************************************************************//**
	@brief 
***************************************************************************/
class Exporter : public giraffe::Exporter {
  typedef giraffe::Exporter super;

 private:
  wxArrayString metadata_;
  wxArrayString vertexes_;
  wxArrayString uvs_;
  wxArrayString normals_;
  wxArrayString faces_;
  wxArrayString skinIndexes_;
  wxArrayString skinWeights_;

 public:
  Exporter();
  virtual ~Exporter();

 protected:
  virtual bool exportModel() override;

 private:
  wxString exportMaterials();
  wxString exportMaterial(const model::Material* material);

  wxString exportSkeleton();
  void exportMatrix(const glm::mat4& matrix, wxArrayString& lines, 
                    const wxString& rotKey = "rot");

  void exportGeometries();
  void exportGeometry(const model::Geometry* geometry);

  wxString exportAnimations();
  wxString exportAnimation(const model::Animation* animation);
};
/***********************************************************************//**
	$Id$
***************************************************************************/
}
}
