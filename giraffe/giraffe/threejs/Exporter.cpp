﻿/***********************************************************************//**
	@file
***************************************************************************/
#include "stdafx.hpp"
#include "giraffe/model/Animation.hpp"
#include "giraffe/model/Bone.hpp"
#include "giraffe/model/Geometry.hpp"
#include "giraffe/model/Material.hpp"
#include "giraffe/model/Mesh.hpp"
#include "giraffe/model/Model.hpp"
#include "giraffe/model/Pose.hpp"
#include "giraffe/model/Skeleton.hpp"
#include "giraffe/model/Texture.hpp"
#include "giraffe/threejs/Exporter.hpp"

namespace giraffe {
namespace threejs {
/***********************************************************************//**
	@brief コンストラクタ
***************************************************************************/
Exporter::Exporter() {
}
/***********************************************************************//**
	@brief デストラクタ
***************************************************************************/
Exporter::~Exporter() {
}
/***********************************************************************//**
	@brief 
***************************************************************************/
bool Exporter::exportModel() {
  open();
  wxArrayString lines;
  lines.Add(exportMaterials());
  lines.Add(exportSkeleton());
  exportGeometries();
  lines.Add(wxString::Format("\"vertices\": [\n%s]", Join(vertexes_)));
  lines.Add(wxString::Format("\"uvs\": [[\n%s]]", Join(uvs_)));
  lines.Add(wxString::Format("\"normals\": [\n%s]", Join(normals_)));
  lines.Add(wxString::Format("\"faces\": [\n%s]", Join(faces_)));

  lines.Add(wxString::Format("\"skinIndices\": [\n%s]", 
                             Join(skinIndexes_)));

  lines.Add(wxString::Format("\"skinWeights\": [\n%s]", 
                             Join(skinWeights_)));

  lines.Add(exportAnimations());

  lines.Add(wxString::Format("\"metadata\": {\n%s}", Join(metadata_)));
  wxString buff;
  buff << "{\n" << Join(lines) << "}\n";
  wxStringInputStream in(buff);
  getOut()->Write(in);
  return true;
}
/***********************************************************************//**
	@brief 
***************************************************************************/
wxString Exporter::exportMaterials() {
  model::Model* model = getModel();
  size_t materialNum = model->countMaterial();
  metadata_.Add(wxString::Format("\"materials\": %d", int(materialNum)));
  wxString buff;
  buff << "\"materials\": [\n";
  wxArrayString lines;
  for(size_t i = 0, n = materialNum; i < n; i++) {
    lines.Add(exportMaterial(model->getMaterial(i)));
  }
  return buff << Join(lines) << "]";
}
/***********************************************************************//**
	@brief 
***************************************************************************/
wxString Exporter::exportMaterial(const model::Material* material) {
  wxArrayString lines;
  if(const model::Texture* texture = 
     material->getTexture(model::Material::TYPE_DIFFUSE)) {
    wxFileName path = getRelativePath(texture->getPath());
    lines.Add(wxString::Format("\"mapDiffuse\": \"%s.png\"", 
                               path.GetName()));
    lines.Add("\"mapDiffuseWrap\": [ \"repeat\", \"repeat\" ]");
  }
  wxString result;
  return result << "{\n" << Join(lines, "  ") << "}";
}
/***********************************************************************//**
	@brief 
***************************************************************************/
wxString Exporter::exportSkeleton() {
  const model::Skeleton* skeleton = getModel()->getSkeleton();
  const model::Pose* basePose = getModel()->getBasePose();
  size_t boneNum = skeleton->countBone();
  metadata_.Add(wxString::Format("\"bones\": %d", int(boneNum)));
  wxArrayString bones;
  for(size_t i = 0, n = boneNum; i < n; i++) {
    wxArrayString lines;
    const model::Bone* bone = skeleton->getBone(i);
    const model::Bone* parent = bone->getParentBone();
    lines.Add(wxString::Format("\"parent\": %d", 
                               parent ? parent->getIndex() : -1));
    lines.Add(wxString::Format("\"name\": \"%s\"", bone->getName()));
    exportMatrix(basePose->getMatrix(i), lines, "rotq");
    bones.Add(wxString::Format("{\n%s}", Join(lines, "  ")));
  }
  return wxString::Format("\"bones\": [\n%s]", Join(bones));
}
/***********************************************************************//**
	@brief 
***************************************************************************/
void Exporter::exportMatrix(const glm::mat4& matrix, 
                            wxArrayString& lines, 
                            const wxString& rotKey) {
  glm::vec3 scl(glm::length(glm::vec3(matrix[0])), 
                glm::length(glm::vec3(matrix[1])), 
                glm::length(glm::vec3(matrix[2])));
  glm::vec3 pos = glm::vec3(matrix[3]);
  glm::quat rotq;
  if(scl.x > 0 && scl.y > 0 && scl.z > 0) {
    rotq = glm::quat_cast
      (glm::mat3(glm::normalize(glm::vec3(matrix[0])), 
                 glm::normalize(glm::vec3(matrix[1])), 
                 glm::normalize(glm::vec3(matrix[2]))));
  }
  lines.Add(wxString::Format("\"pos\": [ %f, %f, %f ]", 
                             pos.x, pos.y, pos.z));
  lines.Add(wxString::Format("\"%s\": [ %f, %f, %f, %f ]", rotKey, 
                             rotq.x, rotq.y, rotq.z, rotq.w));
  lines.Add(wxString::Format("\"scl\": [ %f, %f, %f ]", 
                             scl.x, scl.y, scl.z));
}
/***********************************************************************//**
	@brief 
***************************************************************************/
void Exporter::exportGeometries() {
  const model::Model* model = getModel();
  for(size_t i = 0, n = model->countMesh(); i < n; i++) {
    const model::Mesh* mesh = model->getMesh(i);
    for(size_t i = 0, n = mesh->countGeometry(); i < n; i++) {
      exportGeometry(mesh->getGeometry(i));
    }
  }
  metadata_.Add(wxString::Format("\"vertices\": %d", 
                                 int(vertexes_.size())));
  metadata_.Add(wxString::Format("\"faces\": %d", 
                                 int(faces_.size())));
}
/***********************************************************************//**
	@brief 
***************************************************************************/
void Exporter::exportGeometry(const model::Geometry* geometry) {
  if(!geometry->isShow()) {
    return;
  }
  int indexOffset = int(vertexes_.size());
  const auto& vertexes = geometry->getRenderVertexes();
  const auto& matrixIndexes = geometry->getMatrixIndexes();
  for(size_t i = 0, n = vertexes.size(); i < n; i++) {
    const auto& vertex = vertexes.at(i);
    vertexes_.push_back(wxString::Format("%f, %f, %f", 
                                         vertex.pos.x, 
                                         vertex.pos.y, 
                                         vertex.pos.z));
    uvs_.push_back(wxString::Format("%f, %f", 
                                    vertex.uv.x, 1 - vertex.uv.y));
    glm::vec3 normal = vertex.getNormal();
    normals_.push_back(wxString::Format("%f, %f, %f", 
                                        normal.x, 
                                        normal.y, 
                                        normal.z));
    for(int i = 0; i < 2; i++) {
      skinIndexes_.push_back
        (wxString::Format("%d", matrixIndexes[vertex.matrixIndexes[i]]));
      skinWeights_.push_back
        (wxString::Format("%f", vertex.matrixWeights[i] / 255.0f));
    }
  }
  const auto& indexes = geometry->getRenderIndexes();
  int material = geometry->getMaterial()->getIndex();
  for(size_t i = 0, n = indexes.size(); i < n; i += 3) {
    int a = indexOffset + indexes[i];
    int b = indexOffset + indexes[i + 1];
    int c = indexOffset + indexes[i + 2];
    faces_.push_back(wxString::Format("42, %d, %d, %d, %d, %d, %d, %d, "
                                      "%d, %d, %d", 
                                      a, b, c, material, a, b, c, 
                                      a, b, c));
  }
}
/***********************************************************************//**
	@brief 
***************************************************************************/
wxString Exporter::exportAnimations() {
  wxArrayString animations;
  for(size_t i = 0, n = getModel()->countAnimation(); i < n; i++) {
    animations.Add(exportAnimation(getModel()->getAnimation(i)));
  }
  return wxString::Format("\"animations\": [\n%s]", Join(animations));
}
/***********************************************************************//**
	@brief 
***************************************************************************/
wxString Exporter::exportAnimation(const model::Animation* animation) {
  wxArrayString lines;
  int fps = animation->getFps();
  size_t poseNum = animation->countPose();
  lines.Add(wxString::Format("\"name\": \"%s\"", animation->getName()));
  lines.Add(wxString::Format("\"fps\": %d", animation->getFps()));
  lines.Add(wxString::Format("\"length\": %f", 
                             float(poseNum) / float(fps)));
  wxArrayString hierarchy;
  const model::Skeleton* skeleton = getModel()->getSkeleton();
  for(size_t i = 0, n = skeleton->countBone(); i < n; i++) {
    wxArrayString bones;
    const model::Bone* bone = skeleton->getBone(i);
    const model::Bone* parent = bone->getParentBone();
    bones.Add(wxString::Format("\"parent\": %d", 
                               parent ? parent->getIndex() : -1));
    wxArrayString keys;
    for(size_t j = 0; j < poseNum; j++) {
      wxArrayString poses;
      poses.Add(wxString::Format("\"time\": %f", float(j) / fps));
      const model::Pose* pose = animation->getPose(j);
      glm::mat4 m;
      if(const model::Bone* animationBone = 
         animation->getSkeleton()->findBone(bone->getName())) {
        m = pose->getMatrix(animationBone->getIndex());
      }
      exportMatrix(m, poses);
      keys.Add(wxString::Format("{\n%s}", Join(poses)));
    }
    bones.Add(wxString::Format("\"keys\": [\n%s]", Join(keys)));
    hierarchy.Add(wxString::Format("{\n%s}", Join(bones)));
  }
  lines.Add(wxString::Format("\"hierarchy\": [\n%s]", Join(hierarchy)));
  return wxString::Format("{\n%s}", Join(lines));
}
/***********************************************************************//**
	$Id$
***************************************************************************/
}
}
