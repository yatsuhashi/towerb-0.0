﻿/***********************************************************************//**
	@file
***************************************************************************/
#pragma once

namespace twb {
/***********************************************************************//**
        @brief 矩形テンプレート
***************************************************************************/
template <class T>
class Rect {
 private:
  T pos_;
  T size_;

 public:
  Rect() {}

  Rect(const T& pos, const T& size)
    : pos_(pos), 
      size_(size)
  {}

  Rect& setPos(const T& pos) {
    pos_ = pos;
    return *this;
  }

  T& getPos() {
    return pos_;
  }

  const T& getPos() const {
    return pos_;
  }

  Rect& setSize(const T& size) {
    size_ = size;
    return *this;
  }

  T& getSize() {
    return size_;
  }

  const T& getSize() const {
    return size_;
  }

  auto getLeft() const -> decltype(pos_.x) {
    return pos_.x;
  }

  auto getTop() const -> decltype(pos_.y) {
    return pos_.y;
  }

  auto getRight() const -> decltype(pos_.y) {
    return pos_.x + size_.x;
  }

  auto getBottom() const -> decltype(pos_.y) {
    return pos_.y + size_.y;
  }

  bool isContain(const T& pos) const {
    return (pos.x >= pos_.x && pos.x < pos_.x + size_.x &&
            pos.y >= pos_.y && pos.y < pos_.y + size_.y);
  }

  template <class C>
  operator Rect<C>() const {
    return Rect<C>(C(pos_.x), C(pos_.y), C(size_.x), C(size_.y));
  }

  void createVertexes(T* vertexes) const {
    vertexes[0] = pos_;
    vertexes[1] = T(pos_.x + size_.x, pos_.y);
    vertexes[2] = T(pos_.x, pos_.y + size_.y);
    vertexes[3] = pos_ + size_;
  }

  /*!
   *
   */
  bool operator==(const Rect& rhs) const {
    return getPos() == rhs.getPos() && getSize() == rhs.getSize();
  }

  /*!
   *
   */
  bool operator!=(const Rect& rhs) const {
    return getPos() != rhs.getPos() || getSize() != rhs.getSize();
  }
};
/***********************************************************************//**
        @brief 
***************************************************************************/
typedef Rect<glm::ivec2> IRect;
typedef Rect<glm::vec2> FRect;
/***********************************************************************//**
        $Id$
***************************************************************************/
}
