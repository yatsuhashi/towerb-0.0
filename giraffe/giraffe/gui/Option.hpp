﻿/***********************************************************************//**
	@file
***************************************************************************/
#pragma once

namespace giraffe {
namespace gui {
/***********************************************************************//**
	@brief オプション
***************************************************************************/
class Option : public wxFileConfig {
  typedef wxFileConfig super;

 public:
  Option();
  virtual ~Option();

  bool getBool(const wxString& name) const;
  void setBool(const wxString& name, bool value) {
    super::Write(name, value);
  }

 private:
  void setup();
};
/***********************************************************************//**
	$Id$
***************************************************************************/
}
}
